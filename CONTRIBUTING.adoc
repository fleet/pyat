
== Contributing


Contributions are welcome, and they are greatly appreciated! Every little bit
helps, and credit will always be given.

You can contribute in many ways:

### Types of Contributions

#### Report Bugs

Report bugs at https://gitlab.ifremer.fr/fleet/pyat/issues

If you are reporting a bug, please include:

* Your operating system name and version.
* Any details about your local setup that might be helpful in troubleshooting.
* Detailed steps to reproduce the bug.

#### Fix Bugs or  Implement Features

Look through the Gitlab issues for bugs and features.

#### Write Documentation

pyat could always use more documentation, whether as part of the
official pyat docs, in docstrings, or even on the web in blog posts,
articles, and such.

#### Submit Feedback

The best way to send feedback is to file an issue at https://gitlab.ifremer.fr/fleet/pyat/issues

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome :)

___

### Get Started!


### Get Started!

Ready to contribute? Here's how to set up `pyat` for local development.

. If not already done install and setup anaconda or miniconda https://docs.conda.io/en/latest/miniconda.html

. Clone the `pyat` repo on GitLab.
+
`git clone https://gitlab.ifremer.fr/fleet/pyat --recurse`

. Create your developpent environnement with the create_anaconda_environments.py script in requirements directory.
+
`cd requirements`
+
`python create_anaconda_environments.py -t dev`
+
For detailed info see https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#building-identical-conda-environments

. activate anaconda environment (for development)
`conda activate pyat_dev`


#### Tips

##### To run a subset of tests::

....
> py.test tests.test_pyat
....

##### Run the tests
go to src dir and type

....
> cd src
> python.exe -m pytest
....


