"""
Use to be able to retrieve modules and import in core and app directories
"""

import sys
from pathlib import Path
sys.path.append(str(Path("__file__").absolute().parent.parent.parent))
