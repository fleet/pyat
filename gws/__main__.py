import os
import re
import sys
from pathlib import Path

if __name__ == "__main__":
    module_file = Path(os.path.abspath(__file__))
    current_folder = module_file.parent

    # Define pyat root folder (containing app/, core/, ...)
    pyat_folder = current_folder.parent / "pyat"
    if not pyat_folder.is_dir():
        print("Pyat folder not found")
        sys.exit(1)

    pyat_conf = current_folder / "pyat_conf.json"
    # Patch configuration file, set conda env
    with open(pyat_conf, "r", encoding="utf8") as f:
        file_content = f.read()
        file_content = re.sub("@PYAT_ROOT@", pyat_folder.as_posix(), file_content)
        print(file_content)
