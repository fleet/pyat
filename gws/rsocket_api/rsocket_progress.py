import logging

from gws.rsocket_api.rsocket_message import RsocketMessageEmitter
from pyat.core.utils.monitor import ProgressMonitor


class RSocketMonitor(ProgressMonitor):
    """Class used to report task progress via RSocket"""

    def __init__(self, rsocket_msg_emitter: RsocketMessageEmitter):
        super().__init__(logger=logging.getLogger())
        self.cancelled = False
        self.rsocket_msg_emitter = rsocket_msg_emitter

    def done(self):
        super().done()
        self.rsocket_msg_emitter.emit_progress(self.desc, 1.0)

    def worked(self, doneTicks: int):
        super().worked(doneTicks)
        self.rsocket_msg_emitter.emit_progress(self.desc, self.acc / self.size)

    def check_cancelled(self):
        return self.cancelled

    def set_cancelled(self):
        self.cancelled = True
        super().set_cancelled()
