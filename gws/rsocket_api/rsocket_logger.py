import logging

from gws.rsocket_api.rsocket_message import RsocketMessageEmitter


class LogToRsocketQueueHandler(logging.Handler):
    """
    Logging handler to redirect log message to a queue
    """

    def __init__(self, rsocket_msg_emitter: RsocketMessageEmitter) -> None:
        logging.Handler.__init__(self, logging.INFO)
        self._rsocket_msg_emitter = rsocket_msg_emitter

    def emit(self, record: logging.LogRecord):
        """
        Catch a log message and transform it to a RSocket message
        """
        self._rsocket_msg_emitter.emit_log(log_record=record)
