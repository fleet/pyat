import logging
import threading
import time
from contextvars import ContextVar

from gws.rsocket_api.rsocket_logger import LogToRsocketQueueHandler
from gws.rsocket_api.rsocket_message import RsocketMessageEmitter
from gws.rsocket_api.rsocket_progress import RSocketMonitor
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor

__rsocket_msg_emitter: ContextVar[RsocketMessageEmitter] = ContextVar("RSsocket_message_emitter")
__root_progress_monitor: ContextVar[ProgressMonitor] = ContextVar("Pyat_Progress_Monitor")
# Flag indicating that service is over
__service_finished: ContextVar[threading.Event] = ContextVar("Service_Finished")


def get_service_finished() -> threading.Event:
    """
    Return the instance of the Event
    """
    try:
        return __service_finished.get()
    except LookupError:
        return threading.Event()


def get_rsocket_msg_emitter() -> RsocketMessageEmitter | None:
    """
    Return the instance of the RsocketMessageEmitter
    """
    try:
        return __rsocket_msg_emitter.get()
    except LookupError:
        return None


def get_root_progress_monitor() -> ProgressMonitor:
    """
    Return the instance of the ProgressMonitor
    """
    try:
        return __root_progress_monitor.get()
    except LookupError:
        return DefaultMonitor


def initialize_execution_context(rsocket_msg_emitter: RsocketMessageEmitter) -> None:
    """
    Initialization of all context variables
    """
    __service_finished.set(threading.Event())
    __rsocket_msg_emitter.set(rsocket_msg_emitter)
    __init_logger(rsocket_msg_emitter)
    __init_progress_monitor(rsocket_msg_emitter)


def __init_logger(rsocket_msg_emitter: RsocketMessageEmitter) -> None:
    # Log times in UTC format
    logging.Formatter.converter = time.gmtime
    logging.root.addHandler(LogToRsocketQueueHandler(rsocket_msg_emitter))


def __init_progress_monitor(rsocket_msg_emitter: RsocketMessageEmitter) -> None:
    monitor = RSocketMonitor(rsocket_msg_emitter)
    __root_progress_monitor.set(monitor)
