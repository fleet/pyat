#! /usr/bin/env python3
# coding: utf-8


import json
import logging
import queue
import sys

from pygws.client.payload_interpreters import PayloadInterpreter
from rsocket.helpers import utf8_decode

import gws.rsocket_api.execution_context as exec_ctx
import gws.rsocket_api.rsocket_client as rsocket_client
import pyat.app.application_utils as app_util
from pyat.core.utils.exceptions.exception_list import BadParameter

logger = logging.getLogger(__name__)


class ServiceConfigurationInterpreter(PayloadInterpreter):
    """
    Payload interpreter, reacting to a service launch request.
    """

    def __init__(self, request_queue: queue.Queue[bytearray]):
        self.request_queue = request_queue

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "run_service"

    async def process_payload(self, payload: bytearray) -> None:
        """Receive a bytearray on the configured route."""

        logger.debug("Service request received")
        logger.debug(f"Arguments : {utf8_decode(payload)}")

        # Just put the payload in the queue to trigger the execution of _process_request function
        self.request_queue.put(payload)

        # Leave this function as soon as possible to free the asyncio loop
        return None  # No answer expected by the client


def _process_request(payload: bytearray) -> None:
    try:
        arguments = json.loads(payload)
        if "configuration_file" not in arguments:
            raise BadParameter("No argument configuration_file found in arguments")

        json_configuration_file = arguments["configuration_file"]
        del arguments["configuration_file"]

        monitor = exec_ctx.get_root_progress_monitor()
        service = app_util.extract_function(json_configuration_file)

        # Running the service by
        logger.debug(f"Running service '%s'", service)
        result = app_util.run(arguments, monitor, logger, service, None)
        # Sending the resulting output files
        if result is not None and "outfile" in result:
            rsocket_msg_emitter = exec_ctx.get_rsocket_msg_emitter()
            if rsocket_msg_emitter is not None:
                rsocket_msg_emitter.emit_files(result["outfile"])

        logger.debug(f"End of the service '%s'", service)
    except:
        logger.exception("Service execution failed")


if __name__ == "__main__":
    logging.basicConfig(filename="logs/pooled_service.log", level=logging.INFO, force=True)
    logger.info("Starting RSocket service launcher")

    request_queue: queue.Queue[bytearray] = queue.Queue()

    def wait_for_request_in_queue():
        # Check every 5s if service is over. May happened when RSocket connection is lost
        while not exec_ctx.get_service_finished().is_set():
            try:
                bytearray = request_queue.get(timeout=5)
                _process_request(bytearray)
                return
            except queue.Empty:
                pass

    # Expecting the RSocket port.
    if len(sys.argv) == 2:
        try:
            rsocket_port = int(sys.argv[1])
            logger.info("Listening RSocket TCP port %d", rsocket_port)

            rsocket_client.start(
                socket_port=rsocket_port,
                app_runner=wait_for_request_in_queue,
                payload_interpreters=[
                    ServiceConfigurationInterpreter(request_queue),
                    rsocket_client.PyatCancelPayloadInterpreter(),
                ],
            )
            logger.info("RSocket client stopped")

        except ValueError:
            raise BadParameter("Expecting an integer in argument for the RSocket port")

    else:
        raise BadParameter("Bad number of argument. Expecting the RSocket port")
