import asyncio
import contextvars
import logging
from functools import partial
from typing import Callable, List

import pygws.data_model.rsocket_payload as r_payload
from pygws.client.gws_error import GwsError
from pygws.client.payload_interpreters import (
    CancelPayloadInterpreter,
    PayloadInterpreter,
)
from pygws.client.tcpsocket_client import GwsTcpSocketClient
from rsocket.payload import Payload
from rsocket.rsocket_client import RSocketClient

import gws.rsocket_api.execution_context as exec_ctx
from gws.rsocket_api.rsocket_message import RsocketMessageEmitter

logger = logging.getLogger(__name__)


class PyatCancelPayloadInterpreter(CancelPayloadInterpreter):
    """
    React when cancel is received on LogPayload route
    """

    async def process_payload(self, payload: bytearray):
        """Managing the cancel"""
        logger.debug("Cancellation triggered.")
        exec_ctx.get_root_progress_monitor().set_cancelled()


async def _send_messages_from_queue(
    client: RSocketClient,
    terminate_event: asyncio.Event,  # pylint:disable=unused-argument
    connection_lost_event: asyncio.Event,
    rsocket_queue: asyncio.Queue[Payload],
    app_future: asyncio.Future | None,
):
    """A Web socket client just waits for the end of the connection.
    All the communication is managed by the PayloadInterpreter(s)"""
    logger.debug("Starts sending message")
    try:
        if rsocket_queue is not None:
            while (
                app_future is None
                or not app_future.done()  # Continue till the end of the monitored process
                or not rsocket_queue.empty()  # Continue if messages are pending
            ) and not connection_lost_event.is_set():  # Stop immediately when connection is lost
                if not rsocket_queue.empty():
                    payload = await rsocket_queue.get()
                    rsocket_queue.task_done()
                    if payload is not None:
                        await client.fire_and_forget(payload)
                await asyncio.sleep(0.1)
    except:  # pylint:disable=bare-except
        logger.exception("Error in message")
    logger.debug("Stops sending message")

    # Here, monitored process is done or socket connection is lost
    if not connection_lost_event.is_set():
        # Informs the client of the end of the process
        logger.debug("Sending terminate")
        await client.fire_and_forget(r_payload.make_terminate_payload())
        logger.debug("Terminate sent")

    # Set up the finish flag
    exec_ctx.get_service_finished().set()


async def _open_socket_and_send_messages(
    socket_port: int,
    rsocket_queue: asyncio.Queue[Payload],
    app_future: asyncio.Future | None,
    payload_interpreters: List[PayloadInterpreter],
):
    """
    Open the TCP socket (RSocketClient)
    Listen the queue, extract each message and send them to the TCP socket
    Listen until the pyat script has finished and all messages have been processed
    """
    logger.debug("Opening TCP socket on port %d", socket_port)
    rsocket_client = GwsTcpSocketClient(
        "localhost",
        socket_port,
        process_with_rsocket=partial(_send_messages_from_queue, rsocket_queue=rsocket_queue, app_future=app_future),
    )
    try:
        await rsocket_client.run(payload_interpreters)
    except GwsError:
        logger.exception("Client execution failed")


async def _start(socket_port: int, app_runner: Callable | None, payload_interpreters: List[PayloadInterpreter]):
    """
    The purpose of this function is to launch the script in a thread and, at the same time, manage the communication with the RSocket.
    """
    # First, define the execution context.
    # The context hold all the variables useful for the pyat script.
    rsocket_queue: asyncio.Queue[Payload] = asyncio.Queue()
    rsocket_msg_emitter = RsocketMessageEmitter(rsocket_queue)
    exec_ctx.initialize_execution_context(rsocket_msg_emitter)

    # Launch app in a thread (run_in_executor)
    # The pyat script accesses a copy of the execution context where any modifications
    # it makes will not be visible to any other process using this context
    app_future = None
    if app_runner is not None:
        app_context_vars = contextvars.copy_context()
        func_call = partial(app_context_vars.run, app_runner)
        app_future = asyncio.get_running_loop().run_in_executor(None, func_call)

    # While the script is running, we establish communication via RSocket to transfer messages.
    await _open_socket_and_send_messages(socket_port, rsocket_queue, app_future, payload_interpreters)


def start(
    socket_port: int, app_runner: Callable | None = None, payload_interpreters: List[PayloadInterpreter] | None = None
):
    """
    Entry point of the API, for asynchronous script execution
    Just call _start() in the asyncio loop (asyncio is required for rsocket communication)
    """
    # By default, only client can received only Cancel payload
    if payload_interpreters is None:
        payload_interpreters = [PyatCancelPayloadInterpreter()]
    for payload_interpreter in payload_interpreters:
        logger.debug("Accept payload on route %s", payload_interpreter.get_rsocket_route())

    asyncio.run(_start(socket_port=socket_port, app_runner=app_runner, payload_interpreters=payload_interpreters))
