import asyncio
import json
import logging
from typing import Dict, List

import pygws.data_model.rsocket_payload as r_payload
from rsocket.extensions.helpers import composite, route
from rsocket.payload import Payload


class RsocketMessageEmitter:
    """
    Class used to format and emit a message via the RSocket
    """

    def __init__(self, rsocket_queue: asyncio.Queue[Payload]) -> None:
        self._rsocket_queue = rsocket_queue

    def emit_log(self, log_record: logging.LogRecord):
        """
        Emit a single float to the specified route
        """
        self._rsocket_queue.put_nowait(r_payload.make_log_payload(log_record))

    def emit_files(self, files: List[str]):
        """
        Emit an array of files
        """
        self._rsocket_queue.put_nowait(r_payload.make_files_payload(files))

    def emit_dict(self, obj: Dict | list[Dict]):
        """
        Emits a dictionary as JSON string.
        """
        self._rsocket_queue.put_nowait(Payload(json.dumps(obj).encode(), composite(route("StringPayload"))))

    def emit_string(self, string: str):
        """
        Emits a string.
        """
        self._rsocket_queue.put_nowait(Payload(string.encode(), composite(route("StringPayload"))))

    def emit_strings(self, strings: List[str]):
        """
        Emit an list of strings
        """
        self._rsocket_queue.put_nowait(r_payload.make_strings_payload(strings))

    def emit_progress(self, task_name: str, progress: float):
        """
        Emit the process progression.
        progress : progression from 0.0 to 1.0
        """
        self._rsocket_queue.put_nowait(r_payload.make_progress_payload(task_name, progress))

    def emit_map_of_double(self, values: Dict[str, float]):
        """
        Emit a spatial resolution payload
        """
        self._rsocket_queue.put_nowait(r_payload.make_map_of_float_payload(values))
