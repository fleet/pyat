#! /usr/bin/env python3
# coding: utf-8


import logging
import os
import sys
from typing import Dict, NamedTuple

from mhkit import dolfyn
import flatbuffers
import numpy as np
import pygws.data_model.flatbuffers.protocol_adcp_generated as proto_adcp
from rsocket.helpers import utf8_decode

import gws.rsocket_api.rsocket_client as rsocket_client
from pyat.app.application_utils import load_json_file
from pyat.core.utils.exceptions.exception_list import BadParameter

logger = logging.getLogger(__name__)
log_head = os.path.basename(__file__)


class AdcpDriver:
    def __init__(self, file_path: str):
        self.file_path = file_path
        self.ds = dolfyn.read(self.file_path)

    def get_time(self) -> np.ndarray:
        return self.ds["time"].to_numpy()

    def get_range(self) -> np.ndarray:
        return self.ds["range"].astype(np.float32).to_numpy() * -1

    def get_latitude(self) -> np.ndarray:
        return self.ds["latitude_gps"].astype(float).to_numpy()

    def get_longitude(self) -> np.ndarray:
        return self.ds["longitude_gps"].astype(float).to_numpy()

    def get_northward_velocity(self) -> np.ndarray:
        """
        return the water velocity, north direction.
        This is a 2 dimensions array : [range][time]
        """
        return self.ds["vel"].sel(dir="N").astype(np.float32).to_numpy()

    def get_eastward_velocity(self) -> np.ndarray:
        """
        return the water velocity, east direction.
        This is a 2 dimensions array : [range][time]
        """
        return self.ds["vel"].sel(dir="E").astype(np.float32).to_numpy()

    def close(self):
        self.ds.close()


class AdcpData(NamedTuple):
    """
    Class representing all data of an ADCP file
    """

    file_path: str
    time: np.ndarray
    latitude: np.ndarray
    longitude: np.ndarray
    range: np.ndarray
    time_index: np.ndarray
    eastward_velocity: np.ndarray
    northward_velocity: np.ndarray


def _parse_adcp_file(adcp_file_path: str) -> AdcpData:
    """
    returns a dict of current data contained in the specified file
    """
    adcp_driver = AdcpDriver(adcp_file_path)

    try:
        nv = adcp_driver.get_northward_velocity()
        ev = adcp_driver.get_eastward_velocity()
        ev_shape = ev.shape
        # Array with True values when eastward_velocity and northward_velocity != NaN
        not_nan_velocity = np.isfinite(ev) & np.isfinite(nv)

        # Build an array of range indexes
        ranges = adcp_driver.get_range()
        ranges = np.tile(ranges, (ev_shape[1], 1)).T
        ranges = ranges[not_nan_velocity]

        # Build an array of time indexes
        time_index = np.tile(np.arange(ev_shape[1]), (ev_shape[0], 1))
        time_index = time_index[not_nan_velocity]

        return AdcpData(
            file_path=adcp_file_path,
            time=adcp_driver.get_time(),
            latitude=adcp_driver.get_latitude(),
            longitude=adcp_driver.get_longitude(),
            range=ranges,
            time_index=time_index,
            eastward_velocity=ev[not_nan_velocity],
            northward_velocity=nv[not_nan_velocity],
        )
    finally:
        adcp_driver.close()


# Cache of ADCP file data. File were parsed with Dolfyn and processed with a AdcpDriver
_ADCP_FILE_CACHE: Dict[str, AdcpData] = {}


def _open_adcp_file(adcp_file_path: str) -> AdcpData:
    """
    Open an Adcp file.
    If present in the cache, return the AdcpData directly.
    Otherwise, parse the file with Dolfyn, complete the cache and return the AdcpData
    """
    logger.info(f"{log_head} : Nb of the ADCP file in cache {len(_ADCP_FILE_CACHE)}")

    if adcp_file_path not in _ADCP_FILE_CACHE:
        try:
            logger.info(f"{log_head} : Parsing the ADCP file {adcp_file_path}")
            adcp_data = _parse_adcp_file(adcp_file_path)
            _ADCP_FILE_CACHE[adcp_file_path] = adcp_data
        except Exception as e:
            raise IOError(f"Not an ADCP file ({adcp_file_path})") from e

    return _ADCP_FILE_CACHE[adcp_file_path]


def _create_adcp_file_info(adcp_data: AdcpData) -> bytearray:
    """
    Build an AdcpFileInfo from the AdcpData
    """
    builder = flatbuffers.Builder()
    file_path = builder.CreateString(adcp_data.file_path)
    proto_adcp.AdcpFileInfoStart(builder)
    proto_adcp.AdcpFileInfoAddFilePath(builder, file_path)

    proto_adcp.AdcpFileInfoAddVectorCount(builder, len(adcp_data.eastward_velocity))
    proto_adcp.AdcpFileInfoAddDatetimeMin(builder, int(adcp_data.time[0].astype(np.int64) / 1e6))
    proto_adcp.AdcpFileInfoAddDatetimeMax(builder, int(adcp_data.time[-1].astype(np.int64) / 1e6))
    proto_adcp.AdcpFileInfoAddLatitudeMin(builder, adcp_data.latitude.min())
    proto_adcp.AdcpFileInfoAddLatitudeMax(builder, adcp_data.latitude.max())
    proto_adcp.AdcpFileInfoAddLongitudeMin(builder, adcp_data.longitude.min())
    proto_adcp.AdcpFileInfoAddLongitudeMax(builder, adcp_data.longitude.max())
    proto_adcp.AdcpFileInfoAddRangeMin(builder, adcp_data.range.min())
    proto_adcp.AdcpFileInfoAddRangeMax(builder, adcp_data.range.max())
    adcp_file_info = proto_adcp.AdcpFileInfoEnd(builder)
    builder.Finish(adcp_file_info)
    logger.info(f"{log_head} : AdcpFileInfo created")

    return builder.Output()


def _apply_filtering(adcpData: AdcpData, range_min: float, range_max: float) -> AdcpData:
    """
    Generate a new AdcpData by filtering current of the AdcpData source
    """
    logger.info(f"{log_head} : Filtering vectors, range between {range_min} and {range_max}")

    return AdcpData(
        file_path=adcpData.file_path,
        time=adcpData.time,
        latitude=adcpData.latitude,
        longitude=adcpData.longitude,
        range=adcpData.range[(adcpData.range >= range_min) & (adcpData.range <= range_max)],
        time_index=adcpData.time_index[(adcpData.range >= range_min) & (adcpData.range <= range_max)],
        eastward_velocity=adcpData.eastward_velocity[(adcpData.range >= range_min) & (adcpData.range <= range_max)],
        northward_velocity=adcpData.northward_velocity[(adcpData.range >= range_min) & (adcpData.range <= range_max)],
    )


def _apply_sampling(adcpData: AdcpData, sampling: int) -> AdcpData:
    """
    Generate a new AdcpData by sampling current of the AdcpData source
    """
    logger.info(f"{log_head} : Sampling {sampling} vectors expected")

    ind = np.arange(0, len(adcpData.time_index), len(adcpData.time_index) / sampling, int)
    return AdcpData(
        file_path=adcpData.file_path,
        time=adcpData.time,
        latitude=adcpData.latitude,
        longitude=adcpData.longitude,
        range=adcpData.range[ind],
        time_index=adcpData.time_index[ind],
        eastward_velocity=adcpData.eastward_velocity[ind],
        northward_velocity=adcpData.northward_velocity[ind],
    )


class OpenAdcpFileInterpreter:
    """
    Handling an open request
    """

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "open_adcp_file"

    async def process_payload(self, payload: bytearray) -> bytearray | None:
        """Process payload"""
        adcp_file_path = utf8_decode(payload)
        logger.info("Opening ADCP file %s", adcp_file_path)
        adcp_data = _open_adcp_file(adcp_file_path)
        return _create_adcp_file_info(adcp_data)


class CloseAdcpFileInterpreter:
    """
    Handling a close request
    """

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "close_adcp_file"

    async def process_payload(self, payload: bytearray) -> bytearray | None:
        """Process payload"""
        adcp_file_path = utf8_decode(payload)
        if adcp_file_path in _ADCP_FILE_CACHE:
            logger.info(f"{log_head} : closing ADCP file {adcp_file_path}")
            del _ADCP_FILE_CACHE[adcp_file_path]
        return None


class AdcpCurrentInterpreter:
    """
    Return the current data for the file specified in the payload (expecting a AdcpCurrentRequest)
    """

    def get_rsocket_route(self) -> str:
        """Route of payload"""
        return "get_adcp_current"

    async def process_payload(self, payload: bytearray) -> bytearray | None:
        """Managing the AdcpCurrentRequest payload"""
        request = proto_adcp.AdcpCurrentRequest.GetRootAs(payload)
        adcp_file_path = utf8_decode(request.FilePath())
        adcp_data = _open_adcp_file(adcp_file_path)

        # Apply filtering
        range_min = request.RangeMin()
        range_max = request.RangeMax()
        if range_min is not None and range_max is not None:
            adcp_data = _apply_filtering(adcp_data, range_min, range_max)

        # Apply sampling
        sampling = request.Sampling()
        if len(adcp_data.time_index) > sampling > 0:
            adcp_data = _apply_sampling(adcp_data, sampling)

        builder = flatbuffers.Builder()
        file_path = builder.CreateString(adcp_file_path)
        time_values = builder.CreateNumpyVector(adcp_data.time.ravel().astype(np.int64))
        latitude_values = builder.CreateNumpyVector(adcp_data.latitude.ravel())
        longitude_values = builder.CreateNumpyVector(adcp_data.longitude.ravel())
        range_values = builder.CreateNumpyVector(adcp_data.range.ravel())
        time_index_values = builder.CreateNumpyVector(adcp_data.time_index.ravel())
        eastward_velocity_values = builder.CreateNumpyVector(adcp_data.eastward_velocity.ravel())
        northward_velocity_values = builder.CreateNumpyVector(adcp_data.northward_velocity.ravel())

        proto_adcp.AdcpCurrentStart(builder)
        proto_adcp.AdcpCurrentAddFilePath(builder, file_path)
        proto_adcp.AdcpCurrentAddVectorCount(builder, len(adcp_data.eastward_velocity))

        proto_adcp.AdcpCurrentAddTime(builder, time_values)
        proto_adcp.AdcpCurrentAddLatitude(builder, latitude_values)
        proto_adcp.AdcpCurrentAddLongitude(builder, longitude_values)
        proto_adcp.AdcpCurrentAddRange(builder, range_values)
        proto_adcp.AdcpCurrentAddTimeIndex(builder, time_index_values)
        proto_adcp.AdcpCurrentAddEastwardVelocity(builder, eastward_velocity_values)
        proto_adcp.AdcpCurrentAddNorthwardVelocity(builder, northward_velocity_values)
        builder.Finish(proto_adcp.AdcpCurrentEnd(builder))
        logger.info(f"{log_head} : AdcpCurrent created with {len(adcp_data.eastward_velocity)} vectors")

        return builder.Output()


if __name__ == "__main__":
    logging.basicConfig(filename="logs/adcp_driver.log", level=logging.DEBUG, force=True)
    logger.info("Starting ADCP driver")
    # Expecting the JSON configuration file.
    if len(sys.argv) == 2:
        # pylint: disable=unbalanced-tuple-unpacking
        arguments = load_json_file(sys.argv[1])
        if "rsocket_port" not in arguments:
            raise BadParameter("Socket port not in configuration file. Execution aborted")

        logger.info("TCP socket port is %d", arguments["rsocket_port"])
        payload_interpreters = [OpenAdcpFileInterpreter(), AdcpCurrentInterpreter(), CloseAdcpFileInterpreter()]
        rsocket_client.start(arguments["rsocket_port"], payload_interpreters=payload_interpreters)
    else:
        raise BadParameter("Bad number of argument for globe - python process")
