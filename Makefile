test:
	pytest -vv --ignore=pyat/test/xsf --ignore=pyat/playground

test-playground:
	pytest -v --ignore=pyat/test/xsf

jupyter:
	jupyter lab --notebook-dir=.

lint:
	pylint pyat
black:
	black pyat
black-all:
	git br -D lbo-black
	git co -b lbo-black
	black pyat/
	git co pyat/playground
	git a pyat*py
	git cim "black all"
	git ps -fu origin lbo-black
mypy:
	mypy pyat

compile:
	python setup.py build_ext --inplace



