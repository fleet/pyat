import numpy as np
import netCDF4 as nc
from scipy.spatial.transform import Rotation


rot=Rotation.from_euler("zyx",(90,0,0),degrees=True)
t=rot.as_dcm()

#check if y become x
res=np.dot(t,[0,1,0])
print(res)
print(rot)
