import time

import netCDF4 as nc
import numpy as np


class NetcdfFormatter:
    """
       Utility class use to modify and test adcp file in order to define the expected netcdf format
    """

    def __init__(self):
        """
            constructor initialize global settings
        """
        self.make_fake_data = False  # indicate if we generate a small example file or convert a tile

    def migrate(self, file_name_in, file_name_out):
        """
            main entry point for this class, will migrate an EMODnet netcdf format to a new netcdf format
        :param file_name_in:
        :param file_name_out:
        :return:
        """
        start = time.time()
        print("start migration of file {} to file {}".format(file_name_in, file_name_out))
        self.file_name_in = file_name_in
        self.file_name_out = file_name_out
        self.input = nc.Dataset(self.file_name_in)

        self.output = nc.Dataset(file_name_out, "w", format="NETCDF4")
        start = time.time()

        self.process_root()
        self.process_platform()
        self.process_annotation()
        self.process_environment()
        self.process_ADCP()
        self.output.close()
        self.input.close()

        print("end of migration for file {0} , {1} seconds elapsed".format(file_name_out, str(time.time() - start)))

    def process_root(self) -> None:
        """
            process root node, duplicate attributes
        :return:
        """
        self.__copy_all_attribute(self.input, self.output)

    def __copy_attribute(self, name, dataset_in, dataset_out):
        dataset_out.setncattr(name, dataset_in.__dict__.get(name))

    def __copy_all_attribute(self, dataset_in, dataset_out):
        """Copy all attributes from one Dataset to another one"""
        for att in dataset_in.ncattrs():
            self.__copy_attribute(att, dataset_in, dataset_out)

    def __copy_dimension(self, name, dataset_in, dataset_out):
        dataset_out.createDimension(name, size=dataset_in.dimensions[name].size)

    # noinspection PyProtectedMember
    def __copy_variable(self, name, dataset_in, dataset_out):
        v = dataset_in.variables[name]
        if not v._isvlen or v.dtype == str:
            created = dataset_out.createVariable(name, datatype=v.dtype, dimensions=v.dimensions)
            created[:] = v[:]
            self.__copy_all_attribute(v, created)
        else:
            # there is only one vlen type
            created = dataset_out.createVariable(name, datatype=self.sample_t, dimensions=v.dimensions)
            created[:] = v[:]
            self.__copy_all_attribute(v, created)

    def __copy_group(self, name, dataset_in, dataset_out):
        lin = dataset_in.groups[name]
        lout = dataset_out.createGroup(name)
        self.__copy_all_attribute(lin, lout)
        for dim in lin.dimensions:
            self.__copy_dimension(dim, lin, lout)
        for var in lin.variables:
            self.__copy_variable(var, lin, lout)

    def process_platform(self) -> None:
        #  self.__copy_group("Platform",self.input,self.output)
        input_platform = self.input.groups["Platform"]
        self.output.createGroup("Platform")
        output_platform = self.output.groups["Platform"]
        output_platform.createDimension("MRU", 1)
        output_platform.createDimension("position", 1)
        output_platform.createDimension("transducer", 1)
        MRU_id = "001"
        Position_id = MRU_id
        positionGrp = output_platform.createGroup("Positions").createGroup(MRU_id)
        attitudeGrp = output_platform.createGroup("Attitudes").createGroup(Position_id)

        enum_dict = {"receive_only": 0, "transmit_only": 1, "monostatic": 3}
        transducer_type_t = output_platform.createEnumType(np.int8, "transducer_type_t", enum_dict)

        # recreate positions and MRUs variables
        v = output_platform.createVariable("MRU_offset_x", "f8", ("MRU"))
        v.long_name = (
            "Distance along the x-axis from the platform "
            "coordinate system origin to the motion reference unit sensor origin"
        )
        v.units = "m"
        v[:] = input_platform.variables["MRU_offset_x"][0]

        v = output_platform.createVariable("MRU_offset_y", "f8", ("MRU"))
        v.long_name = (
            "Distance along the y-axis from the platform coordinate "
            "system origin to the motion reference unit sensor origin"
        )
        v.units = "m"
        v[:] = input_platform.variables["MRU_offset_y"][0]

        v = output_platform.createVariable("MRU_offset_z", "f8", ("MRU"))
        v.long_name = (
            "Distance along the z-axis from the platform coordinate "
            "system origin to the motion reference unit sensor origin"
        )
        v.units = "m"
        v[:] = input_platform.variables["MRU_offset_z"][0]

        v = output_platform.createVariable("MRU_rotation_x", "f8", ("MRU"))
        v.long_name = "Extrinsic rotation about the x-axis from the platform to MRU coordinate systems"
        v.units = "arc_degree"
        v[:] = input_platform.variables["MRU_rotation_x"][0]

        v = output_platform.createVariable("MRU_rotation_y", "f8", ("MRU"))
        v.long_name = "Extrinsic rotation about the y-axis from the platform to MRU coordinate systems"
        v.units = "arc_degree"
        v[:] = input_platform.variables["MRU_rotation_y"][0]

        v = output_platform.createVariable("MRU_rotation_z", "f8", ("MRU"))
        v.long_name = "Extrinsic rotation about the z-axis from the platform to MRU coordinate systems"
        v.units = "arc_degree"
        v[:] = input_platform.variables["MRU_rotation_z"][0]

        v = output_platform.createVariable("MRU_ids", str, ("MRU"))
        v.long_name = "MRU serial number or identification name"
        v[0] = MRU_id

        v = output_platform.createVariable("position_ids", str, ("position"))
        v.long_name = "Position serial number or identification name"
        v[0] = Position_id

        v = output_platform.createVariable("position_offset_x", "f8", ("position"))
        v.long_name = (
            "Distance along the x-axis from the platform "
            "coordinate system origin to the latitude/longitude sensor origin"
        )
        v.units = "m"
        v[:] = input_platform.variables["position_offset_x"][0]

        v = output_platform.createVariable("position_offset_y", "f8", "position")
        v.long_name = (
            "Distance along the y-axis from the platform "
            "coordinate system origin to the latitude/longitude sensor origin"
        )
        v.units = "m"
        v[:] = input_platform.variables["position_offset_y"][0]

        v = output_platform.createVariable("position_offset_z", "f8", ("position"))
        v.long_name = (
            "Distance along the z-axis from the platform "
            "coordinate system origin to the latitude/longitude sensor origin"
        )
        v.units = "m"
        v[:] = input_platform.variables["position_offset_z"][0]

        v = output_platform.createVariable("transducer_id", str, "transducer")
        v.long_name = "Transducer serial number or identification name"
        v[0] = "EC150"

        v = output_platform.createVariable("transducer_rotation_x", "f8", ("transducer"))
        v.long_name = (
            "Extrinsic angular rotation about the x-axis "
            "from the transducer zero angle to the coordinate system origin zero angle."
        )
        v.units = "arc_degree"
        v[:] = 0

        v = output_platform.createVariable("transducer_rotation_y", "f8", ("transducer"))
        v.long_name = (
            "Extrinsic angular rotation about the y-axis "
            "from the transducer zero angle to the coordinate system origin zero angle."
        )
        v.units = "arc_degree"
        v[:] = 0

        v = output_platform.createVariable("transducer_rotation_z", "f8", ("transducer"))
        v.long_name = (
            "Extrinsic angular rotation about the z-axis "
            "from the transducer zero angle to the coordinate system origin zero angle."
        )
        v.units = "arc_degree"
        v[:] = 0

        v = output_platform.createVariable("transducer_offset_x", "f8", ("transducer"))
        v.long_name = "x-axis distance from the platform coordinate system origin to the sonar transducer"
        v.units = "m"
        v[:] = input_platform.variables["transducer_offset_x"][0]

        v = output_platform.createVariable("transducer_offset_y", "f8", ("transducer"))
        v.long_name = "y-axis distance from the platform coordinate system origin to the sonar transducer"
        v.units = "m"
        v[:] = input_platform.variables["transducer_offset_y"][0]

        v = output_platform.createVariable("transducer_offset_z", "f8", ("transducer"))
        v.long_name = "z-axis distance from the platform coordinate system origin to the sonar transducer"
        v.units = "m"
        v[:] = input_platform.variables["transducer_offset_z"][0]

        # move position data to position subgroup
        self.__copy_dimension("time1", input_platform, positionGrp)
        self.__copy_variable("distance", input_platform, positionGrp)

        self.__copy_variable("heading", input_platform, positionGrp)
        self.__copy_variable("latitude", input_platform, positionGrp)
        self.__copy_variable("longitude", input_platform, positionGrp)
        self.__copy_variable("speed_ground", input_platform, positionGrp)
        self.__copy_variable("speed_relative", input_platform, positionGrp)
        self.__copy_variable("vertical_offset", input_platform, positionGrp)

        self.__copy_group("NMEA", self.input.groups["Platform"], positionGrp)

        self.__copy_dimension("time1", input_platform, attitudeGrp)

        self.__copy_variable("pitch", input_platform, attitudeGrp)
        self.__copy_variable("roll", input_platform, attitudeGrp)

    def process_environment(self) -> None:
        self.__copy_group("Environment", self.input, self.output)

    def process_ADCP(self) -> None:
        adcp_in = self.input.groups["adcp"]
        adcp_beam_in = adcp_in.groups["BeamData"]
        adcp_current_in = adcp_in.groups["CurrentVelocity"]
        # create a group sonar, then beam
        sonarGrp = self.output.createGroup("Sonar")
        sonarGrp.sonar_manufacturer = "Simrad"
        sonarGrp.sonar_model = "TBD:sonar_model"
        sonarGrp.sonar_serial_number = "TBD:sonar_serial_number"
        sonarGrp.sonar_software_version = "1.13.7058.41514"
        sonarGrp.sonar_type = "adcp"
        sonarGrp.sonar_software_name = "EK80"
        # create sonar enum types
        enum_dict = {"not_stabilised": 0, "stabilised": 1}
        beam_stabilisation_t = sonarGrp.createEnumType(np.int8, "beam_stabilisation_t", enum_dict)

        enum_dict = {"single": 0, "split_aperture": 1}
        beam_t = sonarGrp.createEnumType(np.int8, "beam_t", enum_dict)

        enum_dict = {"type_1": 1, "type_2": 2}
        conversion_equation_t = sonarGrp.createEnumType(np.int8, "conversion_equation_t", enum_dict)

        enum_dict = {"CW": 0, "LFM": 1, "HFM": 2}
        transmit_t = sonarGrp.createEnumType(np.int8, "transmit_t", enum_dict)

        # create sonar vlen type
        sample_t = sonarGrp.createVLType(np.float32, "sample_t")

        # should be recorded in a group hierarchy but it is good enough
        self.sample_t = sample_t

        # create subgroup beam_group
        beamGrp = sonarGrp.createGroup("Beam_group1")
        self.beamGrp = beamGrp

        # create subgroup adcp
        adcpGrp = beamGrp.createGroup("adcp")
        adcpGrp.createGroup("Vendor_specific")
        self.adcpGrp = adcpGrp

        # create dimension
        beamGrp.createDimension("beam", adcp_in.dimensions["beam"].size)
        beamGrp.createDimension("ping_time", adcp_in.dimensions["ping_time"].size)

        self.__copy_variable("ping_time", adcp_in, beamGrp)
        self.__copy_variable("transmit_bandwidth", adcp_in, beamGrp)
        self.__copy_variable("transmit_duration_equivalent", adcp_in, beamGrp)
        self.__copy_variable("transmit_duration_nominal", adcp_in, beamGrp)
        self.__copy_variable("transmit_frequency_start", adcp_in, beamGrp)
        self.__copy_variable("transmit_frequency_stop", adcp_in, beamGrp)
        self.__copy_variable("transmit_power", adcp_in, beamGrp)
        self.__copy_variable("transmit_source_level", adcp_in, beamGrp)
        self.__copy_variable("transmit_type", adcp_in, beamGrp)
        self.__copy_variable("water_temperature_at_transducer", adcp_in, beamGrp)
        self.__copy_variable("backscatter_i", adcp_beam_in, beamGrp)
        self.__copy_variable("backscatter_r", adcp_beam_in, beamGrp)
        self.__copy_variable("beam_direction_x", adcp_beam_in, beamGrp)
        self.__copy_variable("beam_direction_y", adcp_beam_in, beamGrp)
        self.__copy_variable("beam_direction_z", adcp_beam_in, beamGrp)
        self.__copy_variable("beam_stabilisation", adcp_beam_in, beamGrp)
        self.__copy_variable("beamwidth_receive_major", adcp_beam_in, beamGrp)
        self.__copy_variable("beamwidth_receive_minor", adcp_beam_in, beamGrp)
        self.__copy_variable("beamwidth_transmit_major", adcp_beam_in, beamGrp)
        self.__copy_variable("blanking_interval", adcp_beam_in, beamGrp)
        self.__copy_variable("sample_interval", adcp_beam_in, beamGrp)
        self.__copy_variable("sample_time_offset", adcp_beam_in, beamGrp)
        self.__copy_variable("time_varied_gain", adcp_beam_in, beamGrp)
        self.__copy_variable("transducer_gain", adcp_beam_in, beamGrp)

        # create platform_variables
        beamGrp.createVariable(
            "platform_heading", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        beamGrp.createVariable(
            "platform_latitude", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        beamGrp.createVariable(
            "platform_longitude", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        beamGrp.createVariable(
            "platform_pitch", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        beamGrp.createVariable(
            "platform_roll", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        beamGrp.createVariable(
            "platform_vertical_offset", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        beamGrp.createVariable(
            "sound_speed_at_transducer", "f8", ("ping_time")
        ).comment = "To be refined, see SonarNetcdf convention"

        # adcp Group
        self.__copy_variable("back_scatter_at_bottom_i", adcp_beam_in, adcpGrp)
        self.__copy_variable("back_scatter_at_bottom_r", adcp_beam_in, adcpGrp)
        self.__copy_variable("correlation", adcp_beam_in, adcpGrp)
        self.__copy_variable("correlation_at_bottom", adcp_beam_in, adcpGrp)
        self.__copy_variable("slant_range_to_bottom", adcp_beam_in, adcpGrp)
        self.__copy_variable("velocity", adcp_beam_in, adcpGrp)

        # create sampling variable definition
        self.__copy_variable("bin_length", adcp_current_in, adcpGrp)

        self.__copy_variable("bottom_track_velocity_vessel_x", adcp_current_in, adcpGrp)
        self.__copy_variable("bottom_track_velocity_vessel_y", adcp_current_in, adcpGrp)
        self.__copy_variable("bottom_track_velocity_vessel_z", adcp_current_in, adcpGrp)

        self.__copy_variable("current_velocity_geographical_down", adcp_current_in, adcpGrp)
        self.__copy_variable("current_velocity_geographical_east", adcp_current_in, adcpGrp)
        self.__copy_variable("current_velocity_geographical_north", adcp_current_in, adcpGrp)

        self.__copy_variable("depth_first_sample_center", adcp_current_in, adcpGrp)

        # ping_averaged => rien compris
        self.__copy_variable("ping_averaged", adcp_current_in, adcpGrp)
        self.__copy_variable("quality", adcp_current_in, adcpGrp)
        self.__copy_variable("vertical_sample_interval", adcp_current_in, adcpGrp)

        # group Mean_current
        mean_group = adcpGrp.createGroup("Mean_current")
        mean_dimension = adcpGrp.createDimension("mean_time", adcp_in.dimensions["ping_time"].size / 4)
        mean_group.createVariable("bin_length", "f8", ("mean_time")).comment = "To be defined, mean of above value"
        mean_group.createVariable(
            "bottom_track_velocity_vessel_x", "f8", ("mean_time")
        ).comment = "To be defined, mean of above value"
        mean_group.createVariable(
            "bottom_track_velocity_vessel_y", "f8", ("mean_time")
        ).comment = "To be defined, mean of above value"
        mean_group.createVariable(
            "bottom_track_velocity_vessel_z", "f8", ("mean_time")
        ).comment = "To be defined, mean of above value"

        mean_group.createVariable(
            "current_velocity_geographical_down", datatype=self.sample_t, dimensions=("mean_time"),
        ).comment = "To be defined, mean of above value"
        mean_group.createVariable(
            "current_velocity_geographical_east", datatype=self.sample_t, dimensions=("mean_time"),
        ).comment = "To be defined, mean of above value"
        mean_group.createVariable(
            "current_velocity_geographical_north", datatype=self.sample_t, dimensions=("mean_time"),
        ).comment = "To be defined, mean of above value"

        # create platform_variables
        mean_group.createVariable(
            "mean_platform_heading", "f8", ("mean_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        mean_group.createVariable(
            "mean_platform_latitude", "f8", ("mean_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        mean_group.createVariable(
            "mean_platform_longitude", "f8", ("mean_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        mean_group.createVariable(
            "mean_platform_pitch", "f8", ("mean_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        mean_group.createVariable(
            "mean_platform_roll", "f8", ("mean_time")
        ).comment = "To be refined, see SonarNetcdf convention"
        mean_group.createVariable(
            "mean_platform_vertical_offset", "f8", ("mean_time")
        ).comment = "To be refined, see SonarNetcdf convention"

    def process_annotation(self) -> None:
        self.__copy_group("Annotation", self.input, self.output)


if __name__ == "__main__":
    input_file = "D:/data/EC150-ADCP/Example files for Ifremer/EC150-ADCP-Sample-D20190502-T053046.nc"
    output_file = "D:/data/EC150-ADCP/Example files for Ifremer/EC150-ADCP-Sample-D20190502-T053046_EXPECTED.nc"

    app = NetcdfFormatter()
    app.migrate(input_file, output_file)
