import math
import struct
import xml.etree.ElementTree as ET
import tempfile as tmp
import os
import numpy as np
from osgeo import gdal as gdal
import netCDF4 as nc

from pyat.playground.gdf_creator import gdf_constants as cst


class XmlBinaryConverter:
    def __init__(self, input, output):
        self.input = os.path.realpath(input)
        self.base = os.path.basename(input)
        self.input_directory = os.path.dirname(input)
        self.image_dir = os.path.splitext(self.base)[0]
        self.output = os.path.realpath(output)

    def __decode_dimension(self, value: str) -> int:
        """ retrieve a dimension in a dict or try to cast it to int"""
        if value in self.dimensions_dict.keys():
            return self.dimensions_dict[value]
        else:
            return int(value)

    def __decode_struct_type(self, value: str) -> str:
        """ convert ssc type to struct type"""
        if value.lower() == "double":
            return "d"
        elif value.lower() == "single":
            return "f"
        else:
            raise Exception(f"Unsupported type {value}")

    def __decode_np_type(self, value: str):
        """ convert ssc type to numpy"""
        if value.lower() == "double":
            return np.float64
        elif value.lower() == "single":
            return np.float32
        else:
            raise Exception(f"Unsupported type {value}")

    def __parse_signal(self, signal_root, seek_name):
        values = None
        for item in signal_root:
            # parse value depth Top
            item_name = item.find("Name")
            if item_name is None:
                continue
            if item_name.text != seek_name:
                continue
            signal_storage_type = item.find("Storage").text
            signal_filename = item.find("FileName").text
            signal_dimensions = item.find("Dimensions").text
            array_dims = [x.strip() for x in signal_dimensions.split(",")]
            # allocate storage matrix
            cardinality = len(array_dims)
            if cardinality == 1:
                values = np.empty(
                    shape=(self.__decode_dimension(array_dims[0])), dtype=self.__decode_np_type(signal_storage_type)
                )

            elif cardinality == 2:
                values = np.empty(
                    shape=(self.__decode_dimension(array_dims[0]), self.__decode_dimension(array_dims[1])),
                    dtype=self.__decode_np_type(signal_storage_type),
                )
            else:
                raise Exception(f"Unsupported operation, cannot handle {cardinality} dimension matrix")
            # allocate matrix

            datafile = os.path.join(self.input_directory, signal_filename)
            with open(datafile, mode="rb") as f:
                raw = f.read()
                xin = struct.unpack("d" * values.size, raw)
                old_shape = values.shape
                values.resize((values.size))
                values[:] = xin[:]
                values.resize(old_shape)
            return values
        return None

    def crop_values(self, values: np.ndarray):
        # crop image to remove nanand col with invalid values at the end
        mask = ~np.isnan(values)  # check where lines are invalid
        valid_line = mask.any(1)  # boolean vector indicating if there is at least one valid value on a line
        last_line = np.max(np.where(valid_line))  # last line having a valid value
        # crop values to remove invalid lines at the bottom
        values = values[0:last_line, :]
        return values

    def __compute_tiff_file_name(self, index):
        sub_dir = "%03d" % int(index / 100)
        tiff_file_name = "%05d" % index
        tiff_path = os.path.join(self.input_directory, self.image_dir, sub_dir, tiff_file_name + ".tif")
        return tiff_path

    def read_texture(self, index, decimate=False) -> np.ndarray:
        """compute directory storage and read a texture. Do a crop to remove non necessary empty lines"""
        tiff_path = self.__compute_tiff_file_name(index)
        if not os.path.exists(tiff_path):
            tiff_path = self.__compute_tiff_file_name(1)
        #hack for methan files
        if os.path.exists(tiff_path):
            dataset = gdal.Open(tiff_path, gdal.GA_ReadOnly)
            band = dataset.GetRasterBand(1)
            values = band.ReadAsArray()
            return values
        else:
            png_path = os.path.join(self.input_directory, self.image_dir,  "Reflectivity_1.png" )
            dataset = gdal.Open(png_path, gdal.GA_ReadOnly)
            band = dataset.GetRasterBand(1)
            band2 = dataset.GetRasterBand(2)
            band3 = dataset.GetRasterBand(3)
            values = band.ReadAsArray()
            values2 = band2.ReadAsArray()
            values3 = band3.ReadAsArray()
            # try to find back value, we do not really care for exact value
            v=12301* (values+values2*255+values3*255*255)/(255*2)+23
            if decimate:
                return np.array((v[0,:],v[40,:],v[74,:]))
            else:
                return v

    def convert(self):
        # parse a XML binary file containing slices
        root = ET.parse(self.input).getroot()
        signal = root.find("Signals")
        images = root.find("Images")
        dimensions = root.find("Dimensions")

        # parse dimensions
        self.dimensions_dict = dict()
        slice_dim = dimensions.find("nbSlices")
        slice_dim = int(slice_dim.text)
        self.dimensions_dict["nbSlices"] = slice_dim

        ping_count = dimensions.find("nbPings")
        ping_count = int(ping_count.text)
        self.dimensions_dict["nbPings"] = ping_count

        value_depth_top = self.__parse_signal(signal, seek_name="DepthTop")
        value_depth_bottom = self.__parse_signal(signal, seek_name="DepthBottom")
        value_latitude_top = self.__parse_signal(images, seek_name="LatitudeTop")
        value_longitude_top = self.__parse_signal(images, seek_name="LongitudeTop")

        # for item in images:  # images are more to be considered as 2D signals
        #     # print(item.tag)
        #     # print(item.attrib)
        #     filename = item.find("FileName")
        #     name = item.find("Name")
        #     print(filename.text)

        with nc.Dataset(self.output, mode="w") as out:
            # declare type
            out.dataset_type = cst.RootGrp.TYPE_FLYTEXTURE
            out.history = "Created via conversion from " + self.input

            datalayer_dim = out.createDimension(cst.RootGrp.DATALAYER_COUNT, 2)  # we force layer count to 2
            auto_datalayer_name = os.path.splitext(self.base)[0].split("_")
            auto_datalayer_name = auto_datalayer_name[len(auto_datalayer_name) - 1]
            v = out.createVariable(
                varname=cst.RootGrp.DATALAYER_DISPLAY_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT), datatype=str
            )
            auto_datalayer_name_2 = auto_datalayer_name+"_noised"
            v[0] = "->" + auto_datalayer_name + "<-"
            v[1] = "-> noised  <-"

            v = out.createVariable(
                varname=cst.RootGrp.DATALAYER_VARIABLES_NAMES, dimensions=(cst.RootGrp.DATALAYER_COUNT), datatype=str
            )
            v[0] = auto_datalayer_name
            v[1] = auto_datalayer_name_2

            for slice in range(1, slice_dim + 1):
                slice_index = slice - 1
                digit_count = int(math.log10(slice_dim)) + 1
                # create one group per slice
                format_str = "%0" + str(digit_count) + "d"
                groupName = format_str % slice
                slice_group = out.createGroup(groupName)
                # group metadata
                slice_group.is_valid = 1
                slice_group.long_name = "Slice number " + str(slice)
                values = self.read_texture(slice,decimate=True)

                (valueLatitudeTop, valueLongitudeTop, valueDepthTop, valueDepthBottom) = (value_latitude_top[slice_index], value_longitude_top[slice_index], value_depth_top, value_depth_bottom)
                pingCount = valueLatitudeTop.shape[0]
                if values.shape[1] != pingCount:
                    raise Exception("Texture are supposed to have the same length as position vectors")
                # create texture
                slice_group.createDimension(cst.RootGrp.DataLayerGrp.LENGTH_DIM, values.shape[1])
                slice_group.createDimension(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, values.shape[0])
                v = slice_group.createVariable(
                    varname=auto_datalayer_name,
                    dimensions=(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, cst.RootGrp.DataLayerGrp.LENGTH_DIM),
                    datatype=float,zlib=True
                )
                v[:] = values[:]

                v = slice_group.createVariable(
                    varname=auto_datalayer_name_2,
                    dimensions=(cst.RootGrp.DataLayerGrp.HEIGHT_DIM, cst.RootGrp.DataLayerGrp.LENGTH_DIM),
                    datatype=float, zlib=True
                )
                v[:] = values[:,::-1]
                # create position vector
                slice_group.createDimension(cst.RootGrp.DataLayerGrp.VECTOR_DIM, 2)
                slice_group.createDimension(cst.RootGrp.DataLayerGrp.POSITION_DIM, pingCount)

                latitude = slice_group.createVariable(
                    varname=cst.RootGrp.DataLayerGrp.LATITUDE_VARIABLE,
                    dimensions=(
                    cst.RootGrp.DataLayerGrp.VECTOR_DIM, cst.RootGrp.DataLayerGrp.POSITION_DIM),
                    datatype="f8",
                )
                latitude[0, :] = valueLatitudeTop[:]
                latitude[1, :] = valueLatitudeTop[:]

                longitude = slice_group.createVariable(
                    varname=cst.RootGrp.DataLayerGrp.LONGITUDE_VARIABLE,
                    dimensions=(
                    cst.RootGrp.DataLayerGrp.VECTOR_DIM, cst.RootGrp.DataLayerGrp.POSITION_DIM),
                    datatype="f8",
                )
                longitude[0, :] = valueLongitudeTop[:]
                longitude[1, :] = valueLongitudeTop[:]

                depth = slice_group.createVariable(
                    varname=cst.RootGrp.DataLayerGrp.ELEVATION,
                    dimensions=(
                    cst.RootGrp.DataLayerGrp.VECTOR_DIM, cst.RootGrp.DataLayerGrp.POSITION_DIM),
                    datatype="f4",
                )
                depth[0, :] = valueDepthTop[0, :]

                depth[1, :] = valueDepthBottom[0, :]





if __name__ == "__main__":
    print("starting xml converter")
    out = tmp.mktemp(suffix=".nc", prefix="FlyTexture", dir="d://tmp")

    print(f"creating file {out}")
    input = "D:/tmp/MAYOBS Coupe longitudinale SS/0198_20190510_111055_EM122_Marion_Dufresne_WCSliceRaw.xml"
    #input = "D:/tmp/Donnees Concentration Methane/TEST-CH4.xml"
    converter = XmlBinaryConverter(input, out)
    converter.convert()

