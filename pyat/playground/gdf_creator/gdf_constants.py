class RootGrp:
    GROUP_PATH = "/"
    # Attributes names
    DATASET_TYPE = "dataset_type"

    TYPE_FLYTEXTURE = "FlyTexture"
    TYPE_GROUNDTEXTURE = "GroundTexture"
    TYPE_POINTCLOUD = "PointCloud"

    HISTORY_NAME = "history"

    DATALAYER_COUNT = "datalayer_count"
    DATALAYER_DISPLAY_NAMES = "datalayer_display_name"
    DATALAYER_VARIABLES_NAMES = "datalayer_variable_name"

    class DataLayerGrp:
        VALIDITY_ATTRIBUTE = "is_valid"
        OBS_DIM = "obs"  # PointCloud only
        LATITUDE_VARIABLE = "latitude"  # PointCloud only
        LONGITUDE_VARIABLE = "longitude"  # PointCloud only
        ELEVATION = "elevation"  # PointCloud only

        VECTOR_DIM = "vector"
        LENGTH_DIM = "length"
        HEIGHT_DIM = "height"
        POSITION_DIM = "position"




