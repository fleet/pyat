import glob

from playground.stage.utils.nc_file_cache import XSFFile

# identify the files to process
input_directory = "W:\traitement_Campagnes/Stage_Kevin/backscatter/boite1"

sonarscope_equation = "/SurveyReport/all/SurveyReport/stage/SonarEquation/0032_20180914_183119_ShipName/"
# set processing parameters
output_dir = "d:/tmp"


def _get_files():
    """
    parse input directory sample and return the list of xsf files
    :return: a list of XSFFiles
    """
    file_names = glob.glob(input_directory + "/backscatter/boite1/*.xsf")
    # Récupère les données du xsf
    files = []
    for file_name in file_names:
        # we remove the tranverse file
        if "0047_20180914_222017_ShipName" not in file_name:
            files.append(XSFFile(file_name))
    return files


files = _get_files()


def get_single_file():
    """
    Return the single file used for test
    """
    single_file = [value for value in files if value.file_name.find("0032_20180914_183119_ShipName") > 0]
    return single_file[0]
