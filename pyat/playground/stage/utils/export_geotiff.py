import numpy as np
import osr
from osgeo import gdal


class Export_File:
    def normal_as_geotiff(self, filename_nx, filename_ny, filename_nz):
        """ export normal as geotiff"""
        Nx, Ny, Nz = self.get_normal()
        self.export(filename_nx, Nx)
        self.export(filename_ny, Ny)
        self.export(filename_nz, Nz)
        print("Exported normals as geotiff")

    def export(self, outfilename, values, reference_dataset, grid):
        dst_ds = gdal.GetDriverByName("GTiff").Create(
            outfilename,
            xsize=reference_dataset.RasterXSize,
            ysize=reference_dataset.RasterYSize,
            bands=1,
            eType=gdal.GDT_Float32,
        )
        dst_ds.SetGeoTransform(grid)  # specify coords
        dst_ds.SetProjection(reference_dataset.GetProjection())
        dst_ds.GetRasterBand(1).WriteArray(values)
        dst_ds.FlushCache()
        print("writing file " + outfilename)

    def xsf_toTIFF(self, outfilename, values_x, values_y, values_angles, cache):
        reference_dataset = gdal.GetDriverByName("GTiff").Create(
            outfilename, xsize=values_x.shape[1], ysize=values_y.shape[0], bands=1, eType=gdal.GDT_Float32,
        )

        geoTransform = (np.nanmin(values_x), 1.5, 0, np.nanmax(values_y), 0, -1.5)
        reference_dataset.SetGeoTransform(geoTransform)
        srs = osr.SpatialReference()  # establish encoding
        srs.ImportFromProj4(cache)  # WGS84 lat/long
        reference_dataset.SetProjection(srs.ExportToWkt())
        reference_dataset.GetRasterBand(1).WriteArray(values_angles)
        reference_dataset.FlushCache()
        print("writing file " + outfilename)
