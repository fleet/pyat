import numpy as np


def secondToNanoF(value: float):
    """
	Return time in nanosecond from a time in seconds
    :param value: the time is second (float value)
	:return:
	"""
    return int(value * 1e9)


def floatsecond_tonano_array(value: np.array):
    """ we try to minimize rounding issues by first upgrading float32 time stamps to float64
        with this we expect to minimize differences and rounding issues
        :param value the input values as an array of float in second
        :return a new allocated array to values in nanosecond
     """
    return (value.astype(np.float64) * int(1e9)).astype(np.uint64, copy=False)


def nano_todatetime(value: np.array):
    """
    Convert nanosecond values to numpy datetime array
    :param value:
    :return: a new allocated array with values in datetime
    """
    return value.astype("datetime64[ns]")
