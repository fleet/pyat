import math

import numpy as np
import osr
from osgeo import gdal

from pyat.playground.stage.utils import geoutils


class GeoTiffNormalComputer:
    """ Compute normal values from a bathymetry geotiff """

    def __init__(self, file_name: str):
        """ initialize and open the dtm file"""
        self.file_name = file_name
        self._open()
        self.Nx = None
        self.Ny = None
        self.Nz = None

    def _open(self):
        reference_dataset = gdal.Open(self.file_name)
        self.geoTransform = reference_dataset.GetGeoTransform()
        self.reference_dataset = reference_dataset
        self.projection = geoutils.fromWKT_to_epsg(reference_dataset.GetProjection())

    def get_reference(self):
        """
        Return dataset and geoTransform to export the file
        """
        return self.reference_dataset, self.geoTransform

    def compute_normal(self):

        # compute normal to the geotiff surface
        print("compute normal to the geotiff surface")
        if self.Nx is None:
            self.compute()

    def get_normal(self):
        """ retrieve the normal vectors associated with the geotiff"""
        return self.Nx, self.Ny, self.Nz

    def compute(self):
        srcband = self.reference_dataset.GetRasterBand(1)
        reference_target = srcband.ReadAsArray(
            0, 0, self.reference_dataset.RasterXSize, self.reference_dataset.RasterYSize
        )
        nodata = srcband.GetNoDataValue()
        if nodata:
            reference_target = np.ma.masked_equal(reference_target, nodata)
        gradient = np.gradient(reference_target)

        (min_x, x_res, a, max_y, b, y_res) = self.geoTransform
        dx = math.fabs(y_res)
        dy = math.fabs(x_res)
        dzx = gradient[1]
        dzy = gradient[0]
        Nx = dy * dzx
        Ny = dx * dzy
        Nz = -dy * dx
        # Normalize vector
        norm = np.sqrt(Nx * Nx + Ny * Ny + Nz * Nz)

        self.Nx = Nx / norm
        self.Ny = Ny / norm
        self.Nz = Nz / norm

    def export_as_geotiff(self, filename_nx, filename_ny, filename_nz):
        """ export normal as geotiff"""
        Nx, Ny, Nz = self.get_normal()
        self._export(filename_nx, Nx)
        self._export(filename_ny, Ny)
        self._export(filename_nz, Nz)
        print("Exported normals as geotiff")

    def _export(self, outfilename: str, values):
        dst_ds = gdal.GetDriverByName("GTiff").Create(
            outfilename,
            xsize=self.reference_dataset.RasterXSize,
            ysize=self.reference_dataset.RasterYSize,
            bands=1,
            eType=gdal.GDT_Float32,
        )
        dst_ds.SetGeoTransform(self.geoTransform)  # specify coords
        dst_ds.SetProjection(self.reference_dataset.GetProjection())
        dst_ds.GetRasterBand(1).WriteArray(values)
        dst_ds.FlushCache()
        print("writing file " + outfilename)

    def export_xsf_toTIFF(self, outfilename, values_x, values_y, values_angles, cache):

        self.reference_dataset = gdal.GetDriverByName("GTiff").Create(
            outfilename, xsize=values_x.shape[1], ysize=values_y.shape[0], bands=1, eType=gdal.GDT_Float32,
        )

        self.geoTransform = (np.nanmin(values_x), 1.5, 0, np.nanmax(values_y), 0, -1.5)
        self.reference_dataset.SetGeoTransform(self.geoTransform)
        srs = osr.SpatialReference()  # establish encoding
        srs.ImportFromProj4(cache)  # WGS84 lat/long
        self.reference_dataset.SetProjection(srs.ExportToWkt())
        self.reference_dataset.GetRasterBand(1).WriteArray(values_angles)
        self.reference_dataset.FlushCache()
        print("writing file " + outfilename)
