"""
Several classes for dtm computing
"""

import datetime

import numpy as np
import pandas as pd
import sonar_netcdf.sonar_groups as XConstants
from osgeo import gdal, osr
from playground.stage.dtm.grid_size_computer import GridComputer
from playground.stage.utils.nc_file_cache import (
    DetectionProjectionCache,
    ShipProjectionCache,
)
from playground.stage.utils.workspace_cache import WorkArea_Cache


class GeotiffGenerator:
    """
    from several xsf file, compute and generate a geotiff dtm
    """

    def __init__(self, filelist, output_directory):
        self.work_area = WorkArea_Cache(filelist, output_directory)
        self.files = filelist
        now = datetime.datetime.now()
        self.projected_dtm = output_directory + now.strftime("%Y_%m_%d_%H_%M_%S") + ".tif"
        # self.projection_cache = projection_cache

    def compute_metric(self, outputfile: str, x_res, y_res):
        """
        Compute geotiff in UTM projection
        """
        # first of all compute the bounding boxes
        min_y_values = []
        max_y_values = []
        max_x_values = []
        min_x_values = []
        for f in self.files:
            cache = self.work_area.cache.projected_detection[f]
            x, y = cache.getXY()
            min_x_values.append(np.min(x))
            max_x_values.append(np.max(x))
            min_y_values.append(np.min(y))
            max_y_values.append(np.max(y))
        min_x, min_y, max_x, max_y = [
            min(min_x_values),
            min(min_y_values),
            max(max_x_values),
            max(max_y_values),
        ]

        image_size_x = int((max_x - min_x) / x_res) + 1
        image_size_y = int((max_y - min_y) / y_res) + 1
        dst_ds = gdal.GetDriverByName("GTiff").Create(
            outputfile,
            xsize=image_size_x,
            ysize=image_size_y,
            bands=1,
            eType=gdal.GDT_Float32,
        )

        # recompute the real xmax and ymax to avoid rounding issues
        # max_x = x_res * image_size_x + min_x
        max_y = y_res * image_size_y + min_y
        geotransform = (min_x, x_res, 0, max_y, 0, -y_res)
        dst_ds.SetGeoTransform(geotransform)  # specify coords
        srs = osr.SpatialReference()  # establish encoding
        srs.ImportFromProj4(cache.projection_proj4_def)  # WGS84 lat/long
        dst_ds.SetProjection(srs.ExportToWkt())  # export coords to file

        print("Geotiff compute_metric : initializing matrix")
        pixels = np.full((image_size_y, image_size_x), dtype=np.float32, fill_value=np.nan)
        lower_angle = np.full((pixels.shape[0], pixels.shape[1]), fill_value=np.nan)
        for f in self.files:
            print("Geotiff compute_metric : processing file ", f.file_name)
            cache = self.work_area.cache.projected_detection[f]
            x, y = cache.getXY()
            depth = f.get_as_detection_masked_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Z)
            beam_pointing_angle = f.get_as_array(
                XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE
            )
            depth = f.takeoff_tide(depth)
            absolute_depth = f.takeoff_waterline(depth)
            # compute coordinates in geotiff
            xcoord = (x - min_x) / x_res
            ycoord = (max_y - y) / y_res

            for x, y, z, beam_pointing_angle in np.nditer([xcoord, ycoord, absolute_depth, beam_pointing_angle]):
                i = int(x)
                j = int(y)
                beam_pointing_angle = np.abs(beam_pointing_angle)
                if 0 <= i < pixels.shape[1] and 0 <= j < pixels.shape[0]:
                    if lower_angle[j][i] > beam_pointing_angle:
                        lower_angle[j][i] = beam_pointing_angle
                        pixels[j][i] = -z  # switch Z positive up
                    elif np.isnan(lower_angle[j][i]):
                        lower_angle[j][i] = beam_pointing_angle
                        pixels[j][i] = -z  # switch Z positive up
        # TODO : Need to check, erreur occur when step is to small
        interpolated_pixels = pd.DataFrame(pixels)
        interpolated_pixels = np.array(interpolated_pixels.interpolate(method="nearest", axis=1))

        interpolated_pixels = np.array(interpolated_pixels.interpolate(method="nearest", axis=0))

        dst_ds.GetRasterBand(1).WriteArray(interpolated_pixels)
        dst_ds.FlushCache()  # write to disk
        print("Geotiff compute_metric : done")

    def compute_dtm(self, step=None):
        self.work_area.define_projection()

        print("init Projection caches")
        for f in self.files:
            self.work_area.cache.projected_detection[f] = DetectionProjectionCache(f, self.work_area.projection)
            # compute ship positions in the generic UTM projection
            self.work_area.cache.ship_position_projected[f] = ShipProjectionCache(f, self.work_area.projection)

        if step is None:
            # on utilise que le premier fichier pour avoir le pas
            print("compute grid step")
            computer = GridComputer(self.files[0], self.work_area.cache.projected_detection[self.files[0]])
            step = computer.compute_grid_step()

        print("dtm grid step ", str(step))

        print("Generate dtm")

        self.compute_metric(outputfile=self.projected_dtm, x_res=step, y_res=step)
