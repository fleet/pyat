import numpy as np
import sonar_netcdf.sonar_groups as XConstants
from playground.stage.utils.nc_file_cache import (
    DetectionProjectionCache,
    ShipProjectionCache,
    XSFFile,
)
from playground.stage.utils.workspace_cache import WorkArea_Cache
from scipy.interpolate import interp1d

from pyat.core.utils.matrix_inspector import display_matrix
from pyat.core.xsf.csr import angle_csr


class IncidenceAngleComputer:
    def compute_IBA(self, xsf_file: XSFFile, detectionZ):
        # now try to recompute IBA
        surface_celerity = xsf_file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.SOUND_SPEED_AT_TRANSDUCER)
        # Note that surface celerity should be replaced by the celerity at transducer depth in case of a submarine engine

        surface_angle = angle_csr.detection_pointing_angle_to_surface_crs(xsf_file.dataset)

        # add this to use the same angles as sonarscope
        transducer_roll_offset = xsf_file.get_as_array(XConstants.RootGrp.PlatformGrp.TRANSDUCER_ROTATION_X)
        beam_pointing_angle_transducer = xsf_file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_BEAM_POINTING_ANGLE
        )

        surface_angle = beam_pointing_angle_transducer + transducer_roll_offset[1]
        # celerity profile
        # TODO check and interpolate on time values if several profiles are available or not
        sound_speed_profile = xsf_file.get_as_array(XConstants.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.SOUND_SPEED)
        svp_depth_values = xsf_file.get_as_array(XConstants.RootGrp.EnvironmentGrp.SoundSpeedProfileGrp.DEPTH)

        f = interp1d(svp_depth_values[0, :], sound_speed_profile[0, :])
        if np.ma.is_masked(detectionZ):
            detectionZ = detectionZ.filled(np.nan)

        bottom_detection_celerity = f(
            detectionZ
        )  # cannot apply function to masked array, so fill masked values with nan

        # compute direct beam incidence angle where incidence angle is angle relative to horizontal
        # START
        # compute the snell descartes constant
        surface_celerity = surface_celerity.reshape(-1, 1)

        immersion = xsf_file.get_as_array(XConstants.RootGrp.SonarGrp.BeamGroup1Grp.PLATFORM_VERTICAL_OFFSET)
        display_matrix(immersion.reshape(-1, 1), "immersion")

        # we compute a new surface celerity to compare with sonarscope
        surface_celerity = f(immersion)  # cannot apply function to masked array, so fill masked values with nan
        surface_celerity = surface_celerity.reshape(-1, 1)
        display_matrix(surface_celerity, "surface_celerity")

        m = bottom_detection_celerity / surface_celerity
        # m=1 # force celerity to one (straight line)
        cos_refraction_angle = (
            np.sin(np.radians(surface_angle)) * m
        )  # we directly use sin instead of the complement of the beam pointing angle
        arrival_angle = np.arccos(np.abs(cos_refraction_angle))

        incidence_angle_rel_horizontal = np.pi / 2 - arrival_angle
        computed_iba_degrees = np.abs(surface_angle) - np.degrees(incidence_angle_rel_horizontal)
        return computed_iba_degrees

    def __init__(self, files, output_directory):
        self.work_area = WorkArea_Cache(files, output_directory)

    def compute_incidence_angle(self, normal, xsf_file):
        """
        Compute the incidence angle , ie the angle between the normal and the beam ray
        :param normal: the normal for the associated dtm
        :param xsf_file: a single xsf file
        :return: a pingdetection matrix filled with incidence angles in radians
        """
        # Now we need to compute the incidence angle , ie the angle between the normal and the beam ray
        # The beam ray is given by a vector starting from the detection point and the ship position in utm grid

        self.work_area.define_projection()
        self.work_area.cache.projected_detection[xsf_file] = DetectionProjectionCache(
            xsf_file, self.work_area.projection
        )
        self.work_area.cache.ship_position_projected[xsf_file] = ShipProjectionCache(
            xsf_file, self.work_area.projection
        )

        Nx, Ny, Nz = normal.get_normal()
        shipX, shipY = self.work_area.cache.ship_position_projected[xsf_file].getXY()
        detectionX_UTM, detectionY_UTM = self.work_area.cache.projected_detection[xsf_file].getXY()
        detectionZ = xsf_file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Z
        )
        babord_tribord = xsf_file.get_as_detection_masked_array(
            XConstants.RootGrp.SonarGrp.BeamGroup1Grp.BathymetryGrp.DETECTION_Y
        )

        detectionX_UTM, detectionY_UTM, detectionZ = xsf_file.takeoff_offset_transducer(
            detectionX_UTM, detectionY_UTM, detectionZ
        )

        # Array of 1 with the separation babord/tribord
        babord_tribord = babord_tribord / np.abs(babord_tribord)

        beamX = shipX.reshape((shipX.size, 1)) - detectionX_UTM
        beamY = shipY.reshape((shipY.size, 1)) - detectionY_UTM
        # normalize beam vector
        norm = np.sqrt(beamX**2 + beamY**2)

        # now we got the direction vector from the detection point to the ship

        iba_computed = self.compute_IBA(xsf_file, detectionZ)
        incidence = np.rad2deg(np.arctan(detectionZ / norm)) + iba_computed

        # take into account for IBA, and compute a new triangle depth with constant celerity and
        detectionZ_IBA = norm * np.tan(np.radians(incidence))
        beamZ = -detectionZ_IBA  # (shipZ-detectionZ_IBA) - shipZ

        # normalize
        norm = np.sqrt(beamX**2 + beamY**2 + beamZ**2)
        # now beamXYZ is a vector from the detection point to the ship
        beamX /= norm
        beamY /= norm
        beamZ /= norm

        # retrieve in dtm the nearest normal value
        # need to be optimized with numba
        (originX, x_res, a, originY, b, y_res) = normal.geoTransform
        indexX = (detectionX_UTM - originX) / x_res
        indexX = indexX.astype(int)
        indexY = ((detectionY_UTM - originY) / y_res).astype(int)

        indexXMax = Nx.shape[1]
        indexYMax = Nx.shape[0]
        # mask is not taken into account with indexing, so
        # indexXmask = indexX.mask  # save old mask
        indexX.mask = False  # reset mask to set everything valid
        indexX[indexX >= indexXMax] = 0  # set mask with higher values to a valid index
        indexX[indexX < 0] = 0  # set mask with lower values to a valid index

        # indexYmask = indexY.mask  # save old mask
        indexY.mask = False  # reset mask
        indexY[indexY >= indexYMax] = 0  # set mask with higher values to a valid index
        indexY[indexY < 0] = 0  # set mask with lower values to a valid index

        file_normals_X = Nx[indexY, indexX]
        file_normals_Y = Ny[indexY, indexX]
        file_normals_Z = Nz[indexY, indexX]

        # now compute the beam directions .dot the normals. This give use the cos of the angle
        incidence_angles_radians = file_normals_X * beamX + file_normals_Y * beamY + file_normals_Z * beamZ
        incidence_angles_radians = np.ma.masked_array(
            incidence_angles_radians, indexX.mask
        )  # apply again invalid values mask
        incidence_angles_radians = np.arccos(incidence_angles_radians)
        incidence_angles_radians = incidence_angles_radians * babord_tribord

        print("end computing incidence angle for file " + xsf_file.file_name)

        x_UTM = self.work_area.cache.projected_detection[xsf_file]._x
        y_UTM = self.work_area.cache.projected_detection[xsf_file]._y

        return (
            x_UTM,
            y_UTM,
            self.work_area.cache.projected_detection[xsf_file].projection_proj4_def,
            np.ma.filled(np.degrees(incidence_angles_radians), np.nan),
        )

    # Delete after
    def compute(self):
        self.compute_dtm()
        normal = self.compute_normal()
        print("Start incidence angle processing")
        for f in self.xsf_files:
            self.compute_incidence_angle(normal, f)
        print("End processing incidence angles")

        # step_degree=GeoUtils.meters_to_decimal_degrees(step, (y_max_latitude + y_min_latitude) / 2)
        # tiff.compute_latlon(outputfile='d:\\tmp\\test_latlon.tif', \
        # x_min_longitude=x_min_longitude, y_min_latitude=y_min_latitude, x_max_longitude=x_max_longitude, \
        # y_max_longitude= y_max_latitude, long_res_degree=step_degree, lat_res_degree=step_degree)
