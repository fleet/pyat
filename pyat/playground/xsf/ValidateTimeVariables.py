import matplotlib.pyplot as plt

plt.rcParams['figure.dpi'] = 270
import netCDF4 as nc
import numpy as np


class bcolors:
    """ Utility for color display in terminal or jupyter"""

    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKCYAN = "\033[96m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"

    def print_example(text: str):
        print(f"{bcolors.WARNING}{text}{bcolors.ENDC}")


def pprint(msg, back_ground_color=None):
    """Intercept print call"""
    if back_ground_color == None:
        print(msg)
    else:
        print(f"{back_ground_color}{msg}{bcolors.ENDC}")


def error(msg: str):
    pprint(msg, back_ground_color=bcolors.FAIL)


def info(msg: str):
    pprint(msg, back_ground_color=None)


def warning(msg: str):
    pprint(msg, back_ground_color=bcolors.WARNING)


def header(msg: str):
    pprint(msg, back_ground_color=bcolors.BOLD)


def _print_variable(variable, ignored_variable_list):
    variable_path = variable._grp.path + "/" + variable.name
    if hasattr(variable, "axis"):
        if variable.axis == "T":
            # got a time typed variables
            v = variable[:]
            vview = v.astype("datetime64[ns]")
            unit = variable.units
            if "nanoseconds since 1970-01-01 00:00:00Z" not in unit:
                error(f"Error Variable {variable_path} does not match expected time unit {variable.units}")
            info(f"Variable : {variable_path} {variable.units} overview {vview}\n")


def _recurse_and_display(dataset, recurse_subgroup=True, ignored_variable_list=[]):
    if isinstance(dataset, nc.Variable):
        _print_variable(dataset, ignored_variable_list)
        return

    # if dataset.parent is not None:
    #     print(f"Group {dataset.name} ({dataset.path}")
    # else:
    #     # root dataset has not name
    #     print(f"Root Group {dataset.path}")
    for variable in sorted(dataset.variables):
        _print_variable(dataset[variable], ignored_variable_list)

    if recurse_subgroup:
        for subgroup_name in dataset.groups:
            _recurse_and_display(dataset.groups[subgroup_name])


np.set_printoptions(threshold=5)
file_path = "C:/data/datasets/XSF/0006_20200504_111056_FG_EM122.xsf.nc"
file_path = "C:/data/datasets/0005_20130131_173953_Pingeline.xsf.nc"
dataset = nc.Dataset(file_path)
_recurse_and_display(dataset, True)
dataset.close()
