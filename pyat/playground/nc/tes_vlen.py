import os
import netCDF4 as nc
import tempfile as tmp
import numpy as np

import random
""""
Create a vlen variable and try to apply scale factor
"""
def create_file(file:str, create_with_compression:bool=False):
    with nc.Dataset(file,"w") as f:
        #create vlen type and name it
        vlen_t = f.createVLType(np.int, "my_type")
        x = f.createDimension("x",2)
        y = f.createDimension("y",8)
        vlvar = f.createVariable("var", vlen_t, ("y","x"))
        vlvar.scale_factor = 0.1 #not taken into account
        vlvar2 = f.createVariable("var_static",  np.float, ("y","x"))

        #fill with arbitrary values
        data = np.empty(len(y)*len(x),object)
        for n in range(len(y)*len(x)):
            #create array of variable len
            data[n] = np.pi+np.arange(500) #create fake data with pi values
        data = np.reshape(data,(len(y),len(x)))
        for y_i in range(0,len(y)):
            for x_i in range(0,len(x)):
                vlvar[y_i,x_i] = data[y_i,x_i]


def print_content_sample(file:str):
    with nc.Dataset(file) as input:
        values=input["var"]
        values_nonvlen=input["var_static"]
        x_len=len(input.dimensions["x"])
        y_len=len(input.dimensions["y"])
        sample=values[1,1]
        last_index=len(sample)-1
        print(f"file {file} : content_vlen = [{sample[0]} ...{sample[last_index]} ]")
        print(f"file {file} : content = [{values_nonvlen[0,0]} ... {values_nonvlen[y_len-1,x_len-1]}]")


file=tmp.mktemp(suffix=".nc",prefix="nocompress_")

create_file(file)
print(f"Without compression {os.path.getsize(file)}")
print_content_sample(file)
file=tmp.mktemp(suffix=".nc",prefix="compress_")
create_file(file,True)
print(f"With compression {os.path.getsize(file)}")
print_content_sample(file)
