import datetime

from netCDF4 import Dataset
import numpy as np

now = datetime.datetime.now()

# using vlens
nc = Dataset('test1.nc', 'w')
vlen_type = nc.createVLType(np.float64, 'vltest')
nc.createDimension('x', None)
v = nc.createVariable('vl', vlen_type, 'x')
# random lengths between 1 and 1000
ilen = np.random.randint(900,1000,size=1000)
n = 0
for nlen in ilen:
    data = np.random.uniform(low=0.0, high=1.0, size=nlen)
    v[n] = data
    n += 1
nc.close()
print("End of vlen time elapsed {} \n".format(datetime.datetime.now() - now))


now = datetime.datetime.now()

# using fixed size arrays with lossy compression
nc = Dataset('test2.nc', 'w')
nc.createDimension('x', None)

nc.createDimension('y', 1000) # max possible size of data
# zlib compression (lossy if least_significant_digit != None)
#v = nc.createVariable('fl', np.float64, ('x','y'), zlib=True,least_significant_digit=1)
#  lossy compression by packing into unsigned bytes
v = nc.createVariable('fl', np.uint8, ('x','y'),zlib=True,)
v.scale_factor=255.
# random lengths between 1 and 1000
ilen = np.random.randint(900,1000,size=100000)
n = 0
for nlen in ilen:
    data = np.random.uniform(low=0.0, high=1.0, size=nlen)
    v[n,0:nlen] = data[:]
    n += 1
nc.close()
print("End of zipped time elapsed {} \n".format(datetime.datetime.now() - now))
