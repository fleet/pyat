#! /usr/bin/env python3
# coding: utf-8

import logging
import time
from typing import NamedTuple

import gws.rsocket_api.execution_context as exec_ctx


class Args(NamedTuple):
    """
    Class representing all arguments for the process
    """

    nb_seconds: int = 10


def launch(**kwargs) -> None:
    """
    Function accepting all arguments of the process as a dict. Possible arguments are listed in "Args" class
    """
    launch_with_Args(Args(**kwargs))


def launch_with_Args(args: Args) -> None:
    """
    Main function
    """
    logger = logging.getLogger()
    logger.warn(f"Waiting {args.nb_seconds}s for cancellation")

    monitor = exec_ctx.get_root_progress_monitor()
    monitor.begin_task("Waiting for cancel", 100)

    chrono = 0.0
    sleep_time = args.nb_seconds / 20.0
    while chrono < args.nb_seconds and not monitor.check_cancelled():
        time.sleep(sleep_time)
        chrono = chrono + sleep_time
        monitor.worked(5)

    if monitor.check_cancelled():
        logger.info(f"Cancel successfully")
    else:
        logger.warn(f"Cancel no received")
