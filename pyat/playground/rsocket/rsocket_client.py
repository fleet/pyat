import asyncio
import logging
from datetime import timedelta

import numpy as np
from rsocket.extensions.helpers import composite, route
from rsocket.helpers import single_transport_provider
from rsocket.payload import Payload
from rsocket.rsocket_client import RSocketClient
from rsocket.transports.tcp import TransportTCP
from rsocket.extensions.tagging import TaggingMetadata


async def main(server_port):
    logging.info("Connecting to server at localhost:%s", server_port)

    connection = await asyncio.open_connection("localhost", server_port)

    async with RSocketClient(
        single_transport_provider(TransportTCP(*connection)),
        fragment_size_bytes=16777215,
        keep_alive_period=timedelta(seconds=10),
    ) as client:
        rng = np.random.default_rng(12345)
        values = rng.random(size=(15000, 15000), dtype=np.float32)
        payload = Payload(
            values.tobytes(),
            composite(route("Test_route"), TaggingMetadata(b"Test_tag", list("Test_value"))),
        )
        await client.fire_and_forget(payload)
        await asyncio.sleep(1)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    asyncio.run(main(10000))
