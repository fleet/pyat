from pathlib import Path
import os
import time
import zipfile
from zipfile import  ZipFile
from datetime import datetime


"""
Tools used to zip .all and .wcd files recursively starting from a directory
"""
def compress(files:list):
    for file in files:
        dir=file.parent
        file_name=file.name
        zip_path=dir.joinpath(str(file_name)+str(".zip"))
        if not os.path.exists(zip_path):
            print(f"Compressing file {file_name}")
            os.chdir(dir)
            with ZipFile(zip_path, 'w', compression=zipfile.ZIP_DEFLATED) as myzip:
                myzip.write(file_name)



start="T:/"
while(True):
    try:
        p=Path(start)
        print(f"{datetime.now()} seeking .all files in {start}")
        files=list(p.glob("**/018*.all"))
        compress(files)
        files=list(p.glob("**/019*.all"))
        compress(files)
        files=list(p.glob("**/02*.all"))
        compress(files)
        print(f"{datetime.now()} seeking .wcd files in {start}")
        files=list(p.glob("**/018*.wcd"))
        compress(files)
        files=list(p.glob("**/019*.wcd"))
        compress(files)
        files=list(p.glob("**/02*.wcd"))
        compress(files)
        print(f"{datetime.now()} Wait 10 min")
    except:
        print("Exception occurred")
    time.sleep(60 * 10)  # Sleep 10 min


