#! /usr/bin/env python3
# coding: utf-8

from datetime import datetime
from os import path

from netCDF4 import Dataset
from osgeo import gdal, ogr, osr

gdal.UseExceptions()

# Constant fields
FILE_FIELD = "FicData"
PROFILE_FIELD = "Profil"
START_DATE_FIELD = "DateDeb"
END_DATE_FIELD = "DateFin"
START_TIME_FIELD = "HeureDeb"
END_TIME_FIELD = "HeureFin"
START_LAT_FIELD = "LatDeb"
START_LON_FIELD = "LonDeb"
END_LAT_FIELD = "LatFin"
END_LON_FIELD = "LonFin"
CAMPAIGN_NAME = "Campagne"
CAMPAIGN_NUM = "NumCamp"
NAVIGATION = "Navigation"
TOOL = "Outil"


def run(input: str):
    """
    Main method : converts NVI/MBG to a shape file.
    """
    # shape file driver
    shp_driver = ogr.GetDriverByName("ESRI Shapefile")

    # data source
    if not path.exists(input):
        raise FileNotFoundError()

    data_source = ogr.Open(input, 0)

    if data_source is None:
        print("Error while opening file " + str(input))
        raise IOError("Error while opening file " + str(input))

    print(f"Opened {input}")
    layer = data_source.GetLayer()
    featureCount = layer.GetFeatureCount()
    print(f"Number of features {featureCount}")
    for feature in layer:
        profil = feature.GetField(PROFILE_FIELD)
        geom = feature.GetGeometryRef()
        point_count = geom.GetPointCount()
        print(f"{profil} : count {point_count}")


"""
Main method (entry point)
"""
if __name__ == "__main__":
    run("E:/temp/Thalia.kml")
