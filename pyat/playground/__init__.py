# -*- coding: utf-8 -*-

"""Playground directory is used to put file developped for testing purpose."""
__author__ = """Cyrille Poncelet"""
__email__ = "cyrille.poncelet@ifremer.fr"
__version__ = "0.1.0"
