import pandas as pd
import xarray as xr
import numpy as np
import tempfile as tmp
import matplotlib.pyplot as plt



def merge_overlapping_files():
    """Create two dataset of """

    #see https://stackoverflow.com/questions/62806175/xarray-combine-by-coords-return-the-monotonic-global-index-error
    #
    #create first file
    dataset_A=xr.Dataset(
        data_vars={
            "values": (("t"), np.arange(0,8)),
        },
        coords={
            "t": pd.date_range("2020-07-05", periods=8, freq="D"),
        },
    )
    f_A=tmp.mktemp("_A.nc")
    dataset_A.to_netcdf(f_A)


    values_time = pd.date_range("2020-07-12 00:00:00", periods=8, freq="D").to_list()
    values_time[1] = pd.to_datetime("2020-07-10 00:00:00")


    #create second file
    dataset_B=xr.Dataset(
        data_vars={
            "values": (("t"), np.arange(8,16)),
        },
        coords={
            "t":values_time,
        },
    )
    f_B=tmp.mktemp("_B.nc")
    dataset_B.to_netcdf(f_B)

    #now open A and B as multi dataset

    merged = xr.open_mfdataset((f_A,f_B))
    print(merged)
    fig=merged["values"].plot.line("b-^")
    plt.grid(True)
    plt.show()

