import xlrd
import glob as g
from pathlib import Path

def _format_cell(sheet,row,col):
    return sheet.cell_value(row,col).strip().replace("|","\\|")


def toAdoc_XSF(filename: str, output_dir: str ):
    """ Specialized fonction for conversion of XSF format excel files to adoc"""
    short_name = Path(filename).stem
    reader = xlrd.open_workbook(filename)
    sheets = reader.sheets()
    with open(f"{output_dir}/{short_name}.adoc","w",encoding='utf8') as f:
        f.write(f"# {short_name}\n\n")
        for sheet in sheets:
            nrow=sheet.nrows
            ncols=sheet.ncols
            title= sheet.cell_value(0,0)
            status = sheet.cell_value(1,0)
            f.write(f"## {title} {status}\n\n")
            comment = sheet.cell_value(1, 1)
            f.write(f"{comment}\n")
            f.write("[cols=\"4*\",%autowidth,options=\"header\",]\n")
            f.write("|===\n")
            f.write("|Name |Detail |Mapping|Commentaire\n")
            for row in range(2,nrow):
                if sheet.cell_value(row,0).strip() != "Name":
                    f.write(f"|{_format_cell(sheet,row,0)} |{_format_cell(sheet,row,1)} | {_format_cell(sheet,row,2).strip()} |{_format_cell(sheet,row,3).strip()} \n")
            f.write("|===\n")



# in case we are started in standalone app, for debug only
if __name__ == "__main__":
    directory = "D:\\XSF\\Mappings"
    files = g.glob(f"{directory}/[KAS]*.xlsx")
    for file in files:
        toAdoc_XSF(file,directory)