import os

from pyat.core.sonarscope.cruise_summary.cruise_auto_summary import CruiseAutoSummary


def test_cruise_auto_summary_dev() -> None:
    input_dir = r"C:\data\datasets\Backscatter\Compensation\THALIA_ESSDEC2019\EM2040\XSF\reduced"
    input_dir = r"C:\data\BOBECO-LEG1\RESON7150\XSF"
    cas = CruiseAutoSummary(input_dir, work_dir=os.path.join(input_dir, "summary"))
    cas.parse_files(file_pattern="20110922_0435*")

    cas.display_metadata.print_global_metadata()

    cas.plot_sound_speed_profiles()

    # # plot attitudes data
    # cas.plot_attitudes()
    # cas.plot_heading()

    #
    # # plot surface sound speed vs navigation
    # cas.plot_surface_sound_speed()

    # plot navigation

    cas.plot_navigation()

    # plot transmit type (CW/FM) vs navigation
    cas.plot_transmit_type()

    # # plot wc presence
    # cas.plot_wc_presence()
    #
    # # plot RTK presence
    # cas.plot_gps_quality()  # need interpolation from captor

    # plot mode / mode 2/ mode 3 for kongsberg files
    cas.plot_runtime_mode_infos()  # runtime info needs interpolation,

    # plot tx sector count vs navigation
    cas.plot_tx_sector_count()

    # plot nb swath/ping
    cas.plot_nbswath_per_ping()  # if missing computed from runtime

    cas.plot_synthetic_mode()

    # # plot interping distance
    # cas.plot_interping_distance(use_finest_level=True)  # computed variable
    #
    # # plot Sound speed profile (partiel = 0-100m)
    # cas.plot_sound_speed_profiles(max_depth=-500)
    #
    # # plot depth/rel seafloor
    # cas.plot_height_above_seafloor()
    #
    # # plot Mean QF
    # cas.plot_mean_qf()  # can be missing
    #
    # # plot bsn
    # cas.plot_bsn()
    #
    # # plot bso
    # cas.plot_bso()
    #
    # # plot abs coefficient
    # cas.plot_mean_abs()

    cas.plot_backscatter()
