from pyat.core.sonarscope.bs_correction.angular_renormalization import xsf_constant_process

def test_angular_renormalization_dev() -> None:
    input_file = r"C:\data\test_bulleur_thalia\0090_20190429_063439_Bassop.xsf.nc"
    output_file = r"C:\data\test_bulleur_thalia\0090_20190429_063439_Bassop_renorm.xsf.nc"
    mean_model_file = r"C:\data\test_bulleur_thalia\0090_20190429_063439_Bassop.bsar.nc"

    xsf_constant_process(i_paths=[input_file], o_paths=[output_file], mean_model_file=mean_model_file, overwrite=True)
