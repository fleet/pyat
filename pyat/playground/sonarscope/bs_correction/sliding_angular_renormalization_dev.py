from pyat.core.sonarscope.bs_correction.sliding_angular_renormalization import xsf_sliding_process
from pyat.core.sonarscope.model.sounder_lib import SounderType


def angular_renormalization_dev() -> None:
    input_files = [
        r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF\0033_20190428_062303_Bassop.xsf.nc",
        r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF\0037_20190428_064932_Bassop.xsf.nc",
        # r"/mnt/c/data/THALIA_ESSDEC2019/EM2040/reduced/XSF/0033_20190428_062303_Bassop.xsf.nc",
        # r"/mnt/c/data/THALIA_ESSDEC2019/EM2040/reduced/XSF/0037_20190428_064932_Bassop.xsf.nc",
    ]

    output_files = [
        r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF_rolling\0033_20190428_062303_Bassop_renorm.xsf.nc",
        r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF_rolling\0037_20190428_064932_Bassop_renorm.xsf.nc",
        #r"/mnt/c/data/THALIA_ESSDEC2019/EM2040/reduced/XSF_rolling/0033_20190428_062303_Bassop_renorm.xsf.nc",
        #r"/mnt/c/data/THALIA_ESSDEC2019/EM2040/reduced/XSF_rolling/0037_20190428_064932_Bassop_renorm.xsf.nc",
    ]
    sounder_type = SounderType.EM2040_ALL

    xsf_sliding_process(
        sounder_type=sounder_type, i_paths=input_files, o_paths=output_files, overwrite=True, use_snippets=False
    )

angular_renormalization_dev()
