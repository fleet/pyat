from pyat.core.sonarscope.bs_correction.kongsberg_correction_computer import KongsbergCorrectionComputer
from pyat.core.sonarscope.model.constants import VariableKeys as Key
from pyat.core.sonarscope.model.signal.ping_detection_signal import PingDetectionSignal
from pyat.core.sonarscope.model.signal.ping_signal import PingSignal
from pyat.core.xsf import xsf_driver


def test_kongsberg_correction_computer_dev() -> None:
    #all
    input_file = r"C:\\data\\EVOHE\\EVOHE2022\\EM2040\\XSF\\0168_20221203_192249_ThalassaEM2040.xsf.nc"
    #kmall
    #input_file = r"C:\\data\\ESSTECH-CdM-2022\\EM2040\\DONNEES\\XSF\\0003_20220314_214611.xsf.nc"
    with xsf_driver.open_xsf(file_path=input_file, mode="r") as xsf:
        ping_detection_model = PingDetectionSignal(xsf_dataset=xsf)
        ping_detection_model.read([Key.DETECTION_INCIDENCE_ANGLE])
        ping_model = PingSignal(xsf_dataset=xsf)
        computer = KongsbergCorrectionComputer(
            ping_timed_dataset=ping_model, ping_detection_dataset=ping_detection_model
        )
        computer.compute_lambert_correction()
        computer.compute_specular_correction()
        computer.compute_insonified_area_db()
