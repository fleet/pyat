from pyat.core.sonarscope.bs_correction.dtm_angles_computer import DtmAnglesComputer
from pyat.core.sonarscope.bs_correction.generic_correction_computer import GenericCorrectionComputer
from pyat.core.sonarscope.model.signal.ping_detection_signal import PingDetectionSignal
from pyat.core.sonarscope.model.signal.ping_signal import PingSignal
from pyat.core.xsf import xsf_driver

def test_generic_correction_computer_dev() -> None:
    input_file = r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF\0033_20190428_062303_Bassop.xsf.nc"
    dtm_file = r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF\0033_20190428_062303_Bassop.dtm.nc"
    # input_file = r"C:\data\Etude_spatialisation_WC\A.SAUNIER\EnginAvecOffsetPitch\0005_20140818_083230_raw_01.xsf.nc"
    # dtm_file = r"C:\data\Etude_spatialisation_WC\A.SAUNIER\EnginAvecOffsetPitch\0005_20140818_083230_raw_01.dtm.nc"
    with xsf_driver.open_xsf(file_path=input_file, mode="r") as xsf:
        ping_detection_model = PingDetectionSignal(xsf_dataset=xsf)
        ping_model = PingSignal(xsf_dataset=xsf)
        dtm_angles_computer = DtmAnglesComputer(ref_path=dtm_file)
        computer = GenericCorrectionComputer(
            ping_timed_dataset=ping_model,
            ping_detection_dataset=ping_detection_model,
            dtm_angles_computer=dtm_angles_computer,
        )
        computer.compute_lambert_correction()
        computer.compute_specular_correction()
        computer.compute_insonified_area_db()
