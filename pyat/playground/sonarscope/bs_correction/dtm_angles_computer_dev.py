from pyat.core.sonarscope.bs_correction.dtm_angles_computer import DtmAnglesComputer
from pyat.core.sonarscope.common.configuration import default_config

def test_dtm_angles_computer_dev() -> None:
    from pyat.core.sonarscope.model.signal.ping_detection_signal import PingDetectionSignal
    from pyat.core.sonarscope.model.constants import VariableKeys as Key
    from pyat.core.xsf import xsf_driver

    input_dtm_file = r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF\ALL_Bassop.dtm.nc"
    input_xsf_file = r"C:\data\THALIA_ESSDEC2019\EM2040\reduced\XSF\0033_20190428_062303_Bassop.xsf.nc"
    with xsf_driver.open_xsf(file_path=input_xsf_file, mode="r") as xsf:
        ping_detection_model = PingDetectionSignal(xsf_dataset=xsf)
        ping_detection_model.read([Key.DETECTION_LATITUDE, Key.DETECTION_LONGITUDE])

        detection_longitudes = ping_detection_model.xr_dataset[Key.DETECTION_LONGITUDE].to_numpy()
        detection_latitudes = ping_detection_model.xr_dataset[Key.DETECTION_LATITUDE].to_numpy()

        computer = DtmAnglesComputer(ref_path=input_dtm_file)
        computer._compute_dtm_slope_aspect()
        slope, aspect = computer._interpolate_slope_aspect_from_lonlat(detection_longitudes, detection_latitudes)

        default_config.logger.debug(f"done computing slope/aspect")

    default_config.logger.debug(f"done cleaning")
