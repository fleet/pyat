import tempfile
from typing import List
from click import progressbar
import dask
import numpy as np
import xarray as xr
import os
from dask.diagnostics import ProgressBar
from distributed import Client

from pyat.core.sonarscope.bs_correction.mean_bs_model import BackscatterCurve, BackscatterCurveByIncidenceByPing
from pyat.core.sonarscope.bs_correction.sliding_angular_renormalization import xsf_sliding_process
from pyat.core.sonarscope.model.sounder_lib import SounderType
from pyat.core.sonarscope.common.configuration import IntegrationMethod, default_config
from pyat.core.utils.netcdf_utils import DEFAULT_COMPRESSION_LEVEL


def backscattercurve_to_netcdf_debug() -> None:
    
    file_count = 10
    ping_count = 120000
    
    incidence_angle_range, incidence_bin_width, incidence_bin_count, incidence_bin_centers = (
            default_config.incidence_angles.angle_range,
            default_config.incidence_angles.bin_width,
            default_config.incidence_angles.bin_count,
            default_config.incidence_angles.bin_centers,
        )
    mean_values_by_incidence = np.full(
        shape=(ping_count, incidence_bin_count),
        fill_value=100.0,
    )
    value_counts_by_incidence = np.full(
        shape=(ping_count, incidence_bin_count),
        fill_value=10,
    )
    mode = np.full(shape=(ping_count), fill_value=0)
    
    ping_time = np.full(shape=(ping_count), fill_value=0, dtype=np.int64)
    current_time = np.datetime64('now')
        
    input_files = []
    for f in range(file_count):
        for i in range(ping_count):
            ping_time[i] = current_time.astype(np.int64)
            current_time += np.timedelta64(1,"s")
        
        # create curve by incidence
        curve_by_incidence = BackscatterCurveByIncidenceByPing.build(
            mean_values=mean_values_by_incidence,
            count=value_counts_by_incidence,
            bin_centers=incidence_bin_centers,
            ping_time=ping_time,
            mode=mode,
            origin=None,
        )
        tmp_curvefile = tempfile.mktemp()
        curve_by_incidence.to_netcdf(tmp_curvefile)
        input_files.append(tmp_curvefile)
        
    final_curvefile = tempfile.mktemp()
    incr_merge_backscatter_curve(input_files, final_curvefile)
    
    for f in input_files:
        os.remove(f)
    os.remove(final_curvefile)
    
    print('success')

def merge_backscatter_curve(input_netcdf_files: List[str], output_netcdf_file: str):
    with dask.config.set({"array.slicing.split_large_chunks": True, 
                          'scheduler':'synchronous', 
                          "array.chunk-size":"32MiB", 
                          "distributed.worker.memory.target": 0.02,
                          "distributed.worker.memory.spill": 0.05}):
        os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE"
        encodings = {}
        input_ds = []
        try:
            for f in input_netcdf_files:
                ds = xr.open_dataset(
                    f,
                    chunks={BackscatterCurve.PING_TIME: 1000,},
                )
                # retrieve compression parameters from input datasets
                for key, var in ds.data_vars.items():
                    if key not in encodings:
                        encodings[key] = {}
                    encodings[key]["zlib"] = var.encoding["zlib"]
                    encodings[key]["complevel"] = DEFAULT_COMPRESSION_LEVEL
                    if "chunksizes" in encodings[key]:
                        encodings[key]["chunksizes"] = max(encodings[key]["chunksizes"], var.encoding["chunksizes"])
                    else:
                        encodings[key]["chunksizes"] = var.encoding["chunksizes"]
                input_ds.append(ds)
            dataset_by_ping = xr.merge(input_ds).chunk({BackscatterCurve.PING_TIME: 1000,})
            delayed_ds = dataset_by_ping.to_netcdf(output_netcdf_file, encoding=encodings,compute=False)
            with ProgressBar():
                delayed_ds.compute()
        finally:
            for d in input_ds:
                d.close()

def incr_merge_backscatter_curve(input_netcdf_files: List[str], output_netcdf_file: str):
    encodings = {}
    input_ds = []
    try:
        with dask.config.set({"array.slicing.split_large_chunks": True,}):
            dataset_by_ping = xr.Dataset()
            for f in input_netcdf_files:
                ds = xr.open_dataset(
                    f,
                    chunks={BackscatterCurve.PING_TIME: 1000,},
                )
                # retrieve compression parameters from input datasets
                for key, var in ds.data_vars.items():
                    if key not in encodings:
                        encodings[key] = {}
                    encodings[key]["zlib"] = var.encoding["zlib"]
                    encodings[key]["complevel"] = DEFAULT_COMPRESSION_LEVEL
                    if "chunksizes" in encodings[key]:
                        encodings[key]["chunksizes"] = max(encodings[key]["chunksizes"], var.encoding["chunksizes"])
                    else:
                        encodings[key]["chunksizes"] = var.encoding["chunksizes"]
                input_ds.append(ds)
                dataset_by_ping = xr.merge([dataset_by_ping,ds])
            delayed_ds = dataset_by_ping.to_netcdf(output_netcdf_file, encoding=encodings,compute=False)
            delayed_ds.compute()
    finally:
        for d in input_ds:
            d.close()


backscattercurve_to_netcdf_debug()
