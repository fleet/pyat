from osgeo import gdal, osr
from rio_cogeo.cogeo import cog_validate, cog_info


def gdal_translate(input_file, output_file):
    # Open the input raster dataset
    input_dataset = gdal.Open(input_file)

    if input_dataset is None:
        print(f"Failed to open input file: {input_file}")
        return

    # Create output raster dataset
    driver = gdal.GetDriverByName('COG')
    spatial_ref = osr.SpatialReference()
    spatial_ref.ImportFromEPSG(3857)
    web_mercator = spatial_ref.ExportToWkt()
    output_dataset = driver.CreateCopy(output_file, input_dataset, options=["COMPRESS=LZW", 'TARGET_SRS=' + web_mercator])

    if output_dataset is None:
        print(f"Failed to create output file: {output_file}")
        return

    print(f"Translation successful. Output file: {output_file}")

    # Close datasets
    output_dataset = None
    input_dataset = None


def validate_cog(file):
    print(f'File {file} valid : {cog_validate(src_path=file)}')
    print(cog_info(src_path=file))


# Example usage
if __name__ == "__main__":
    dir_test = "F:\\temp\\test_dtm\\"
    input_file = dir_test + "1_16_9701012055_CALMAR97_elevation.tif"
    output_file = input_file.replace('.tif', '_cog.tif')

    gdal_translate(input_file, output_file)
    validate_cog(output_file)
