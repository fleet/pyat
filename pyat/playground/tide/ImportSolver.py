"""
Use to be able to retrieve modules and import in core and app directories
"""
print(__name__)
print(__package__)
from os import sys, path

local_dir=path.dirname(path.abspath("__file__")) #tide
p=path.dirname(path.dirname(path.dirname(local_dir))) #pyat
print(f"Appending {p} to PATH")
sys.path.append(p)
