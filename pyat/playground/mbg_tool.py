from calendar import different_locale

import netCDF4 as nc
import math
import numpy as np
import matplotlib.pyplot as plt
import os


""""
Compute difference between two mbg points by point
"""


Depth = "mbDepth"
AlongDistance = "mbAlongDistance"
AcrossDistance = "mbAcrossDistance"
DistanceScale = "mbDistanceScale"
cFlag = "mbCFlag" #Flag of cycle


def _read_mbg_depth(file: nc.Dataset):
    """Function allowing to read a mbg depth value"""
    # decoding of mbg depth value is quite special since based on a secondary variable holding scaling
    depth_factor = file.variables[Depth]
    return depth_factor[:]
    pass

def _read_cflag(file: nc.Dataset):
    variable = file.variables[cFlag]
    variable.set_auto_maskandscale(False)
    values=variable[:]
    return np.frombuffer(values, dtype='int8').reshape(values.shape)

def diff(first_file: str, second_file: str, title=""):
    """"
    Compute difference between two mbg points by point
    """

    with nc.Dataset(first_file) as first_dataset,nc.Dataset(second_file) as second_dataset:
        first_depth = _read_mbg_depth(first_dataset)
        second_depth = _read_mbg_depth(second_dataset)
        diff = first_depth-second_depth
        print(f"statistics min: {np.nanmin(diff)}, max:{np.nanmax(diff)}, mean {np.nanmean(diff)}, median {np.nanmedian(diff)}")
        plt.figure()
        plt.imshow(first_depth-second_depth)
        plt.title(title)
        plt.colorbar()

        fig, ax = plt.subplots(nrows=1, ncols=1)
        ax.plot(diff[:,215])
        ax.grid(True)
        plt.title(title + " ecarts median par beam")

        fig, ax = plt.subplots(nrows=1,ncols=1)
        ax.plot(np.median(diff,axis=0))
        ax.grid(True)
        plt.title(title +" ecarts median par beam")
        fig, ax = plt.subplots(nrows=1,ncols=1)
        for ping in range(20,400,25):
            ax.plot(diff[ping,:],label=f"ping {ping}")
        plt.title(title +f" ecarts par beam")
        ax.grid(True)


def _read_along(file:nc.Dataset):
    scale_variable = file.variables[DistanceScale]
    scale_variable.set_auto_maskandscale(False)
    decoded_scale = np.frombuffer(scale_variable[:], dtype='int8')
    decoded_scale=decoded_scale.astype(np.integer)
    decoded_scale = decoded_scale * scale_variable.scale_factor
    decoded_scale= decoded_scale.reshape(-1,1)
    return file.variables[AlongDistance][:]*decoded_scale

def _read_across(file:nc.Dataset):
    scale_variable = file.variables[DistanceScale]
    scale_variable.set_auto_maskandscale(False)
    decoded_scale = np.frombuffer(scale_variable[:], dtype='int8')
    decoded_scale=decoded_scale.astype(np.integer)
    decoded_scale = decoded_scale * scale_variable.scale_factor
    decoded_scale= decoded_scale.reshape(-1,1)
    return file.variables[AcrossDistance][:]*decoded_scale

def show_distances(file:str):
    with nc.Dataset(file) as f:
        along=_read_along(f)
        across=_read_across(f)
        depth=_read_mbg_depth(f)
        plt.figure()
        plt.title("Distance")
        for i in range(20,400,25):
            plt.subplot(311)
            plt.plot(along[i,:],label=f"ping {i}")
            plt.subplot(312)
            plt.plot(across[i,:],label=f"ping {i}")
            plt.subplot(313)
            plt.plot(depth[i, :],label=f"ping {i}")
        plt.subplot(311).set_title("along")
        plt.subplot(312).set_title("across")
        plt.subplot(313).set_title("depth")
        plt.subplot(311).grid()
        plt.subplot(312).grid()
        plt.subplot(313).grid()
        plt.show()



def read_mbg_time(file: str):
    with nc.Dataset(file) as dataset:
        time = dataset.variables["mbDate"]
        time_frac = dataset.variables["mbTime"]
        time.set_auto_scale(False)  # disable autoscale to be able to set a starting date, force to 1970
        time_values = time[:]
        time_frac_values = time_frac[:]

        npd = time_values.astype('timedelta64[D]')
        npd2 = time_frac_values.astype('timedelta64[ms]')
        time_final = np.datetime64("1970-01-01 00:00:00") + npd + npd2
        return time_final

def show_time(file:str):

    plt.title(f"{file} : time")
    time = read_mbg_time(file)
    plt.plot(time[:,1],marker="+")

    plt.show(True)
# in case we are started in standalone app, for debug only
if __name__ == "__main__":
    # file_path_ref ="E:/data/CPERTUISOT_REBROUSSEMENT_TEMPOREL/2020_1800149958-bathy-TH_EM2040_1.mbg"
    # file_path_ref ="E:/data/CPERTUISOT_REBROUSSEMENT_TEMPOREL/ALL/0000_20201010_080106_Bassop_good2.mbg"
    # with nc.Dataset(file_path_ref) as f:
    #     v=_read_cflag(f)
    #     ex=v[833:835,:]
    #
    # show_time(file_path_ref)


    file_path_ref = "C:/data/datasets/SVP_Profile/Vel_Pour_Cyrille/mbg_may15_volcan/0210_20201010_065008_EM122_Marion_Dufresne.mbg"
    file_path_modified_new = "C:/data/datasets/SVP_Profile/Vel_Pour_Cyrille/Processed/0210_20201010_065008_EM122_Marion_Dufresne-soundvelocitycorrection6.mbg"
    #file_path_modified_no_angle = "D:/WorkspaceUserGlobe/SVP/0210_20201010_065008_EM122_Marion_Dufresne-soundvelocitycorrection_210_no_angle_correction.mbg"
    #file_path_modcel = "D:/data/SVP_Profile/Vel_Pour_Cyrille/Processed/0210_20201010_065008_EM122_Marion_Dufresne_modecel.mbg"

    # fil_186= "D:/data/SVP_Profile/Vel_Pour_Cyrille/mbg_may15_volcan/0186_20201009_180549_EM122_Marion_Dufresne.mbg"
    # fil_186_processed= "D:/WorkspaceUserGlobe/SVP/0186_20201009_180549_EM122_Marion_Dufresne-soundvelocitycorrection_186_profile.mbg"
    # diff(file_path_ref, file_path_modified_no_angle, title="fichier 210 (Globe no angle correction)")
    #
    #show_distances(file_path_ref)

    diff(file_path_ref, file_path_modified_new, title="fichier 210 (Globe)")
    #diff(file_path_ref,file_path_modcel, title="fichier 210 vs modcel")
    #diff(file_path_modified_new, file_path_modcel, title="fichier 210 (globe vs modcel)")
    # diff(file_path_modcel,file_path_modified)
    #diff(fil_186,fil_186_processed, "fichier 186")
    plt.grid()
    plt.show()
