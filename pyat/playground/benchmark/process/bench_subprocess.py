import time
import subprocess
nanoseconds = 1000000000  # 1 billion nanoseconds

# Define the command to run
cmd = ["python", "subprocess_exe.py"]

start_nanosec = time.time_ns()
# Launch the subprocess and capture the output
subprocess.run(cmd)

# Decode the output from bytes to string
output_str = 0 #output.decode("utf-8")
start_exe_time = int(output_str)
# To avoid inter process communication the code to execute to compute final time is printed to output
print(f"start_nanosec={start_nanosec}")
print("nanoseconds = 1000000000")
print("exe_time_s= (start_exe_time-start_nanosec)/nanoseconds")
print("print(f'Execution time {exe_time_s} s')")
