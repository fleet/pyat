import time
from multiprocessing import Process,Queue
nanoseconds = 1000000000  # 1 billion nanoseconds


# a custom function that blocks for a moment
def task(q):
    # block for a moment
    time_nanosec = time.time_ns()
    q.put(time_nanosec) #send time back to main proceess


def run_and_get_time():
    # Launche a process, do nothing except retrieve starting time
    q=Queue()
    process = Process(target=task,args=(q,))
    start_nanosec = time.time_ns()
    # run the process
    process.start()
    # wait for the process to finish
    start_exe_time=q.get()
    process.join()

    return (start_exe_time-start_nanosec)/nanoseconds


# entry point
if __name__ == '__main__':
    for i in range(0,10):
        print(f"Execution time {run_and_get_time()} s")
