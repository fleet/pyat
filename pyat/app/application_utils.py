#! /usr/bin/env python3
# coding: utf-8

import inspect
import json
import logging
import os
import sys
import tempfile
from datetime import datetime
from importlib import import_module
from typing import Callable, Dict

from dateutil import parser
from py4j.java_gateway import JavaGateway, Py4JError

import gws.rsocket_api.execution_context as exec_ctx
import gws.rsocket_api.rsocket_client as rsocket_client
import pyat.core.utils.argument_utils as arg_util
from pyat.core.utils.exceptions.exception_list import BadParameter
from pyat.core.utils.monitor import DefaultMonitor, JavaMonitor


def launch_application(
    json_conf_file_path: str,
    callable_process: Callable[..., Dict | None] = None,
    json_parameters=None,
) -> Dict:
    """
    Launch an application wrapping a callable process.
    When the application is launched by Java (Globe), the progress monitor is searched in Py4J gateway.

    :param json_conf_file_path: the path of the application (*_app.py)
    :param callable_process: a class previously imported. eg:
        'from pyat.core.dtm.peak_detector import PeakFinder'
        -> pass PeakFinder as a parameter
    :param json_parameters: If specified, take priority over the command line arguments. Make it easier to call it
    inside a script

    When called through a command line:
    Parameters of the application are read in the json file when present as the first argument of the command line.
    If this json file is missing, all the command line is considered as the parameters of the application
    When a second json file is present in the command line, it is considered as the expected report file

    Then the application is instantiated with all the parameters and finally called
    """
    script_id = os.environ.get("PYTHON_SCRIPT_ID")

    # if callable not provided, find it from configuration file
    if not callable_process:
        callable_process = extract_function(json_conf_file_path)

    # set the logger and the monitor depending on what called the application
    monitor, logger = init_logger(script_id, callable_process)

    # get arguments
    arguments: Dict = {}
    # check json_parameters
    if json_parameters:
        # if there is a json_parameters given as argument, take priority over the command line arguments
        arguments = (
            load_json_file(json_parameters)
            if json_parameters.endswith("json")
            else json.loads(json_parameters, parse_float=True)
        )
    elif sys.argv:
        system_args = sys.argv[1:]
        if system_args[0].endswith("json"):
            # a json parameter file has been passed in system arguments
            arguments = load_json_file(system_args[0])
        else:
            # get parameters from command line. First one is the name of the processed file
            arg_parser = arg_util.create_argv_parser(callable_process.__name__, json_conf_file_path)
            arguments = vars(arg_parser.parse_args(system_args))
    else:
        logger.error(f"Error while launching : {callable_process} : arguments missing")

    json_report_file = None
    if "report_file" in arguments:
        json_report_file = arguments["report_file"]
        del arguments["report_file"]
    if "configuration_file" in arguments:
        del arguments["configuration_file"]

    # there are 2 launch modes.
    #  - one asynchronous, using rsocket when the port is present.
    #  - the other synchronous, by executing the script directly.
    if "rsocket_port" in arguments:
        rsocket_port = arguments["rsocket_port"]
        del arguments["rsocket_port"]
        start_rsocket_and_run(arguments, monitor, logger, callable_process, rsocket_port)
        return {}  # No result yet : run is called asynchronously
    else:
        return run(arguments, monitor, logger, callable_process, json_report_file)


def start_rsocket_and_run(arguments, monitor, logger, callable_process, rsocket_port: int) -> None:
    def _async_run():
        # Run the app
        result = run(arguments, monitor, logger, callable_process, None)

        # Sending the resulting output files
        if result is not None and "outfile" in result:
            rsocket_msg_emitter = exec_ctx.get_rsocket_msg_emitter()
            rsocket_msg_emitter.emit_files(result["outfile"])

    rsocket_client.start(rsocket_port, _async_run)


def run(arguments, monitor, logger, callable_process, json_report_file) -> Dict:
    """
    :param arguments: dictionary containing parameters (previously parsed from json)
    :param monitor: from init_logger
    :param logger: from init_logger
    :param callable_process: a class previously imported cf. launch_application
    """
    if arguments:
        try:
            monitor.begin_task(str(callable_process), 100)

            # add monitor if possible
            if "monitor" in inspect.signature(callable_process).parameters:
                arguments["monitor"] = monitor

            # setup temp directory if possible
            if "temp_dir" in arguments:
                tempfile.tempdir = arguments["temp_dir"]
            elif json_report_file is not None:
                # Consider temp dir in Globe workspace as report file directory
                tempfile.tempdir = os.path.dirname(json_report_file)

            is_function: bool = inspect.isfunction(callable_process)

            # if necessary, cast arguments to match with callable parameters types
            callable_params = inspect.signature(
                callable_process if is_function else callable_process.__call__
            ).parameters
            for arg in arguments:
                if arg in callable_params and callable_params[arg].annotation is datetime:
                    arguments[arg] = parser.parse(arguments[arg])  # cast "datetime" arguments

            # call function or run class
            report = callable_process(**arguments) if is_function else callable_process(**arguments)()

            # Reporting
            monitor.done()
            return _write_report(arguments, report, json_report_file, logger)
        except ValueError as e:
            logger.exception(str(e))
            return _write_report(None, {"error": str(e)}, json_report_file, logger)
        except LookupError as e:
            # Error occurs when app try to access to an empty context variable (gws.rsocket_api.execution_context)
            logger.exception("This process need a TCP socket. Activates this option and try again.")
            return _write_report(None, {"error": str(e)}, json_report_file, logger)
        except Exception as e:
            logger.exception(f"An exception was thrown : {str(e)}")
            return _write_report(None, {"error": str(e)}, json_report_file, logger)
    else:
        error = """ Useless process without input(s) and parameter(s). Stop the program.
        Please enter input with the option -i I_PATHS [I_PATHS ...], --i_paths I_PATHS [I_PATHS ...].
        """
        logger.error(error)
        return {"error": error}


def init_logger(script_id, callable_process: Callable):
    """
    :param script_id:
    :param callable_process: a class previously imported cf. launch_application

    :return monitor: get previously by init_logger
    :return logger: get previously by init_logger
    """
    monitor = DefaultMonitor

    logging.basicConfig(
        level=logging.INFO,
        datefmt="%Y-%m-%d %H:%M:%S",
        format="%(asctime)s - %(levelname)s - %(name)s : %(message)s",
        force=True,
    )

    logger = logging.getLogger(callable_process.__name__)
    if script_id:
        try:
            logging.getLogger("py4j").setLevel(100)
            gateway = JavaGateway()
            gateway.jvm.System.getProperty("java.runtime.name")

            api = gateway.entry_point
            if api.getScriptMonitor(script_id):
                monitor = JavaMonitor(api.getScriptMonitor(script_id))

            logger.info(f"Py4J connection enabled: SCRIPT_ID = {script_id}")
            return monitor, logger
        except Py4JError:
            logger.info("No JVM listening.")
            return monitor, logger
    return monitor, logger


def get_json_configuration_file(application_file_path: str) -> str:
    """
    Compute the json configuration file of the an application.
    :param application_file_path -- the path of the application (*_app.py).
    :return [str] -- the json configuration file
    """
    bn = os.path.basename(application_file_path)
    dir_path = os.path.dirname(application_file_path)
    return os.path.join(dir_path, "conf", bn[: bn.rfind("_")] + ".json")


def get_json_configuration_file_from_argument_file(json_arguments_path: str) -> str:
    """
    Extract the json configuration file of the an application from the json argument file.
    :param application_file_path -- the path of the json argument file.
    :return [str] -- the json configuration file
    """
    arguments = load_json_file(json_arguments_path)
    if "configuration_file" in arguments:
        return arguments["configuration_file"]
    raise BadParameter(f"No argument configuration_file found in {json_arguments_path} - python process")


def load_json_file(file_path: str):
    """
    Loads json file.
    """
    with open(file_path, "r", encoding="utf-8") as file:
        return json.load(file)


def extract_function(json_conf_file_path: str):
    """
    Extracts from the conguration file, the property function
    It represents the python entry point to be invoked to start the service
    """
    conf = load_json_file(json_conf_file_path)
    module_path, function = conf["function"].rsplit(".", 1)
    mod = import_module(module_path)
    return getattr(mod, function)


def _write_report(arguments, report, json_path, logger) -> Dict:
    # Default behavior : write all out files to report
    if report is None:
        o_paths = "o_paths" if "o_paths" in arguments else "o_path"
        if o_paths in arguments:
            out_file_paths = arguments[o_paths]
            if isinstance(out_file_paths, str):
                out_file_paths = [out_file_paths]
            report = {"outfile": [file_path for file_path in out_file_paths if os.path.exists(file_path)]}

    if report is not None and json_path is not None:
        try:
            with open(json_path, "w", encoding="utf-8") as f:
                json.dump(report, f, indent=4)
        except Exception as e:
            logger.exception(f"Error while writing report file : {str(e)}")

    return report
