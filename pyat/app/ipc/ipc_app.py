import zmq
import pyat.app.application_utils as app_util
import pyat.app.launcher_app as launcher_app

parameters_file_key = 'argumentFilePath'


def start_server(port: str):
    context = zmq.Context()
    socket = context.socket(zmq.REP)
    print(f"Start PyAT IPC server (port: {port})")
    socket.bind(f"tcp://*:{port}")
    while True:
        message = socket.recv_json()  # Wait for next request from client
        print(f"Received request: {message}")

        if parameters_file_key in message:
            parameters_file = message[parameters_file_key]
            print(f"Run with arguments file : {parameters_file}...")
            result = {}
            try:
                result = launcher_app.__launch__(parameters_file)
            finally:
                socket.send_json(result)

        else:
            socket.send(b'pyat')

    # socket.close() ?


def start_client(port: str):
    context = zmq.Context()

    #  Socket to talk to server
    print(f"Connecting to GLOBE server at port : {port}")
    socket = context.socket(zmq.REQ)
    socket.connect(f"tcp://localhost:{port}")

    #  Do 10 requests, waiting each time for a response
    for request in range(10):
        msg = {
            "type": "VERSION",
            "argument": "this is my arguments"
        }
        print(f"Sending request : {msg}")
        socket.send_json(msg)

        #  Get the reply.
        message = socket.recv()
        print("Received reply %s [ %s ]" % (request, message))


if __name__ == "__main__":
    start_server("5556")
