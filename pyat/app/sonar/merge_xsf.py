#! /usr/bin/env python3
# coding: utf-8

import sonar_netcdf.process.sonar_file_merger as sfm

import pyat.app.application_utils as app_util
from pyat.core.xsf.netcdf_merger_bridge import NcMergerBridge


class XsfMergerBridge(NcMergerBridge):
    def __init__(self, **kwargs):
        super().__init__(nc_merger_class=sfm.SNMerger, **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), XsfMergerBridge)
