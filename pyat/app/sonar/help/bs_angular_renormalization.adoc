:last-update-label!:

== Backscatter angular renormalization

=== Overview
Compute a backscatter renormalization to remove angular dependency.
Based on statistic curves, backscatter offsets are computed and applied to file inputs in order to have a constant (typically -20) mean response.
