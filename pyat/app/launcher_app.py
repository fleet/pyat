#! /usr/bin/env python3
# coding: utf-8
import sys

import pyat.app.application_utils as app_util
from pyat.core.utils.exceptions.exception_list import BadParameter


def __launch__(json_arguments_path: str):
    # Extract configuration file from arguments file
    json_configuration_file = app_util.get_json_configuration_file_from_argument_file(json_arguments_path)
    return app_util.launch_application(json_configuration_file, json_parameters=json_arguments_path)


if __name__ == "__main__":
    # Default process : the function to run will be determined from the JSON configuration file.
    if len(sys.argv) == 2:
        # pylint: disable=unbalanced-tuple-unpacking
        __launch__(sys.argv[1])
    else:
        raise BadParameter("Bad number of argument for globe - python process")
