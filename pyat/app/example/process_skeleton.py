import pyat.app.application_utils as app_util
import pyat.core.utils.pyat_logger as log


class Example:
    """
    Empty example taking one file as input and generating one output file

    !! Do not forget to add your json file to globe.conf

    """
    def __init__(self, **params):
        """Init function, initialize class member
          this will parse parameters and store them"""

        #create a logger, will allow to print in Globe console with info, error, warning level
        self.logger = log.logging.getLogger(Example.__name__)

        #parse input file parameters
        if "i_paths" in params:
            self.input_file = params["i_paths"]
        else:
            #If parameter is not found for any reason raise an exception
            raise Exception("Parameter i_paths is missing")

        #parse output file parameters
        if "o_paths" in params:
            self.output_file = params["o_paths"]
        else:
            #If parameter is not found for any reason raise an exception
            raise Exception("Parameter o_paths is missing")

        #parse parameter overwrite, if not found set to false by default
        self.overwrite = bool(params["overwrite"]) if "overwrite" in params else False


    def __call__(self):
        """Run the process"""
        self.logger.info(f"--- Starting")

        self.logger.info(f"--- Run process input file(s) {self.input_file}")

        # PUT YOUR CODE HERE


        self.logger.info(f"--- Run process outputfile(s) {self.output_file}")
        self.logger.info(f"--- Stopping")

if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), Example)
