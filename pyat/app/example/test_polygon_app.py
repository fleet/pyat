from typing import List
import pyat.app.application_utils as app_util
import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor


class TestPolygon:
    def __init__(
        self,
        i_paths: List[str],
        o_paths: List[str],
        mask: List[str],
        overwrite: bool = False,
        monitor: ProgressMonitor = DefaultMonitor,
    ):
        self.monitor = monitor
        self.logger = log.logging.getLogger(self.__class__.__name__)
        self.logger.info(f"{self.__class__.__name__} called")
        self.logger.info(f"i_paths = {i_paths} ")
        self.logger.info(f"o_paths = {o_paths} ")
        self.logger.info(f"mask = {mask} ")
        self.logger.info(f"overwrite = {overwrite} ")

    def __call__(self) -> None:
        self.monitor.done()


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), TestPolygon)
