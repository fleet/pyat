:last-update-label!:

== Laloxy

=== Overview
This application writes a copy of a given CSV file where longitude/X and latitude/Y columns are converted to another format/projection.

Among available output formats there is some Caraïbes-compatible fixed-size encoding of longitude-latitude or XY/UTM/rectangular coordinates.


=== Parameters description

==== input_file
path to a CSV-like file containing coordinates data, latitude/X and longitude/Y each in their own column.

==== output_file
path to a (non-existing) file to write in the obtained output.

==== indexes
The indexes of columns containing the coordinates in the input file.
Must provide two indexes, one for latitude/X, another for longitude/Y.

==== delimiter


==== input_format
The format of coordinates found in the input file. Can be:

- DECIMAL: only degrees, such as `-77.508333` or `-164.754167°`
- DEG_MIN_DEC: Degrees and Minutes, such as `-45° 17,896' N`
- DEG_MIN_SEC: Degrees Minutes and Seconds, such as `164° 45' 15.0012" W`
- XY: UTM projected coordinates, such as `933813.46`. Parameter `input_proj` must provide the associated projection.

==== output_format
The format of coordinates written in the output file.
Must be one of the following:

- DECIMAL: Degrees, such as `-77.508333`.
- DEG_MIN_DEC: Degrees and Minutes, such as `-45 17,896`.
- DEG_MIN_SEC: Degrees Minutes and Seconds, such as `-164 45 15.0012`.
- RICHDEG_MIN_SEC: Degrees Minutes and Seconds in a more stylized way, such as `164° 45' 15.0012" W`.
- CARAIBES_DECIMAL: Caraibes-like Degrees, as `sdd.dddddd` for latitude, and `sddd.dddddd` for longitudes.
- CARAIBES_DEG_MIN_DEC: Caraibes-like Degrees and Minutes, as `sddd  mm.mmmmm` for latitude, and `sddd  mm.mmmmm` for longitudes.
- CARAIBES_XY: Caraibes-like UTM projected coordinates, as `smmmmmmmmm.mm` for latitude, and `smmmmmmmmm.mm` for longitudes.


==== input_proj
The projection used by the coordinates found in the input file.

`epsg:4326` is to be used when input coordinates are given in latitude/longitude format (DECIMAL, DEG_MIN_DEC or DEG_MIN_SEC).


==== output_proj
The projection used by the coordinates written in the output file.

`epsg:4326` is to be used when output coordinates are written in latitude/longitude format (DECIMAL, DEG_MIN_DEC, DEG_MIN_SEC, RichDEG_MIN_SEC, CARAIBES_DECIMAL or CARAIBES_DEG_MIN_DEC).


==== rounding
Written Coordinates will be written in output with a number of decimal numbers equal to `rounding`. This may be overwritten by `output_format` if set to a size-specific format (Caraibes).

==== overwrite
Allow output file to exists. If overwrite is not set to true, and output file exists, the program will exit.
