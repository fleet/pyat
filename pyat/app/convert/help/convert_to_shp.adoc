:last-update-label!:
== MBG / NVI -> Shape

=== Overview
Creates a shape file (.shp) from sounder files (.mbg) or/and navigation files (.nvi).
