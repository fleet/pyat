#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.cubic_interpolation_process as cubic
from pyat.app.emodnet.interpolation.abstract_interpolation_process import (
    InterpolationProcessAdapter,
)


class CubicInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of CubicInterpolationProcess to launch a cubic interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(cubic.CubicInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), CubicInterpolationProcessAdapter)
