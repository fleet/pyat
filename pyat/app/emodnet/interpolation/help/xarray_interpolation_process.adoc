:last-update-label!:

== Interpolate over a dtm

=== Overview
Interpolation functions based on rioxarray interpolation tool (https://corteva.github.io/rioxarray/stable/examples/interpolate_na.html).

This algorithm interpolates data, if a shape file or kml is defined, interpolation is limited to the given areas.
After interpolation, a dominant cdi id is computed if the CDI layer is defined.


