#! /usr/bin/env python3
# coding: utf-8


import pyat.app.application_utils as app_util
import pyat.core.dtm.interpolation.telea_interpolation_process as telea
from pyat.app.emodnet.interpolation.abstract_interpolation_process import InterpolationProcessAdapter


class TeleaInterpolationProcessAdapter(InterpolationProcessAdapter):
    """
    Adapts an instance of TeleaInterpolationProcess to launch a linear interpolation on DTMs.
    """

    def __init__(self, **kwargs):
        super().__init__(telea.TeleaInterpolationProcess(**kwargs), **kwargs)


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), TeleaInterpolationProcessAdapter)
