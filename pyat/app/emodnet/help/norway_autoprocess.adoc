:last-update-label!:

== Norway Auto process

=== Overview
A process to ease EMODnet processing for Norway (at least).

This process takes

* input files as tiff (tested for UTM projection),
* cdi as a csv list

It reprojects to a 1/16 res in lat long and applies cdi to the dtm.nc files

