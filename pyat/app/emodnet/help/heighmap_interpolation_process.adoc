:last-update-label!:

== Interpolate over a dtm

=== Overview
Interpolation functions for heightmaps developed within the EMODnet Bathymetry (High Resolution Seabed Mapping) project.

This toolbox implements a set of Radial Basis Functions (RBF) interpolants on elevation data

The algorithm used here is a fork of https://github.com/coronis-computing/heightmap_interpolation
done for integration purpose in Globe and pyat project


If you want to use interpolation as a fill hole method, consider to set the missing value flag
