:last-update-label!:

== DTM4 (NetCDF4) -> Isobath

=== Overview
Computes isobaths from a set of DTM file(s) and export them as ESRI shapefiles, using GDAL contour computation capabilities (https://gdal.org/api/gdal_alg.html#_CPPv421GDALContourGenerateEx15GDALRasterBandHPv12CSLConstList16GDALProgressFuncPv[See GDAL algorithm description])

Required parameter :

* Elevation interval between isobaths in meters (m)
