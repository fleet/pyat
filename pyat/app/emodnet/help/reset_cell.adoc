:last-update-label!:

== Reset cells process

=== Overview
This application takes DTM file(s) as input and returns new DTM file(s) with some reset cells depending on filters
parameters.

By default, the name of the output file is i_path with the suffix "-zeroed". There are no filter by zone,
no filter by cdi, no filter by layer.
