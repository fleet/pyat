""" Script parsing globe.conf file and generating an index for all declared processes"""
import locale

import json


output_file = "help/index.adoc"


with open("../globe.conf", encoding=locale.getpreferredencoding()) as f, open(
    output_file, mode="w", encoding=locale.getpreferredencoding()
) as f_out:
    print(f"= Python process List", file=f_out)
    print(":toc: left", file=f_out)
    print(":toclevels: 4", file=f_out)
    print("\n_this document index python processes interfaced with Globe_\n", file=f_out)

    for line in f:
        line = line.partition("#")[0]
        line = line.rstrip()
        if len(line) > 0:
            file_to_load = line
            file_to_load = file_to_load.replace("app/", "./")
            with open(file_to_load, encoding=locale.getpreferredencoding()) as mod:
                print(f"processing{file_to_load}")
                h = json.load(mod)
                if "help" in h:
                    help_name = h["help"]
                    help_name = help_name.replace(".html", ".adoc")
                    help_name = help_name.replace("app/", "../")
                    name = h["name"]
                    print(f"include::{help_name}[]", file=f_out)
                    print("\n", file=f_out)
