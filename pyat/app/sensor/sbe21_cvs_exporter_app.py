import pyat.app.application_utils as app_util
import pyat.core.sensor.SBE21 as sbe21
from pyat.app.sensor.sensor_cvs_exporter_app import SensorCSVExporter


class CSVExporter(SensorCSVExporter):
    def __init__(self, **params):
        super().__init__()
        self.parse_parameter(**params)
        self.parser_func = sbe21.read_SB21_NMEA


if __name__ == "__main__":
    app_util.launch_application(app_util.get_json_configuration_file(__file__), CSVExporter)
