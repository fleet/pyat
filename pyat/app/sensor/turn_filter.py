import logging as log
from typing import List, Optional

from pytechsas.utils import turn_filter
import gws.rsocket_api.execution_context as exec_ctx

from pyat.core.navigation import navigation_factory
from pyat.core.utils.monitor import DefaultMonitor, ProgressMonitor

logger = log.getLogger("techsas_turn_filter")


def apply_filter_batch(
    i_paths: List[str],
    filter_method: str = "std",
    heading_period: Optional[int] = None,
    heading_threshold: Optional[float] = None,
    speed_period: Optional[int] = None,
    speed_threshold: Optional[float] = None,
    minimum_duration: Optional[int] = None,
    o_cut_file: Optional[str] = None,
    save_in_input_file: bool = False,
    monitor: ProgressMonitor = DefaultMonitor,
) -> None:
    # Prefer to use RSocket monitor if available
    if exec_ctx.get_root_progress_monitor() is not None:
        monitor = exec_ctx.get_root_progress_monitor()

    monitor.begin_task("Evalutating", 100 * len(i_paths))

    # process magnetism for each file
    for i_path in i_paths:
        # Copy input file to output path"
        # if not overwrite and os.path.exists(o_path):
        #    logger.warning(f"File {o_path} already exists, skipping it")
        #    continue
        # if o_path != i_path:
        #    shutil.copy(i_path, o_path)
        monitor.worked(10)
        turn_filter.apply_filter_on_file(
            nav=navigation_factory.from_file(i_path),
            filter_method=filter_method,
            heading_period=heading_period,
            heading_threshold=heading_threshold,
            speed_period=speed_period,
            speed_threshold=speed_threshold,
            minimum_duration=minimum_duration,
            o_cut_file=o_cut_file,
            save_in_input_file=save_in_input_file,
        )
        monitor.worked(90)
