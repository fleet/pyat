:last-update-label!:

== TECHSAS magnetic processing

=== Overview
This tool processes geolocated TECHSAS magnetic files (.mag.nc) with the following steps: 

==== 1 - (Optional) Apply cable out length

Compute the new position of measurements taking into account the cable length between the ship and the sensor (in meters).

If the new location of a measure point can't be compute, the point is flagged "unvalid".

==== 2 - Theoretical magnetic values
Compute *theoretical magnetic* values from navigation data. 

The reference used is IGRF-13 (International Geomagnetic Reference Field). More details at : https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html

==== 3 - Magnetic anomalies
Compute *magnetic anomalies* :
[source,]
----
magnetic_anomaly = measured_mag - theoretic_mag
---- 