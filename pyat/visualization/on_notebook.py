import matplotlib.pyplot as plt

import numpy as np


class Notebook:
    def display_2d(self, values, title, show, bins=100):
        """
        Print in the notebook stats about values
        Case where values.ndim > 1
        :param values: Matrix to display
        :param title: Title of the graph
        :param show: Display or not the graph
        :return:
        """
        f = plt.figure(title, figsize=(15, 5))
        ax = plt.subplot(1, 2, 1)
        c = ax.imshow(values, aspect="auto")
        ax.set_xlabel("Data")
        f.colorbar(c, ax=ax)
        ax = plt.subplot(1, 2, 2)
        ax.hist(values.flatten(), histtype="step", bins=bins)
        ax.set_xlabel("Histogram ")
        print("Mean : ", np.nanmean(values))
        print("Median : ", np.nanmedian(values))
        print("Std : ", np.nanstd(values))
        print("Max : ", np.nanmax(values))
        print("Min : ", np.nanmin(values))
        print(title.center(100))
        if show:
            plt.show()

    def display_1d(self, values, title, show):
        """
        Print in the notebook stats about values
        Case where values.ndim > 1
        :param values: Matrix to display
        :param title: Title of the graph
        :param show: Display or not the graph
        """
        f = plt.figure(title, figsize=(15, 5))
        ax = plt.subplot(1, 2, 1)
        c = ax.plot(values)
        ax.set_xlabel("Data")
        print("Mean : ", np.nanmean(values))
        print("Median : ", np.nanmedian(values))
        print("Std : ", np.nanstd(values))
        print("Max : ", np.nanmax(values))
        print("Min : ", np.nanmin(values))
        print(title.center(100))
        if show:
            plt.show()
