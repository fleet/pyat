import logging
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from PySide2 import QtCore, QtWidgets, QtGui
from PySide2.QtCore import Slot

matplotlib.use("Qt5Agg")
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from pyat.core.xsf import xsf_reader as reader

logger = logging.getLogger(__name__)


# see https://stackoverflow.com/questions/45787237/exception-handled-surprisingly-in-pyside-slots
def excepthook(cls, exception, traceback):
    print("An exception occured...")
    logger.error("{}".format(exception))


sys.excepthook = excepthook


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):

        QtWidgets.QMainWindow.__init__(self)
        self.resize(1300, 900)
        self.reader = None
        # Menu
        self.menu = self.menuBar()
        self.file_menu = self.menu.addMenu("File")
        load_action = QtWidgets.QAction("Load", self)
        load_action.triggered.connect(self.load_file)
        self.file_menu.addAction(load_action)

        ## Exit QAction
        exit_action = QtWidgets.QAction("Exit", self)
        exit_action.setShortcut("Ctrl+Q")
        exit_action.triggered.connect(self.exit_app)
        self.file_menu.addAction(exit_action)
        self.ask_quit = True

        # add frame and Vertical layout
        self.frame = QtWidgets.QFrame(parent=self)
        self.setCentralWidget(self.frame)
        self.frame_layout = QtWidgets.QGridLayout()
        self.frame.setLayout(self.frame_layout)

        # Create layer selection tree widget
        self.tree = QtWidgets.QTreeView()
        self.tree.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tree.doubleClicked.connect(self.tree_doubleClicked)
        self.frame_layout.addWidget(self.tree, 0, 0)

        # Create matplotlib widget
        self.static_canvas = FigureCanvasQTAgg(Figure())
        self.frame_layout.addWidget(self.static_canvas, 0, 1)
        self._static_ax = self.static_canvas.figure.subplots(1, 1)

        self.addToolBar(NavigationToolbar(self.static_canvas, self))

        self.statistic_label = QtWidgets.QLabel("No data selected")
        self.frame_layout.addWidget(self.statistic_label)
        self.static_canvas.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        # ping selector
        pingSelectorArea = QtWidgets.QHBoxLayout()
        self.label_index = QtWidgets.QLabel("swath ")
        self.label_current_index = QtWidgets.QLabel("swath ")
        self.selector = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self.selector.setMinimum(0)
        self.selector.setMaximum(0)
        pingSelectorArea.addWidget(self.label_index)
        pingSelectorArea.addWidget(self.label_current_index)
        self.selector.valueChanged.connect(self.swathChanged)
        pingSelectorArea.addWidget(self.selector)
        self.stat_button = QtWidgets.QPushButton("Show hist")
        self.stat_button.setToolTip("Compute and display histogram of data levels")

        pingSelectorArea.addWidget(self.stat_button)
        self.stat_button.clicked.connect(self.showStatistics)

        self.tvg_button = QtWidgets.QCheckBox("substract TVG offset")
        self.tvg_button.setToolTip(
            "(Disabled) When check, substract tvg constant offset trying to normalized displayed wc levels"
        )
        self.tvg_button.setEnabled(False)
        pingSelectorArea.addWidget(self.tvg_button)
        self.tvg_button.stateChanged.connect(self.onTvgButtonClick)

        group = QtWidgets.QGroupBox("index selector")
        group.setLayout(pingSelectorArea)
        group.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        self.frame_layout.addWidget(group)

        self.setWindowTitle("adcp/xsf File Parser")

    def onTvgButtonClick(self):
        # redraw data
        self.tree_doubleClicked()

    def showStatistics(self):
        variable_name = self.tree.currentIndex().sibling(self.tree.currentIndex().row(), 0).data()
        variable_path = self.tree.currentIndex().sibling(self.tree.currentIndex().row(), 2).data()
        if self.reader is None:
            return
        v = self.reader.get_variable_data(variable_path, self.selector.value(), self.tvg_button.isChecked())
        if len(v.shape) == 1:
            return
        else:
            ax = plt.subplots()
            v = v.flatten()
            v = v[np.logical_not(np.isnan(v))]
            # default bin for WC data
            axs = plt.subplot()
            if self.reader.is_vlen(variable_path):
                axs.hist(v, bins=np.arange(-128, 128, 1))
            else:
                axs.hist(v)
            plt.show()
            plt.grid()
            plt.title("variable " + variable_name + " index " + str(self.selector.value()))

    def update_index_selector(self, variable_path):
        """update the index selector"""
        if self.reader.is_vlen(variable_path):
            variable = self.reader.get_variable(variable_path)
            first_dim = variable.dimensions[0]
            dim_length = variable.shape[0]
            self.label_index.setText(str(first_dim) + "[0," + str(dim_length) + "]")
            self.label_current_index.setText("--")
            self.selector.setMinimum(0)
            self.selector.setMaximum(dim_length)
            self.selector.setEnabled(True)
        else:
            self.label_index.setText("--")
            self.label_current_index.setText("")
            self.selector.setDisabled(True)

    def _getcurrent_variable(self):
        """
        get the name and path of the current variable
        :return:
        """
        item = self.tree.currentIndex().sibling(self.tree.currentIndex().row(), 0).data()
        # get the name of the path
        variable_path = self.tree.currentIndex().sibling(self.tree.currentIndex().row(), 2).data()
        return item, variable_path

    def tree_doubleClicked(self):
        """
        Event double click on the tree
        :return:
        """
        t = self._getcurrent_variable()
        if t[1] is not None:
            self.update_index_selector(t[1])
            self.plot_variable(t[0], t[1])

    def swathChanged(self, value):
        t = self._getcurrent_variable()
        self.label_current_index.setText(str(self.selector.value()))

        if t[1] is not None:
            self.plot_variable(t[0], t[1])

    @Slot()
    def exit_app(self):
        sys.exit()

    def plot_variable(self, variable_name, path) -> None:
        # import cProfile
        # cp = cProfile.Profile()
        # cp.enable()

        if self.reader is None:
            return
        # this is the only way I found to clear the figure : clear it and recreate it
        self.static_canvas.figure.clf()
        self._static_ax = self.static_canvas.figure.subplots(1, 1)
        if not self.reader.is_ignored(variable_name):
            v = self.reader.get_variable_data(path, self.selector.value(), self.tvg_button.isChecked())
            # validation = beam_group.variables[variable_name]

            print(v.shape)
            try:
                if len(v.shape) == 1:
                    self._static_ax.plot(v)
                else:
                    self._static_ax.imshow(v, aspect="auto")

            except:
                logger.error(self, variable_name, " ", sys.exc_info()[0])
            try:
                self.statistic_label.setText(
                    "statistics for "
                    + variable_name
                    + " [ mean : "
                    + str(np.nanmean(v))
                    + " min:"
                    + str(np.nanmin(v))
                    + " max:"
                    + str(np.nanmax(v))
                    + "]"
                )
            except:
                logger.error(self, variable_name, " ", sys.exc_info()[0])
                self.statistic_label.setText(variable_name + "Error while computing stats")
        else:
            logger.info(self, "Ignore variable " + variable_name)
        self.static_canvas.figure.tight_layout()
        self.static_canvas.figure.canvas.draw()

    # cp.disable()
    # cp.print_stats(2) #sort by cumulative time

    def __get_reader__(self, file_name):
        return reader.XSFReader(file_name)

    def load_file(self):
        # noinspection PyCallByClass
        file_name, _ = QtWidgets.QFileDialog.getOpenFileName(
            self,
            str("Open nc/xsf file"),
            "",
            str("Sonar data file(*.xsf *.nc);;All Files (*)"),
        )
        self.reader = self.__get_reader__(file_name)

        # Tree view
        self.tree.setModel(QtGui.QStandardItemModel())
        self.tree.model().clear()
        self.tree.model().setHorizontalHeaderLabels(["Name", "Local Name", "Path"])
        self.create_Tree(self.reader.dataset.groups, child=None)
        # set the tree
        self.tree.expandAll()
        self.tree.setColumnWidth(0, 250)
        self.tree.hideColumn(2)
        # init slider
        self.selector.setMinimum(0)
        self.selector.setValue(0)
        # self.selector.setMaximum(xsf.swath_count - 1)

    # Browse all the tree and include it
    def create_Tree(self, dataTree, child):
        for a in dataTree:
            if dataTree[a].parent.parent is None:
                child1 = QtGui.QStandardItem(a)
                self.tree.model().appendRow(child1)
            else:
                child1 = QtGui.QStandardItem(a)
                child.appendRow([child1])
            for b in dataTree[a].variables:
                childItem = QtGui.QStandardItem(b)
                try:
                    if dataTree[a].variables[b].long_name:
                        childComment = QtGui.QStandardItem(dataTree[a].variables[b].long_name)
                    else:
                        childComment = dataTree[a].variables[b].name
                    # noinspection PyProtectedMember
                    childPath = QtGui.QStandardItem(
                        dataTree[a].variables[b]._grp.path + "/" + dataTree[a].variables[b].name
                    )
                    child1.appendRow([childItem, childComment, childPath])
                except:
                    pass

            self.create_Tree(dataTree[a].groups, child1)

    def do_you_really_want(self, title: str = "Quit", text: str = "quit") -> QtWidgets.QMessageBox.StandardButton:
        msg_box = QtWidgets.QMessageBox(self)
        msg_box.setWindowTitle(title)
        # msg_box.setIconPixmap(QtGui.QPixmap(app_info.app_icon_path))
        msg_box.setText("Do you really want to %s?" % text)
        msg_box.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        msg_box.setDefaultButton(QtWidgets.QMessageBox.No)
        return msg_box.exec_()

    def closeEvent(self, event: QtCore.QEvent) -> None:
        """actions to be done before close the app"""

        reply = QtWidgets.QMessageBox.Yes
        if self.ask_quit:
            reply = self.do_you_really_want(text="quit?")

        if reply == QtWidgets.QMessageBox.Yes:
            event.accept()
            super().closeEvent(event)
        else:

            event.ignore()

    def run(self):
        #        app = QtWidgets.QApplication(sys.argv)
        self.show()


#        sys.exit(app.exec_())

# in case we are started in standalone app
if __name__ == "__main__":
    try:
        app = QtWidgets.QApplication(sys.argv)
        window = MainWindow()
        window.run()
        sys.exit(app.exec_())
    except:
        exc_info = sys.exc_info()
        print(exc_info[0])
