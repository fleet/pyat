import sys

from PySide2 import QtWidgets

from pyat.visualization.xsfgui_inspector import MainWindow
from pyat.core.adcp.netcdf_adcp_reader import ADCPReader as reader


class Window(MainWindow):
    def __init__(self):
        super(Window, self).__init__()

    def __get_reader__(self, file_name):
        return reader(file_name)


#        sys.exit(app.exec_())

# in case we are started in standalone app
if __name__ == "__main__":
    try:
        app = QtWidgets.QApplication(sys.argv)
        window = Window()
        window.run()
        sys.exit(app.exec_())
    except:
        exc_info = sys.exc_info()
        print(exc_info[0])
