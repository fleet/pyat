# choose function to work as energy or as amplitude
from enum import Enum
import os
from typing import List

from pyat.core.sonarscope.common.angles import IncidenceAngleBins, TransmissionAngleBins
from pyat.core.sonarscope.model.sounder_lib import SounderManufacturer
from pyat.core.utils import pyat_logger
from pyat.core.utils.exceptions.exception_list import InputError
from pyat.core.utils.monitor import ProgressMonitor
from pyat.core.utils.signal import db_to_energy, energy_to_db, amplitude_to_db, db_to_amplitude
from pyat.core.xsf import xsf_driver
from pyat.core.xsf.xsf_driver import XsfDriver


class LinearScale(Enum):
    ENERGY = 1
    AMPLITUDE = 2


class IntegrationMethod(Enum):
    MEAN = 1
    MEDIAN = 2


# set default behaviour
class Parameters:
    def __init__(self):
        self.db_to_linear = None
        self.linear_to_db = None
        self.integration_method = IntegrationMethod.MEDIAN
        self.set_linear_scale(working_scale=LinearScale.AMPLITUDE)
        self.logger = pyat_logger.logging.getLogger(__name__)
        self.monitor = ProgressMonitor()
        self.incidence_angles = IncidenceAngleBins()
        self.transmission_angles = TransmissionAngleBins()
        self.xsf_min_version = 0.5
        self.use_snippets = False
        self.use_svp = True
        self.use_insonified_area = True
        self.remove_calibration = True
        self.sounder_type = None
        self.sounder_manufacturer = None

    def setup(self, sounder_type: str):
        self.sounder_manufacturer = SounderManufacturer.from_type(sounder_type)
        if SounderManufacturer.KONGSBERG == self.sounder_manufacturer:
            self.set_linear_scale(working_scale=LinearScale.AMPLITUDE)
        elif SounderManufacturer.RESON == self.sounder_manufacturer:
            self.set_linear_scale(working_scale=LinearScale.AMPLITUDE)
            self.remove_calibration = False
            self.use_insonified_area = False
        else:
            raise NotImplementedError(
                f"Sounder {sounder_type} not supported yet, coding error in {Parameters.__name__}"
            )

    def set_linear_scale(self, working_scale: LinearScale):
        """Set if db should be converted to energy or db"""

        if working_scale == LinearScale.ENERGY:
            self.db_to_linear = db_to_energy
            self.linear_to_db = energy_to_db
        elif working_scale == LinearScale.AMPLITUDE:
            self.db_to_linear = db_to_amplitude
            self.linear_to_db = amplitude_to_db

    def set_integration_method(self, integration_method: IntegrationMethod):
        """Set if insonified area should be recomputed"""
        self.integration_method = integration_method

    def set_use_insonified_area(self, use_insonified_area: bool):
        """Set if insonified area should be recomputed"""
        self.use_insonified_area = use_insonified_area

    def set_remove_calibration(self, remove_calibration: bool):
        """Set if backscatter calibration should be removed or not"""
        self.remove_calibration = remove_calibration

    def set_use_svp(self, use_svp: bool):
        """Set if embedded sound velocity profiles should be used"""
        self.use_svp = use_svp

    def set_use_snippets(self, use_snippets: bool):
        """Set if detection backscatter should be computed from snippets"""
        self.use_snippets = use_snippets

    def check_version(self, xsf_dataset: XsfDriver):
        if xsf_dataset.get_version() < self.xsf_min_version:
            error = (
                f"Input xsf file {xsf_dataset.sounder_file.file_path} version ({xsf_dataset.get_version()}) is lower than minimal ({self.xsf_min_version}). "
                f"Please regenerate or upgrade your files."
            )
            self.logger.error(error)
            raise InputError(error)

    def check_files_version(self, input_files: List[str]):
        self.logger.info(f"Check input files version")
        for f in input_files:
            with xsf_driver.open_xsf(file_path=f) as xsf_file:
                self.check_version(xsf_dataset=xsf_file)

    def check_output_path(self, output_path: str, overwrite: bool):
        if not overwrite and os.path.exists(output_path):
            self.logger.error(f"Output file {output_path} already exist and overwrite is not allowed")
            raise IOError(f"Output file {output_path} already exist and overwrite is not allowed")


default_config = Parameters()
