#! /usr/bin/env python3
# coding: utf-8

# Operation filter for reset cells
EQUAL: str = "equal"
NOT_EQUAL: str = "not_equal"
LESS_THAN: str = "less_than"
MORE_THAN: str = "more_than"
BETWEEN: str = "between"
MISSING: str = "missing"
NOT_MISSING: str = "not_missing"
OPERATION = [EQUAL, NOT_EQUAL, LESS_THAN, MORE_THAN, BETWEEN, MISSING, NOT_MISSING]

CDI_LAYER = "CDI"
ALL_LAYERS = "All"

OPERATOR_AND = "AND"
OPERATOR_OR = "OR"

# Type of merge
FILL = "fill"
SIMPLE = "simple"
