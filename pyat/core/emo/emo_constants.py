"""
class containing all naming, convention for emo format
"""
FORMAT = "emo"

# Column name in a emo file
COL_LONGITUDE: str = "longitude"
COL_LATITUDE: str = "latitude"
COL_MIN_DEPTH: str = "minDepth"
COL_MAX_DEPTH: str = "maxDepth"
COL_MEAN_DEPTH: str = "meanDepth"
COL_STDEV: str = "standardDeviation"
COL_NB_OF_SOUNDS: str = "nbOfSounds"
COL_INTERPOLATED_CELL: str = "interpolatedCell"
COL_SMOOTHED_DEPTH: str = "smoothedDepth"
COL_SMOOTHED_MEAN_DIFFERENCE: str = "smoothedMeanDifference"
COL_CDIID: str = "CDIID"
COL_DTM_SOURCE: str = "dtmSource"

INTERPOLATED_CDI_MARKER: str = "INT"
