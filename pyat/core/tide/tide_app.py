import os
import pandas as pd
import numpy as np

import pyat.core.utils.pyat_logger as log
from pyat.core.surface_reference.sea_surface import SeaSurfaceProjector
from pyat.core.utils.monitor import DefaultMonitor
from pyat.core.tide.fes2014.fes_tide_predict import FESTideComputer
from pyat.core.tide.shom_harmonic.shom_harmonic_tide_predict import ShomTidePredictor
from pyat.core.utils.exceptions.exception_list import BadParameter


class TidePredictor:
    # pylint:disable=unused-argument
    def update_surface_reference(self, longitudes: np.ndarray, latitudes: np.ndarray, tides: np.ndarray):
        """Update surface reference to switch to LAT or MSL"""
        return tides

    def process(self, data_file, predictor, output_file, monitor):
        # monitor come here with a tick count of 10
        sub = monitor.split(10)
        sub.begin_task(name="Reading input dataset", n=1)

        ds = pd.read_csv(data_file, sep=";", names=["date", "longitude", "latitude"], skiprows=1)

        _dates = ds["date"].to_numpy(dtype="datetime64[us]")
        sub.done()
        sub = monitor.split(60)
        _lats, _lons, _dates, _computed_tides, *_ = predictor.compute_tides(
            latitudes=ds["latitude"].astype("float64").to_numpy(),
            longitudes=ds["longitude"].astype("float64").to_numpy(),
            dates=_dates.astype("datetime64[s]"),
            monitor=sub,
        )
        sub.done()
        sub = monitor.split(20)
        sub.begin_task(name="Changing reference surface from MSL to LAT", n=1)
        _tides_ref_lat = self.update_surface_reference(longitudes=_lons, latitudes=_lats, tides=_computed_tides)

        sub.done()
        sub = monitor.split(10)

        sub.begin_task(name="Exporting results", n=1)

        # format data for export as csv
        date_serie = pd.Series(data=_dates, name="date")
        lat_serie = pd.Series(data=_lats, name="latitude")
        lon_serie = pd.Series(data=_lons, name="longitude")
        tide_serie = pd.Series(data=_tides_ref_lat, name="tide")
        df = pd.concat([date_serie, lon_serie, lat_serie, tide_serie], axis=1)

        df.to_csv(output_file, sep=";", index=False, date_format='%Y-%m-%dT%H:%M:%SZ')
        sub.done()
        monitor.done()


class FESTide(TidePredictor):
    def update_surface_reference(self, longitudes: np.ndarray, latitudes: np.ndarray, tides: np.ndarray):
        proj = SeaSurfaceProjector(model_dir=self.model_dir)

        return proj.project_msl_to_lat(longitudes=longitudes, latitudes=latitudes, values=tides)

    def __init__(self, **kwargs):
        """
        :param target_file:
        :param reference_file:
        :return:
        """
        self.logger = log.logging.getLogger(FESTide.__name__)
        if "model_dir" in kwargs:
            self.model_dir = kwargs["model_dir"]
            self.fes_model_dir = os.path.join(self.model_dir, "fes2014")
        else:
            raise BadParameter("model directory path is mandatory")
        if not os.path.exists(self.fes_model_dir):
            raise BadParameter(f"fes directory does not exist ({self.fes_model_dir})")

        if "data_file" in kwargs:
            self.data_file = kwargs["data_file"]
        else:
            raise BadParameter("data_file file parameter is mandatory")

        if "output_file" in kwargs:
            self.output_file = kwargs["output_file"]
        else:
            raise BadParameter("output file parameter is mandatory")

        if "monitor" in kwargs:
            self.monitor = kwargs["monitor"]
        else:
            self.monitor = DefaultMonitor

    def __call__(self):
        self.logger.info(f"Starting fes model tide prediction for {self.data_file} dataset")
        # initialize tide predictor
        predictor = FESTideComputer(model_dir_path=self.fes_model_dir)
        self.logger.info(f"Parsing input file {self.data_file}")

        # start the processing
        self.process(self.data_file, predictor, self.output_file, self.monitor)

        self.logger.info(f"End of tide prediction result written to file {self.output_file}")


class ShomTide(TidePredictor):
    def __init__(self, **kwargs):
        """
        :param target_file:
        :param reference_file:
        :return:
        """
        self.logger = log.logging.getLogger(ShomTide.__name__)
        if "harmonic_file" in kwargs:
            self.harmonic_file = kwargs["harmonic_file"]
        else:
            raise BadParameter("harmonic_file file path is mandatory")

        if "data_file" in kwargs:
            self.data_file = kwargs["data_file"]
        else:
            raise BadParameter("data_file file parameter is mandatory")
        if "output_file" in kwargs:
            self.output_file = kwargs["output_file"]
        else:
            raise BadParameter("output file parameter is mandatory")
        if "monitor" in kwargs:
            self.monitor = kwargs["monitor"]
        else:
            self.monitor = DefaultMonitor

    def __call__(self):
        self.logger.info(
            f"Starting harmonic tide prediction for {self.data_file} dataset with {self.harmonic_file} harmonic file "
        )
        predictor = ShomTidePredictor(harmonic_file=self.harmonic_file)
        self.logger.info(f"Parsing input file {self.data_file}")

        self.process(self.data_file, predictor, self.output_file, self.monitor)

        self.logger.info(f"End harmonic tide prediction result written to file {self.output_file}")
