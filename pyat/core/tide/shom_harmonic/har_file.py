import locale

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def read_har_file(file_name: str):
    """Read a har file, this file contains harmonic data        :"""
    dictionnary = {}
    data = []
    latitudes = np.empty(shape=(0), dtype="float64")
    longitudes = np.empty(shape=(0), dtype="float64")
    with open(file_name, encoding=locale.getpreferredencoding()) as f:
        header = f.readline()
        if header == "":
            # empty file
            return
        has_data = True
        while has_data:
            read_data = _read_pack(f)
            has_data = read_data is not None
            if read_data is not None:
                data.append(read_data)
                measure, _, _ = read_data
                latitudes = np.append(latitudes, measure.latitude)
                longitudes = np.append(longitudes, measure.longitude)

    df = pd.DataFrame({"Latitude": latitudes, "Longitude": longitudes})

    df.to_csv(file_name + "_position.csv")
    # # create a panda dataframe
    #
    # import geopandas
    # df = pd.DataFrame({"Latitude": latitudes, "Longitude": longitudes})
    #
    # gdf = geopandas.GeoDataFrame(df, geometry=geopandas.points_from_xy(df.Longitude, df.Latitude))
    # print(gdf.head())
    #
    # # plot dataset
    # world = geopandas.read_file("C:/data/datasets/WorldMaps/natural_earth_vector/10m_physical/ne_10m_coastline.shp")
    #
    # # We restrict to South America.
    # ax = world.plot(color="black", edgecolor="black")
    # # We can now plot our ``GeoDataFrame``.
    # gdf.plot(ax=ax, color="red")
    #
    # plt.show()


class MeasurePoint:
    def __init__(self, latidude, longitude):
        self.latitude = latidude
        self.longitude = longitude


def _read_pack(f):
    """read a pack of data (3 lines)"""
    head_line = f.readline()
    first_data = f.readline()
    second_data = f.readline()
    if head_line == "" or first_data == "" or second_data == "":
        return None
    # decode header_line
    data = head_line.split(None)  # split with multiple spaces
    m = MeasurePoint(latidude=float(data[0]), longitude=float(data[1]))

    return (m, first_data, second_data)


if __name__ == "__main__":
    read_har_file("C:/data/datasets/Tide/harmoniques/cstFRANCE_24062009_143.har")
