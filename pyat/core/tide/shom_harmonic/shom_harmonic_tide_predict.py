import ctypes
import datetime
import os
import sys
import timeit as t

import numpy as np
import pandas as pd

import pyat.core.utils.perf_timer as perf
import pyat.core.utils.pyat_logger as log
from pyat.core.tide.common import array_commons as util
from pyat.core.utils.exceptions.exception_list import BadParameter
from pyat.core.utils.monitor import ProgressMonitor


class ShomTidePredictor:
    def __compute_library_name(self) -> str:
        """compute the Harmon library name and path"""
        libname = "clib64" + os.path.sep + "Harmon_x86_64-win64.dll"
        if sys.platform != "win32":
            raise NotImplementedError("Only windows subsystem is supported for har tide prediction")
        library_full_path = os.path.dirname(self.harmonic_file) + os.path.sep + libname
        if not os.path.exists(library_full_path):
            raise BadParameter(
                f"Failed to find harmon library {libname}, expected to be found in {os.path.dirname(self.harmonic_file)} "
            )
        return library_full_path

    def __init__(self, harmonic_file: str):
        # retain harmonic file name
        self.harmonic_file = harmonic_file
        self.logger = log.logging.getLogger(ShomTidePredictor.__name__)
        # compute library name and path
        libname = self.__compute_library_name()
        self.libDll = ctypes.WinDLL(libname)

    def __compute(
        self,
        file_handler: int,
        latitudes: np.array,
        longitudes: np.array,
        years: np.array,
        months: np.array,
        days: np.array,
        hours: np.array,
        minutes: np.array,
        seconds: np.array,
        monitor: ProgressMonitor,
    ):
        """loop over array and compute tide"""
        monitor.begin_task(name="Computing tide on input array", n=len(longitudes))
        # allocate return values
        rtides = np.full(shape=longitudes.shape, fill_value=np.nan, dtype=np.float32)
        # setup arg parameters
        self.libDll.computeTide.argtypes = [
            ctypes.c_int,
            ctypes.c_double,
            ctypes.c_double,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.c_int,
            ctypes.POINTER(ctypes.c_float),
        ]

        it = np.nditer([latitudes, longitudes, years, months, days, hours, minutes, seconds], flags=["f_index"])
        for latitude, longitude, year, month, day, hour, minute, second in it:
            ltide = ctypes.c_float(np.nan)
            self.libDll.computeTide(
                file_handler, latitude, longitude, year, month, day, hour, minute, second, ctypes.byref(ltide)
            )
            rtides[it.index] = ltide.value
            monitor.worked(1)
            if monitor.check_cancelled():
                self.logger.warning.info(f"Computation cancelled by user")
                break
        monitor.done()
        return rtides

    def compute_tides(self, latitudes: np.ndarray, longitudes: np.ndarray, dates: np.ndarray, monitor: ProgressMonitor):
        """Compute tides using harmon algorithm"""
        monitor.begin_task("Compute tides", n=10)
        monitor.worked(1)
        self.logger.info(f"Opening harmonic file {self.harmonic_file}")
        file_handler = self.libDll.setHarmonicsFile(self.harmonic_file.encode("utf-8"))

        monitor.worked(1)
        self.logger.info(f"Preparing input datasets")
        # first of all
        longitudes, latitudes, dates = util.reshape_args(lats=latitudes, lons=longitudes, dates=dates)
        # convert dates array to year, month, ... arrays
        pd_datetime = pd.to_datetime(dates)

        self.logger.info(f"Computing {len(dates)} tides")
        tide = ctypes.c_float(0)

        submonitor = monitor.split(8)
        tides = self.__compute(
            file_handler,
            latitudes=latitudes,
            longitudes=longitudes,
            years=pd_datetime.year.values.astype(np.int32),
            months=pd_datetime.month.values.astype(np.int32),
            days=pd_datetime.day.values.astype(np.int32),
            hours=pd_datetime.hour.values.astype(np.int32),
            minutes=pd_datetime.minute.values.astype(np.int32),
            seconds=pd_datetime.second.values.astype(np.int32),
            monitor=submonitor,
        )

        # tides are computed but relative to MSL we need to switch from MSL to LAT reference

        return latitudes, longitudes, dates, tides


def run(harmonic_file="C:/data/datasets/Tide/harmoniques/cstCELTIQUE_143.har"):
    print("Init tide prediction")

    date = datetime.datetime(2017, 4, 11)
    _dates = np.array([date + datetime.timedelta(seconds=item * 3600 / 10) for item in range(24 * 10 * 12)])

    _lats = np.full(_dates.shape, 48.5)
    _lons = np.full(_dates.shape, -6.5)
    predictor = ShomTidePredictor(harmonic_file=harmonic_file)
    _lats, _lons, _dates, _tides, *_ = predictor.compute_tides(
        latitudes=_lats, longitudes=_lons, dates=_dates.astype("datetime64[s]"), monitor=ProgressMonitor()
    )
    return _lats, _lons, _dates, _tides


def run_classical():
    return run()


if __name__ == "__main__":
    # Creating the time series

    # Redefining default Timer template to make 'timeit' return
    #     test's execution timing and the function return value
    perf.set_template()
    classical_time, v = t.timeit(stmt="run_classical()", setup="from __main__ import run_classical", number=1)
    print(f"Execution time classical : {classical_time} ")
