import datetime
import json
import requests
import pyat.core.utils.pyat_logger as log


# pylint: disable=W0621


class TideGaugeGetter:
    """TideGaugeGetter is a wrapper over shom tide webservices allowing to retrieve tide values on a given set of tidegauges"""

    def __init__(self):
        self.logger = log.logging.getLogger(TideGaugeGetter.__name__)

    def get_tidegauges(self):
        """Retrieve and return the list of available tide gauges"""
        r = requests.get("https://services.data.shom.fr/maregraphie/service/tidegauges", timeout=10)
        if r.status_code != 200:
            self.logger.info(f"Error while querying data {r.status_code} : {r.reason}")
        data = json.loads(r.text)
        names = [x["name"] for x in data]
        longitude = [x["longitude"] for x in data]
        latitude = [x["latitude"] for x in data]
        shom_ids = [x["shom_id"] for x in data]
        return names, longitude, latitude, shom_ids

    def get_tidegauge(self, tidegauge_name):
        """Retrieve and return data from tidegauge of the given name"""
        r = requests.get("https://services.data.shom.fr/maregraphie/service/tidegauges", timeout=10)
        if r.status_code != 200:
            self.logger.info(f"Error while querying data {r.status_code} : {r.reason}")
            return []
        data = json.loads(r.text)
        selected = [x for x in data if tidegauge_name in x["name"]]
        return selected

    def get_tidegauge_details(self, shom_id):
        """Retrieve and return data from tidegauge of the given name"""
        r = requests.get(f"https://services.data.shom.fr/maregraphie/service/completetidegauge/{shom_id}", timeout=10)
        if r.status_code != 200:
            self.logger.info(f"Error while querying data {r.status_code} : {r.reason}")
            return []
        data = json.loads(r.text)

        return data

    def get_values(
        self,
        tidegauge_name: str,
        date_start: datetime,
        date_end: datetime,
        sampling_interval_minute=None,
        data_source=1,
    ):
        """Query web service to retrieve tidegauge values

        Args:
            tidegauge_name : the tide gauge name
            date_start: the starting date of data, utc
            date_end: the end date of data, utc
            sampling_interval_minute: if given a sampling interval in minute
            data_source: the datasource 1 = Brutes haute fréquence, 2 = Brutes temps différé, 3 = Validées temps différé, 4 = Validées horaires, 5 = Brutes horaires
        """
        self.logger.info(f"Querying shom webservice for tidegauge {tidegauge_name} ")
        selected = self.get_tidegauge(tidegauge_name)
        if len(selected) == 0:
            self.logger.error(f"Tidegauge {tidegauge_name} not found")
            return [], []

        source_id = selected[0]["shom_id"]
        self.logger.info(f"Found tidegauge {selected[0]['name']} with id {source_id}")

        # format date to be web service compliants
        date_start_str = date_start.isoformat().replace(" ", "%3A")
        date_end_str = date_end.isoformat().replace(" ", "%3A")

        self.logger.info(f"Querying tidegauge data ")
        request_text = f"https://services.data.shom.fr/maregraphie/observation/json/{source_id}?sources={data_source}&dtStart={date_start}&dtEnd={date_end}&"

        if sampling_interval_minute is not None:
            request_text = f"{request_text}interval={sampling_interval_minute}"
        r = requests.get(request_text, timeout=10)
        if r.status_code != 200:
            self.logger.error(f"Error while querying data {r.status_code} : {r.reason}")
            return [], []
        data_returned = json.loads(r.text)
        data = data_returned["data"]
        self.logger.info(f"Retrieved {len(data)} data")

        value = [float(x["value"]) for x in data]
        # Parse date time
        time = [datetime.datetime.strptime(x["timestamp"], "%Y/%m/%d %H:%M:%S") for x in data]
        # datetime are parse as local time value, switch to utc (cannot be done with strptime)
        time = [
            datetime.datetime(t.year, t.month, t.day, t.hour, t.minute, t.second, tzinfo=datetime.timezone.utc)
            for t in time
        ]

        return time, value


if __name__ == "__main__":
    predictor = TideGaugeGetter()
    names, longitude, latitude, shom_ids = predictor.get_tidegauges()
    ref = {}
    for name, shom_id in zip(names, shom_ids):
        details = predictor.get_tidegauge_details(shom_id)
        if "npbma" in details and "##" not in details["npbma"]:
            ref[name] = details["npbma"]
        else:
            print(f"{name} has no npbma value {details}")
    print(ref)
    # # Creating the time series
    # date_start = datetime.datetime(year=2021, month=4, day=21, hour=4, minute=15, second=0, tzinfo=datetime.timezone.utc)
    # date_end = datetime.datetime(year=2021, month=4, day=22, hour=6, minute=30, second=0, tzinfo=datetime.timezone.utc)
    # # # 1 = Brutes haute fréquence, 2 = Brutes temps différé, 3 = Validées temps différé, 4 = Validées horaires, 5 = Brutes horaires
    # t,v=predictor.get_values("LE_CONQUET",date_start=date_start,date_end=date_end,data_source=1)
    # import matplotlib.pyplot as plt
    # import matplotlib.dates as mdates
    #
    # fig, ax = plt.subplots()
    # ax.plot(t,v,label="Brutes haute fréquence")
    # # t,v=predictor.get_values("BREST",date_start=date_start,date_end=date_end,data_source=2)
    # # plt.plot(t,v,label="Brutes temps différé")
    # # t, v = predictor.get_values("BREST", date_start=date_start, date_end=date_end, data_source=3)
    # # plt.plot(t, v, label="Validées temps différé")
    # # t, v = predictor.get_values("BREST", date_start=date_start, date_end=date_end, data_source=4)
    # # plt.plot(t, v, label="Validées horaires")
    # # t, v = predictor.get_values("BREST", date_start=date_start, date_end=date_end, data_source=5)
    # # plt.plot(t, v, label="Brutes horaires")
    # plt.legend()
    # ax.xaxis.set_major_formatter(mdates.DateFormatter("%Y/%m/%d %H:%M:%S"))
    # plt.setp(ax.get_xticklabels(), rotation=30, ha="right")
    # plt.grid()
    # plt.show()
