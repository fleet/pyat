import numpy as np


def reshape_args(lons: np.ndarray, lats: np.ndarray, dates: np.ndarray):
    """reshape parameters to have a coherent and vectorized set of arguments

    Returns:
    a tuple containing lons, lats and dates reshaped values
    """
    if lats.shape != lons.shape:
        raise ValueError(f"Latitudes and Longitudes array must have the same size")

        # if we have only one data, we duplicate it to have the same count as geo points values
    if len(dates) == 1:
        dates = np.full(lats.shape, dates[0])
        # if we have only one geopoint and several dates, we duplicate geopoints to have the same count as dates
    if len(lons) == 1:
        lons = np.full(dates.shape, lons[0])
        lats = np.full(dates.shape, lats[0])

    if dates.shape != lons.shape:
        raise ValueError(f"(Latitudes, Longitudes) array shape {lons.shape} differs from dates shapes {dates.shape}")
    lons = lons.ravel()
    lats = lats.ravel()
    dates = dates.ravel()
    return lons, lats, dates
