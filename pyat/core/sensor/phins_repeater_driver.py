import re
from datetime import date, timedelta, datetime, time
from pathlib import Path

import numpy as np
import pandas as pd

from pyat.core.sensor.nmea_parser import NMEASentence, ParseError, ChecksumError


def read_phins_repeater_as_df(phins_repeater_file: str) -> pd.DataFrame:
    """
    Reads a phins repeater file and convert it to time-indexed Dataframe.
    :param phins_repeater_file: input phins repeater file containing nmea sentences
    :return: time-indexed Dataframe with decoded phins data
    """
    # Get file starting date and time from filename
    date_phins, time_phins = get_phins_repeater_date(phins_repeater_file)

    # Create an empty list to hold parsed cycles
    data_cycles = []
    data_cycle = {}

    with open(phins_repeater_file, encoding='utf-8') as file:
        first_nmea_msg_type = None
        # Iterate through the NMEA lines
        for line in file.readlines():
            try:
                # try to parse NMEA data
                nmea_data = NMEASentence.parse(line)
                if not first_nmea_msg_type:
                    # store first NMEA msg id to further detect new cycle beginning
                    first_nmea_msg_type = nmea_data["type"]
            except (ParseError, ChecksumError, NotImplementedError):
                continue

            # if new cycle beginning
            if nmea_data["type"] == first_nmea_msg_type and data_cycle:
                # check presence of "time" in data_cycle
                if not "time" in data_cycle.keys():
                    raise IOError(f"File is not a valid 'phins repeater' file: {phins_repeater_file}\n"
                                  f"\tNo 'TIME' NMEA sentence found")
                # store the current cycle and start a new one
                data_cycles.append(data_cycle)
                data_cycle = {}

            # remove "type" key and add data to current cycle
            nmea_data.pop('type')
            data_cycle.update(nmea_data)

        # Add the last cycle
        if data_cycle:
            data_cycles.append(data_cycle)

        # Converts the list of data cycles into a DataFrame
        phins_repeater_df = pd.DataFrame(data_cycles)

        # Combine date and time data
        is_time_cols = phins_repeater_df.columns.to_series().str.contains('time')
        time_cols = phins_repeater_df.columns[is_time_cols]

        phins_repeater_df[time_cols] = phins_repeater_df[time_cols].apply(
            combine_date_and_time, start_date=date_phins, start_time=time_phins, axis=1, raw=True)

        # return a datetime-indexed dataframe
        phins_repeater_df.set_index('time', inplace=True)

        return phins_repeater_df


def get_phins_repeater_date(phins_repeater_file: str) -> (date, time):
    """
    Returns date of phins data deduced from the filename.
    Raises IO error if filename doesn't comply with pattern "PHINS_REPEATER_YYYY-MM-DD_HH-mm-SS_XXX".
    """
    # regex to extract to match filename pattern and extract date
    filename_re = re.compile(
        r"PHINS_REPEATER_(?P<date>\d{4}-\d{2}-\d{2})_(?P<time>\d{2}-\d{2}-\d{2})_\w+"
    )
    match = filename_re.match(Path(phins_repeater_file).stem)
    if not match:
        raise IOError(f"File is not a 'phins repeater' file: {phins_repeater_file}\n"
                      f"\tFile name pattern must be 'PHINS_REPEATER_YYYY-MM-DD_HH-mm-SS_XXX'")

    return (date.fromisoformat(match.group('date')),
            time.fromisoformat(match.group('time').replace('-', ':')))


def combine_date_and_time(time_values: np.ndarray, start_date: date, start_time: time) -> np.ndarray:
    """
    Combines date, read from filename, with time, read from file content, into a datetime with date incrementation logic.
    """
    full_datetimes = np.empty_like(time_values)

    # Uses static variables (last_times and last_dates) to keep track of the last date and time,
    # allowing it to maintain state between calls to handle date incrementation properly
    if 'last_times' not in combine_date_and_time.__dict__:
        combine_date_and_time.last_times = np.repeat(start_time, len(time_values))
    if 'last_dates' not in combine_date_and_time.__dict__:
        combine_date_and_time.last_dates = np.repeat(start_date, len(time_values))

    for idx, (current_time, last_time, last_date) in enumerate(
        zip(time_values, combine_date_and_time.last_times, combine_date_and_time.last_dates)):

        if not isinstance(current_time, time) and np.isnan(current_time):
            full_datetimes[idx] = pd.NaT
        else:
            # Combine last date with the current time
            full_datetimes[idx] = datetime.combine(last_date, current_time)

            if current_time < last_time:
                # Increment the date if current time is less than the last time
                full_datetimes[idx] += timedelta(days=1)
                # and update the corresponding last date value
                combine_date_and_time.last_dates[idx] += timedelta(days=1)

            # Update last times for the next call
            combine_date_and_time.last_times[idx] = current_time

    return full_datetimes


if __name__ == "__main__":
    # filepath = r"E:\ULYXDEMO24\PHINS_REPEATER_2024-06-06_17-04-56_001.txt"
    # filepath = r"E:\ULYXDEMO24\PHINS_REPEATER_2024-06-06_17-04-56_testshort.txt"
    filepath = r"E:\ULYXDEMO24\PHINS_REPEATER_2024-06-06_17-04-56_testultrashort.txt"
    df = read_phins_repeater_as_df(filepath)
    print(df)
