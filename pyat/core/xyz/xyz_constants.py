#! /usr/bin/env python3
# coding: utf-8

"""Class containing all naming, convention for xyz format
"""
# XYZ
FORMAT = "xyz"
EXTENSION = ".xyz"

# Name columns
COL_LAT = "lat"
COL_LON = "lon"
COL_DEPTH = "elevation"
