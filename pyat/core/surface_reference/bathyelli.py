import os
from timeit import default_timer as timer
import numpy as np
import pandas as pd
from scipy.spatial import KDTree as tree
import pyat.core.utils.pyat_logger as log

# pylint: disable=W0621


class BathyElli:
    """Wrap a bathyElli fileset as given by the SHOM
    Allow to query value for a longitude/latitude array
    Values returned are the ellipsoidal height (in meters) of the vertical reference (hydro zero)

    """

    def __init__(self, source_directory: str):
        """
        Args:
            source_directory: root directory containg the source directory as delivered by Shom (either BATHYELLI_ZH_ell_V1_1 or BATHYELLI_ZH_ell_V2)
        """
        self.source_dir = source_directory
        self.atlantique = os.path.join(source_directory, "BATHYELLI_ZH_ell_V2", "BathyElliv2.0_ATL_ZH.glhi")
        self.mediterranee = os.path.join(source_directory, "BATHYELLI_ZH_ell_V2", "BathyElliv2.0_MED_ZH.glhi")
        if not os.path.exists(self.atlantique):
            self.atlantique = os.path.join(source_directory, "BATHYELLI_ZH_ell_V1_1", "ZH_ell_BATHYELLI_atlantique_v1_1_2014.glhi")
            self.mediterranee = os.path.join(source_directory, "BATHYELLI_ZH_ell_V1_1", "ZH_ell_BATHYELLI_mediterranee_v1_1_2014.glhi")
        self.logger = log.logging.getLogger(self.__class__.__name__)

    def __compute(self, filename: str) -> (tree, np.ndarray, np.ndarray, np.ndarray):
        """Parse bathyElli dataset"""
        ts = timer()
        self.logger.info(f"read from numpy in , {(timer() - ts):.2f} sec.")

        ts = timer()
        df = pd.read_csv(
            filename, delimiter=" ", header=None, names=["Longitude", "Latitude", "height", "incertitude "]
        )
        self.logger.info(f"read from panda in , {(timer() - ts):.2f} sec.")
        longitude = df["Longitude"].to_numpy().reshape((1, -1))
        latitude = df["Latitude"].to_numpy().reshape((1, -1))
        height = df["height"].to_numpy().reshape((1, -1))
        v2 = np.c_[df["Longitude"].to_numpy(), df["Latitude"].to_numpy()]
        ts = timer()
        spatial_tree = tree(v2)
        self.logger.info(f"create kdtree index in , {(timer() - ts):.2f} sec.")
        return spatial_tree, height, longitude, latitude

    def __get_ellipsoid_to_zerohydro(
        self, longitudes: np.ndarray, latitudes: np.ndarray, filename: str, cutoff_distance_deg
    ) -> np.ndarray:
        """Retrieve bathyelli nearest value for a given bathyelli dataset (atlantique or med)"""
        values = np.hstack((longitudes.ravel().reshape((-1, 1)), latitudes.ravel().reshape((-1, 1))))
        tree, height_atl, *_ = self.__compute(filename)
        distance, index = tree.query(values)
        height_atl = height_atl.ravel()
        values_read = height_atl[index]
        values_read[distance > cutoff_distance_deg] = np.nan
        return values_read.reshape(latitudes.shape)

    def get_ellipsoid_to_zerohydro(
        self, longitudes: np.ndarray, latitudes: np.ndarray, cutoff_distance_deg=1 / 20
    ) -> np.ndarray:
        """Retrieve nearest bathyelli values for both model  atlantique and med.
        Nearest value if found using a KDTree, distance is computed in deg
        (even though there is a distorsion since length of an arc in lat differs from lon)
        Then a merge between both model is done.

        Values returned are the ellipsoidal height (in meters) of the vertical reference (hydro zero) or nan if out of scope

        """
        values_atl = self.__get_ellipsoid_to_zerohydro(
            longitudes=longitudes,
            latitudes=latitudes,
            filename=self.atlantique,
            cutoff_distance_deg=cutoff_distance_deg,
        )
        values_med = self.__get_ellipsoid_to_zerohydro(
            longitudes=longitudes,
            latitudes=latitudes,
            filename=self.mediterranee,
            cutoff_distance_deg=cutoff_distance_deg,
        )
        # merge two results
        return np.nansum(np.dstack((values_atl, values_med)), 2)


if __name__ == "__main__":
    reference = BathyElli("C:\\data\\datasets\\Tide")

    # compute a grid around bathyel
    latitudes = np.arange(41, 51.5, 1 / 20)
    longitudes = np.arange(-8, 11, 1 / 20)
    xi, yi = np.meshgrid(longitudes, latitudes)
    values = reference.get_ellipsoid_to_zerohydro(xi, yi)

    # import matplotlib.pyplot as plt
    #
    # plt.scatter(xi.ravel(), yi.ravel(), c=values.ravel())
    # plt.show(block=True)
