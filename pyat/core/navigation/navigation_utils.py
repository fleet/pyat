import warnings
from typing import List

import numpy as np

from pyat.core.navigation.abstract_navigation import AbstractNavigation
from pyat.core.navigation.navigation_data import NavigationData


def interpolate(navigation_data: NavigationData, interpolation_time: np.ndarray) -> NavigationData:
    """Interpolate navigation data based on the given time sensor
    :param navigation_data: input navigation data
    :param interpolation_time: time values used to get interpolated data
    :return: interpolated navigation data
    """
    # interpolate data
    time_navigation = navigation_data.times
    if time_navigation[0] > interpolation_time[0]:
        raise Exception(
            f"Not supported case : Starting date ({time_navigation[0]}) from navigation file is higher than starting date of time sensor ({interpolation_time[0]})"
        )

    if time_navigation[-1] < interpolation_time[-1]:
        raise Exception(
            f"Not supported case : Last date  ({time_navigation[-1]}) from navigation file is less than last date of time sensor ({interpolation_time[-1]})"
        )

    # We need to change time to float in order to be able to interpolate data
    # We substract the lowest date as a reference date
    reference_date = time_navigation[0]
    time_navigation_float = (time_navigation - reference_date) / np.timedelta64(1, "s")
    time_sensor_float = (interpolation_time - reference_date) / np.timedelta64(1, "s")

    # Need to check for interpolation around 180/-180 for longitudes
    longitudes = np.interp(time_sensor_float, time_navigation_float, navigation_data.longitudes)
    latitudes = np.interp(time_sensor_float, time_navigation_float, navigation_data.latitudes)

    altitudes = (
        np.interp(time_sensor_float, time_navigation_float, navigation_data.altitudes)
        if navigation_data.altitudes is not None
        else None
    )

    heading = None
    if navigation_data.headings is not None:
        # use 'unwrap' method to get a correct interpolation of heading angles
        heading_unwrapped = navigation_data.headings
        heading_unwrapped[~np.isnan(heading_unwrapped)] = np.unwrap(
            heading_unwrapped[~np.isnan(heading_unwrapped)], period=360
        )
        heading = np.interp(time_sensor_float, time_navigation_float, heading_unwrapped) % 360

    speed = (
        np.interp(time_sensor_float, time_navigation_float, navigation_data.speeds)
        if navigation_data.get_speeds() is not None
        else None
    )

    result = NavigationData(
        name=navigation_data.name,
        times=interpolation_time,
        latitudes=latitudes,
        longitudes=longitudes,
        headings=heading,
        altitudes=altitudes,
        speeds=speed,
    )
    return result


def merge(navigation_list: List[AbstractNavigation]) -> AbstractNavigation:
    """Merges several navigation."""
    aggr_names = []
    aggr_times_nav = []
    aggr_longitudes_nav = []
    aggr_latitudes_nav = []
    aggr_altitudes_nav = []
    aggr_speeds = []
    aggr_headings = []

    # read navigation from files
    for nav_data in navigation_list:
        aggr_names.append(nav_data.get_name())
        aggr_times_nav.extend(nav_data.get_times())
        aggr_longitudes_nav.extend(nav_data.get_longitudes())
        aggr_latitudes_nav.extend(nav_data.get_latitudes())
        aggr_headings.extend(nav_data.get_headings())
        aggr_altitudes_nav.extend(nav_data.get_altitudes())
        aggr_speeds.extend(nav_data.get_speeds())

    # sort nav points by time
    indexer = np.asarray(aggr_times_nav).argsort()

    warnings.filterwarnings("ignore", message="Warning: converting a masked element to nan")
    merged_nav = NavigationData(
        name="; ".join(aggr_names),
        times=np.asarray(aggr_times_nav)[indexer],
        latitudes=np.asarray(aggr_latitudes_nav)[indexer],
        longitudes=np.asarray(aggr_longitudes_nav)[indexer],
        headings=np.asarray(aggr_headings)[indexer],
        altitudes=np.asarray(aggr_altitudes_nav)[indexer],
        speeds=np.asarray(aggr_speeds)[indexer],
    )
    return merged_nav
