import os
from typing import List, Tuple, Union

import numpy as np
from osgeo import gdal, ogr

import pyat.core.utils.pyat_logger as log
from pyat.core.utils.monitor import ProgressMonitor

logger = log.logging.getLogger(__name__)

STR_TO_GDAL_ALGORITHM = {
    "near": gdal.GRA_NearestNeighbour,
    "bilinear": gdal.GRA_Bilinear,
    "cubic": gdal.GRA_Cubic,
    "cubicspline": gdal.GRA_CubicSpline,
    "lanczos": gdal.GRA_Lanczos,
    "average": gdal.GRIORA_Average,
    "mode": gdal.GRA_Mode,
    "max": gdal.GRA_Max,
    #        "sum": gdal.GRA_Sum,
    "min": gdal.GRA_Min,
    "med": gdal.GRA_Med,
    "q1": gdal.GRA_Q1,
    "q3": gdal.GRA_Q3,
}

EXT_TO_OGR_DRIVER = {".shp": "ESRI Shapefile", ".kml": "KML"}


def translate_algorithm(arg_name: str, algo: str):
    if not algo in STR_TO_GDAL_ALGORITHM:
        raise ValueError(f"Invalid value '{algo}' for argument {arg_name}")
    return STR_TO_GDAL_ALGORITHM[algo]


class GDALDataset:
    """
    A GDAL context manager for raster dataset
    Allow the opening (read-only) of a GDAL dataset in a "with" statement
    """

    def __init__(self, filename):
        self.filename = filename
        self.dataset = None

    def __enter__(self) -> gdal.Dataset:
        """open the GDAL dataset in read-only mode"""
        self.dataset = gdal.Open(self.filename, gdal.GA_ReadOnly)
        if self.dataset is None:
            raise IOError(f"Failed to open dataset: {self.filename}")
        return self.dataset

    def __exit__(self, exc_type, exc_value, traceback):
        """ensure GDAL dataset closing on exit"""
        if self.dataset is not None:
            self.dataset = None


class OGRDataset:
    """
    an OGR context manager for vector dataset
    Allow the creation and opening of a OGR dataset (.shp for ex) in a "with" statement
    Only supports formats in EXT_TO_OGR_DRIVER
    """

    def __init__(self, file_path):
        self.file_path = file_path
        self.dataset = None

    def __get_driver_from_ext(self) -> ogr.Driver:
        """ Return the OGR driver corresponding to file extension """
        _, ext = os.path.splitext(self.file_path)
        if ext not in EXT_TO_OGR_DRIVER:
            raise ValueError(f"driver for '{ext}' not implemented yet in {self.__class__.__name__}")
        return ogr.GetDriverByName(EXT_TO_OGR_DRIVER[ext])

    def __enter__(self) -> ogr.DataSource:
        """ create and return the OGR datasource """
        driver = self.__get_driver_from_ext()
        self.dataset = driver.CreateDataSource(self.file_path)
        if self.dataset is None:
            raise IOError(f"Failed to create OGR dataset {self.file_path}")
        return self.dataset

    def __exit__(self, exc_type, exc_value, traceback):
        """ ensure OGR datasource writing and closing on exit """
        if self.dataset is not None:
            self.dataset.FlushCache()
            self.dataset = None


class DatasetWrapper:
    """A gdal dataset wrapper allowing to add a few metadata on dataset"""

    def __init__(self, dataset: gdal.Dataset):
        self.dataset = dataset


class TemporaryDataset(DatasetWrapper):
    """a Temporary dataset, the file associated with the dataset will be deleted by the destructor"""

    # Initializing
    def __init__(self, dataset: gdal.Dataset, filepath: str, **kwargs):
        super().__init__(dataset)
        self.filepath = filepath
        if "verbose" in kwargs:
            self.verbose = bool(kwargs["verbose"])
        else:
            self.verbose = False

    def __del__(self):
        """Temporary dataset destructor, close dataset and remove associated file"""
        # close the dataset
        if self.dataset is not None:
            del self.dataset

        try:
            if self.verbose:
                print("delete file ", self.filepath)
            os.remove(self.filepath)
        except Exception as e:
            logger.warning(f"Error while deleting file {self.filepath} {e}")


def gdal_progress_callback(pct: float, msg: str, callback_data: List[Union[float, str, ProgressMonitor]]) -> int:
    """
    A GDAL Callback progress function, activated by GDAL processes and used to display progression
    callback_data contains :
        - the last printed percent of progression
        - the last printed message
        - A ProgressMonitor
    """
    progress = callback_data[0]
    lastmsg = callback_data[1]
    monitor = callback_data[2]

    # Stop on cancel
    if monitor.check_cancelled():
        monitor.logger.warning(f"Cancelled")
        return 0  # Stop
    # Init progression
    if msg != lastmsg:
        callback_data[1] = msg
        monitor.begin_task(name=msg, n=100)
    # Update progression every 10%
    if pct * 100 >= progress + 10:
        callback_data[0] = progress + 10
        monitor.worked(10)

    return 1  # Continue


def netcdf_to_gdal(value: np.ndarray):
    """Convert a netcdf dataset array with origin left bottom to a gdal dataset array with upper left origin convention"""
    return value[::-1, :]


def gdal_to_netcdf(gdal_dataset: gdal.Dataset):
    """Convert a gdal dataset array with upper left origin convention to a netcdf array convention with origin left bottom"""
    return gdal_dataset.ReadAsArray()[::-1, :]


def get_x_y_coordinates(gdal_dataset: gdal.Dataset):
    """
    Compute and return two X (longitude) and Y (latitude) coordinates vector from a gdal dataset
    The coordinates computed refers to the coordinates at the center of the cells
    Args:
        gdal_dataset:

    Returns:
        (x,y) the coordinates vector
    """
    (GT_0, GT_1, GT_2, GT_3, GT_4, GT_5) = gdal_dataset.GetGeoTransform()
    Xvalues = np.arange(0, gdal_dataset.RasterXSize)
    Yvalues = np.arange(0, gdal_dataset.RasterYSize)

    Xgeo = GT_0 + (Xvalues + 0.5) * GT_1 + 0 * GT_2
    Ygeo = GT_3 + 0 * GT_4 + (Yvalues + 0.5) * GT_5
    return Xgeo, Ygeo


def gdal_raster_to_geo(gdal_dataset: gdal.Dataset, Xpixel, Ypixel):
    """
    Convert Xpixel and Ypixel coordinate to geographic coordinates
    Xpixel and Ypixel should be of the same size
    Returns:
        Xgeo, Ygeo
    """
    (GT_0, GT_1, GT_2, GT_3, GT_4, GT_5) = gdal_dataset.GetGeoTransform()
    Xgeo = GT_0 + Xpixel * GT_1 + Ypixel * GT_2
    Ygeo = GT_3 + Xpixel * GT_4 + Ypixel * GT_5
    return Xgeo, Ygeo


def export_to_tiff(reference_dataset: gdal.Dataset, output_filename: str, input_raster_band: int = 1):
    """Export a gdal dataset to a tiff file"""
    dst_ds = gdal.GetDriverByName("GTiff").Create(
        output_filename,
        xsize=reference_dataset.RasterXSize,
        ysize=reference_dataset.RasterYSize,
        bands=1,
        eType=gdal.GDT_Float32,
    )
    dst_ds.SetGeoTransform(reference_dataset.GetGeoTransform())  # specify coords
    dst_ds.SetProjection(reference_dataset.GetProjection())
    band = reference_dataset.GetRasterBand(input_raster_band)
    dst_ds.GetRasterBand(1).WriteArray(band.ReadAsArray())
    dst_ds.FlushCache()
    print(f"Exporting dataset to tiff file {output_filename}")
