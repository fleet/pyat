"""Helpers functions to handle lonlat conversion to UTM"""


def lon_lat_to_utm_proj4(longitude, latitude):
    utm_band = str(_lonlat_to_zone_number(longitude, latitude))
    w = '' if latitude >= 0 else ' +south'
    return f'+proj=utm +zone={utm_band}{w} +ellps=WGS84 +datum=WGS84 +units=m +no_defs'


def _lonlat_to_zone_number(longitude, latitude):
    """Copied from utm python package"""
    if 56 <= latitude < 64 and 3 <= longitude < 12:
        return 32

    if 72 <= latitude <= 84 and longitude >= 0:
        if longitude < 9:
            return 31
        elif longitude < 21:
            return 33
        elif longitude < 33:
            return 35
        elif longitude < 42:
            return 37

    return int((longitude + 180) / 6) + 1
