import math
from math import atan2, sqrt

import numpy as np
from scipy.spatial.transform import Rotation as R


def get_roll_matrix(roll: float) -> np.ndarray:
    """
    Return the roll transformation matrix
    """
    return R.from_euler('X', roll, degrees=False).as_matrix()


def get_pitch_matrix(pitch: float) -> np.ndarray:
    """
    Return the pitch transformation matrix
    """
    return R.from_euler('Y', pitch, degrees=False).as_matrix()


def get_vcs_attitude(pitch: float, roll: float) -> np.ndarray:
    """
    Return the attitude matrix in Vessel Coordinate System
    """
    return R.from_euler('YX', [pitch, roll], degrees=False).as_matrix()


def transform_vcs_to_scs(pitch: float, roll: float, x_vcs: np.ndarray) -> np.ndarray:
    """
    Transform coordinates from the Vessel Coordinate System to Surface Coordinate System
    """
    return R.from_euler('YX', [pitch, roll], degrees=False).apply(x_vcs)


def to_euler_angles(rotation: R):
    """
    Retrieve Tait-Bryan angles from rotation
    Ref: https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles#Quaternion_to_Euler_angles_(in_3-2-1_sequence)_conversion
    Equivalent to rotation.as_euler("ZYX")
    """

    # transform as quaternion
    q = rotation.as_quat()

    qx, qy, qz, qw = q
    #roll (x-axis rotation)
    sinr_cosp = 2 * (qw * qx + qy * qz)
    cosr_cosp = 1 - 2 * (qx * qx + qy * qy)
    roll = atan2(sinr_cosp, cosr_cosp)

    # pitch (y-axis rotation)
    sinp = sqrt(1 + 2 * (qw * qy - qx * qz))
    cosp = sqrt(1 - 2 * (qw * qy - qx * qz))
    pitch = 2 * atan2(sinp, cosp) - math.pi / 2

    # yaw (z-axis rotation)
    siny_cosp = 2 * (qw * qz + qx * qy)
    cosy_cosp = 1 - 2 * (qy * qy + qz * qz)
    yaw = atan2(siny_cosp, cosy_cosp)

    return [roll, pitch, yaw]

if __name__ == "__main__":
    print(transform_vcs_to_scs(0.63, 1.61, [4.223, 0.01, 1.735]))
