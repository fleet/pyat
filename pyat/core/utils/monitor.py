#! /usr/bin/env python3
# coding: utf-8

import logging

import pyat.core.utils.pyat_logger as log


class ProgressMonitor:
    """
    Same as NullProgressMonitor in Java
    """

    def __init__(self, logger: logging.Logger | None = None):
        # accumulation of done ticks (see self.worked())
        self.acc = 0
        # Name of the current task
        self.desc = ""
        # the total number of work units
        self.size = 0
        self.father = None
        self.father_ticks = 0
        self.last_log_percent = 0
        self.logger = logger if logger is not None else log.logging.getLogger(ProgressMonitor.__name__)

    def _print_level(self):
        if self.father is not None:
            return "#" + self.father._print_level()
        return "#"

    def begin_task(self, name, n):
        """
        Notifies that the main task is beginning.  This must only be called once
        on a given progress monitor instance.

        name : the name (or description) of the main task
        n : the total number of work units into which the main task is been subdivided.
        """
        self.desc = name
        self.size = n
        self.logger.debug(f"{self._print_level()}Task {name} starting")

    def set_work_remaining(self, n):
        """
        Sets the work remaining for this monitor instance. This is the total number
        of ticks that may be reported by all subsequent calls to worked(int), split(int), etc.
        This may be called many times for the same monitor instance. When this method
        is called, the remaining space on the progress monitor is redistributed into the given
        number of ticks.
        """

        self.size = n
        self.acc = 0

    def done(self):
        """
        Notifies that the work is done; that is, either the main task is completed
        or the user canceled it. This method may be called more than once
        (implementations should be prepared to handle this case).
        """

        self.logger.debug(f"{self._print_level()}Task {self.desc} done 100%")
        if self.father is not None:
            self.father.worked(self.father_ticks)

    def check_cancelled(self):
        """
        Returns whether cancellation of current operation has been requested.
        Long-running operations should poll to see if cancelation
        has been requested.
        """

    def set_cancelled(self):
        """
        Sets the cancel state to True.
        """

    def split(self, ticks):
        """
        Creates a sub progress monitor that will consume the given number of ticks from the
        receiver. It is not necessary to call <code>beginTask</code> or <code>done</code> on the
        result. However, the resulting progress monitor will not report any work after the first
        call to done() or before ticks are allocated. Ticks may be allocated by calling self.beginTask())
        or self.setWorkRemaining()).
        """
        sm = ProgressMonitor()
        sm.father = self
        sm.father_ticks = ticks
        sm.size = ticks
        return sm

    def worked(self, doneTicks):
        """
        Notifies that a given number of work unit of the main task
        has been completed. Note that this amount represents an
        installment, as opposed to a cumulative amount of work done
        to date.

        doneTicks : a non-negative number of work units just completed
        """

        if self.size > 0:
            self.acc = self.acc + doneTicks
            percent = 100.0 * self.acc / self.size
            # log progress every 10%
            if percent - self.last_log_percent >= 10:
                self.logger.info(f"{self._print_level()}Task {self.desc} {int(percent)}% done")
                self.last_log_percent = percent
        else:
            self.logger.warning(f"progress bar is not well initialized")


DefaultMonitor = ProgressMonitor()


class JavaMonitor(ProgressMonitor):
    """Class used to report task progress to java"""

    def __init__(self, javaMonitorObject):
        super().__init__()
        self.javaMonitorObject = javaMonitorObject

    def begin_task(self, name, n):
        super().begin_task(name, n)

        self.javaMonitorObject.beginTask(name, n)

    def set_work_remaining(self, n):
        super().set_work_remaining(n)
        self.javaMonitorObject.setWorkRemaining(n)

    def done(self):
        super().done()
        self.javaMonitorObject.done()

    def check_cancelled(self):
        return self.javaMonitorObject.isCanceled()

    def set_cancelled(self):
        self.javaMonitorObject.setCancelled()

    def split(self, ticks):
        """Returns a new monitor for a new subtask"""
        # self.logger.info(f"Create new subtask of size {ticks}")

        javaSubMonitor = self.javaMonitorObject.split(ticks)
        if javaSubMonitor:
            return JavaMonitor(javaSubMonitor)
        else:
            return DefaultMonitor

    def worked(self, doneTicks: int):
        """Reports progress of task
        doneTicks can be float
        """
        super().worked(doneTicks)
        self.javaMonitorObject.worked(doneTicks)
