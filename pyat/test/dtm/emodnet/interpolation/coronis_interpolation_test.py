#! /usr/bin/env python3
# coding: utf-8

import tempfile as tmp

import numpy as np

import pyat.core.dtm.dtm_driver as dtm_driver
import pyat.core.dtm.emodnet.dtm_standard_constants as DTM
import pyat.core.dtm.interpolation.amle_interpolation_process as amle
import pyat.core.dtm.interpolation.ccst_interpolation_process as ccst
import pyat.core.dtm.interpolation.cubic_interpolation_process as cubic
import pyat.core.dtm.interpolation.harmonic_interpolation_process as harmonic
import pyat.core.dtm.interpolation.linear_interpolation_process as linear
import pyat.core.dtm.interpolation.navier_stokes_interpolation_process as navier_stokes
import pyat.core.dtm.interpolation.nearest_interpolation_process as nearest
import pyat.core.dtm.interpolation.purbf_interpolation_process as purbf
import pyat.core.dtm.interpolation.rbf_interpolation_process as rbf
import pyat.core.dtm.interpolation.shiftmap_interpolation_process as shiftmap
import pyat.core.dtm.interpolation.telea_interpolation_process as telea
import pyat.core.dtm.interpolation.tv_interpolation_process as tv
import pyat.test.generator.dtm_generator as dtm_generator
import pyat.test.generator.kml_generator as kml_generator
from pyat.core.dtm.interpolation.heightmap_interpolation_process import HeightmapInterpolationProcess
from pyat.core.dtm.interpolation.interpolation_process import interpolate_dtms


def make_dtm(temp_dir: str) -> str:
    """
    Generate a DTM with 2 holes
    Cell's longitude = [-30°, -29.944444°..., -29.5°],
    Cell's latitude = [39.5°, 39.555555°, ..., 40°]
    Resolution = 200'' (0,055555°)
    """
    # Generations from -90 to -100
    elevations = 10 * np.random.default_rng().random((10, 10)) - 100
    elevations[2, 0] = np.nan
    elevations[9, 8] = np.nan
    # 2 holes ?
    assert np.count_nonzero(np.isnan(elevations)) == 2

    cdi_index = np.full_like(elevations, dtm_driver.get_missing_value(DTM.CDI_INDEX))
    cdi_index[1, 0] = 0
    cdi_index[3, 0] = 0
    cdi_index[2, 1] = 1
    cdi_index[9, 7] = 0
    cdi_index[9, 9] = 1
    cdi_index[8, 8] = 1
    return dtm_generator.make_dtm_with_data(
        (-30.0, 40.0), (-29.5, 39.5), {DTM.ELEVATION_NAME: elevations, DTM.CDI_INDEX: cdi_index}, temp_dir
    )


def test_nominal_nearest_interpolation():
    """
    Nominal test for interpolate_netcdf4 module.
    """
    with tmp.TemporaryDirectory() as temp_dir:
        path_i_dtm = make_dtm(temp_dir)
        path_o_dtm = tmp.mktemp(suffix=".dtm.nc", dir=temp_dir)
        process = nearest.NearestInterpolationProcess()
        interpolate_dtms([path_i_dtm], [path_o_dtm], process.interpolates)
        with dtm_driver.open_dtm(path_o_dtm) as o_driver:
            # No more hole in ouput file
            assert np.count_nonzero(np.isnan(o_driver[DTM.ELEVATION_NAME][:])) == 0


def launch_an_interpolation_and_test_cdi_and_mask(process: HeightmapInterpolationProcess):
    """
    Launch an interpolation process with geo mask.
    Check elevations and CDIs.
    """
    with tmp.TemporaryDirectory() as temp_dir:
        # Generate a KML, covering the northern half, over 1 hole
        coord = [[-31.0, 41.0], [-28.0, 41.0], [-28.0, 39.75], [-31.0, 39.75]]
        kml_path = kml_generator.create_kml(temp_dir, {"zone": coord})

        path_i_dtm = make_dtm(temp_dir)
        path_o_dtm = tmp.mktemp(suffix=".dtm.nc", dir=temp_dir)

        interpolate_dtms([path_i_dtm], [path_o_dtm], process.interpolates, areas=kml_path)
        with dtm_driver.open_dtm(path_o_dtm) as o_driver:
            # Cell [9, 8] in zone : must have an elevation
            assert o_driver[DTM.ELEVATION_NAME][9, 8] is not np.ma.masked
            # Expecting CDI 1 at [1, 1] because it is the most encountered value around this point
            assert o_driver[DTM.CDI_INDEX][9, 8] == 1

            # Cell [2, 0] out of zone : not interpolated, no CDI affected
            assert o_driver[DTM.ELEVATION_NAME][2, 0] is np.ma.masked
            assert o_driver[DTM.CDI_INDEX][2, 0] is np.ma.masked


def test_cubic_interpolation():
    """
    Test of Cubic interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(cubic.CubicInterpolationProcess())


def test_nearest_interpolation():
    """
    Test of Nearest interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(nearest.NearestInterpolationProcess())


def test_linear_interpolation():
    """
    Test of Linear interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(linear.LinearInterpolationProcess())


def test_rbf_interpolation():
    """
    Test of RBF interpolation process.
    """
    rbf_parametres = rbf.RbfParameters()
    rbf_parametres.rbf_type = "cubic"
    launch_an_interpolation_and_test_cdi_and_mask(rbf.RbfInterpolationProcess(rbf_parameters=rbf_parametres))


def test_purbf_interpolation():
    """
    Test of PURBF interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(purbf.PurbfInterpolationProcess(pu_min_point_in_cell=30))


def test_harmonic_interpolation():
    """
    Test of Harmonic interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(harmonic.HarmonicInterpolationProcess())


def test_ccst_interpolation():
    """
    Test of CSST interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(ccst.CcstInterpolationProcess())


def test_tv_interpolation():
    """
    Test of TV interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(tv.TVInterpolationProcess())


def test_amle_interpolation():
    """
    Test of AMLE interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(amle.AMLEInterpolationProcess())


def test_navier_stokes_interpolation():
    """
    Test of Navier stokes interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(navier_stokes.NavierStokesInterpolationProcess(radius=10))


def test_telea_interpolation():
    """
    Test of Telea interpolation process.
    """
    launch_an_interpolation_and_test_cdi_and_mask(telea.TeleaInterpolationProcess(radius=10))


# Test failed (Windows fatal exception: access violation)
# def test_shiftmap_interpolation():
#    """
#    Test of Shiftmap interpolation process.
#    """
#    launch_an_interpolation_and_test_cdi_and_mask(shiftmap.ShiftmapInterpolationProcess())
