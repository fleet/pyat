import glob
import os.path
import tempfile

from osgeo import ogr

from pyat.app.convert.convert_to_shp_app import Convert2ShpApp
from pyat.test.file_test_installer import get_test_path

MBG_PATH = get_test_path().absolute() / "mbg" / "0136_20120607_083636_ShipName_ref.mbg"


def test_mbg():
    try:
        output_file_pattern =tempfile.mktemp()
        #remove old generated file
        output_file = output_file_pattern+".shp"
        assert os.path.exists(MBG_PATH)
        converter = Convert2ShpApp(i_paths=[MBG_PATH],o_path=output_file)
        converter()
        assert os.path.exists(output_file)
        driver = ogr.GetDriverByName('ESRI Shapefile')
        data_source = driver.Open(output_file, 0)  # 0 means read-only. 1 means writeable
        layer = data_source.GetLayer()
        feature = layer.GetFeature(0)
        geom = feature.GetGeometryRef()
        assert geom.GetPointCount() == 1176

    finally:
        #do some silent cleanup
        data_source = None
        silent_delete(output_file_pattern=output_file_pattern)


def silent_delete(output_file_pattern:str):
    try:
        to_delete = glob.glob(output_file_pattern + ".*")
        for f in to_delete:
            os.remove(f)
    except Exception as e:
        print(e)
