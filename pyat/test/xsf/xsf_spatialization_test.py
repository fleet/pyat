import unittest

import sonarnative


class TestBuildConfig(unittest.TestCase):
    def test_version(self):
        config = sonarnative.BuildConfiguration
        print(f"Start of soundernative test version "
              f"{config.VERSION_MAJOR}.{config.VERSION_MINOR}.{config.VERSION_PATCH}")
