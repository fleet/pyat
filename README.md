# __Py__thon __A__coustic __T__oolBox

The python acoustic toolbox gather a set of the French Oceanographic Fleet processing algorithms.
The set of algorithm focus mainly but not exclusively on data processing of single and multibeam echo sounders



## Project Organization

Project organization is inspired by
https://drivendata.github.io/cookiecutter-data-science/#cookiecutter-data-science

```
├── LICENSE
├── README.md          <- The top-level README for developers using this project.
├── licenses           <- This directory holds license and credit information for works astropy is derived from or distributes, and/or datasets.
├── data
│   ├── external       <- Data from third party sources.
│   ├── interim        <- Intermediate data that has been transformed.
│   ├── processed      <- The final, canonical data sets for modeling.
│   └── raw            <- The original, immutable data dump.
│
├── docs               <- A default Sphinx project; see sphinx-doc.org for details
│
├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
│                         the creator's initials, and a short `-` delimited description, e.g.
│                         `1.0-jqp-initial-data-exploration`.
│
├── references         <- Data dictionaries, manuals, reference articles and all other explanatory materials.
│
├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
│   └── figures        <- Generated graphics and figures to be used in reporting
│
├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
│
├── src                <- Source code for use in this project.
│   ├── __init__.py    <- Makes src a Python module
│   │
│   ├── app    <- integrated application modules, these are code that are callable from the globe software
│   │
│   ├── core   <- project main modules and algorithm, should not depends on any graphical (eg QT)
│   │               or visualization library (eg matplotlib)
│   │
│   ├── test    <- Unit test
│   │
│   └── visualization  <- Scripts to create exploratory and results oriented visualizations and graphical api
|
├── requirements   <- The requirements files for reproducing the analysis environment,
|                     use create_anaconda_environment.py script to create them
```

## How to use local project as dependency

To use local version of dependency projects (like PyTechsas, PyNvi...); use this command :

`python -m pip install -e path/to/SomeLocalProject --no-cache-dir`

See : https://pip.pypa.io/en/stable/topics/local-project-installs/

Note : subprojects can be open in the same PyCharm window than PyAt : `File > Open...` ; choose the local project and select `Attach`.

## Deploy new release

Pyat project can be pip installable. Use the following commands :

```
python -m build
python -m twine upload --repository gitlab-pyat dist/*
```

Note : id of server is defined in ~/.pypirc.

